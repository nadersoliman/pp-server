require('coffee-script')

addresses = []

s3Client = require('knox').createClient
  key: process.env.AWS_ACCESS_KEY_ID
  secret: process.env.AWS_SECRET_ACCESS_KEY
  bucket: 'static.kotobkhana.com'

saveAddresses = ()->
  strAddresses = JSON.stringify addresses
  req = s3Client.put 'addresses.json', 
    'Content-Length': strAddresses.length
    'Content-Type': 'application/json'
  req.end strAddresses
  
loadAddresses = ()->
  s3Client.get('addresses.json')
  .on 'response', (res)->
    #console.log res.statusCode
    #console.log res.headers
    res.setEncoding 'utf8'
    strAddresses = ''
    res.on 'data', (chunk) ->
      #console.log 'chunk', chunk
      strAddresses = strAddresses + chunk
    res.on 'end', ()->
      #console.log 'end with', strAddresses
      addresses = JSON.parse strAddresses
  .end()

restify = require 'restify'
server = restify.createServer()
server.pre(restify.pre.sanitizePath())
server.use(restify.acceptParser(server.acceptable))
server.use(restify.authorizationParser())
server.use(restify.dateParser())
server.use(restify.queryParser())
server.use(restify.jsonp())
server.use(restify.gzipResponse())
server.use(restify.bodyParser()) 

server.use(restify.CORS())

unknownMethodHandler = (req, res)->
  if req.method.toLowerCase() is 'options'
    #console.log('received an options method request');
    allowHeaders = ['Accept', 'Accept-Version', 'Content-Type', 'Api-Version', 'Origin', 'X-Requested-With']

    if res.methods.indexOf('OPTIONS') is -1
      res.methods.push 'OPTIONS'

    res.header 'Access-Control-Allow-Credentials', true
    res.header 'Access-Control-Allow-Headers', allowHeaders.join(', ')
    res.header 'Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE'
    res.header 'Access-Control-Allow-Origin', req.headers.origin

    return res.send(204)
  else
    return res.send(new restify.MethodNotAllowedError())

server.on 'MethodNotAllowed', unknownMethodHandler

server.get '/api/address/', (req, res, next)->
  #console.log 'GET, req:', req
  if not addresses
    loadAddresses()
    
  res.send addresses
  next()
  
server.post '/api/address/', (req, res, next)->
  #console.log 'POST, req:', req
  if not addresses
    loadAddresses()
  
  foundAddresses = addresses.filter (address)->
    address.lng is req.body.lng and address.lat is req.body.lat
    
  if foundAddresses.length <= 0
    addresses.push req.body
  
    saveAddresses()
    res.send()
  else
    res.send(409)
  next()
  
server.del '/api/address/:lng/:lat', (req, res, next)->
  #console.log 'DELETE, lng', req.params.lng, ', lat', req.params.lat
  if not addresses
    loadAddresses()
  
  newAddresses = addresses.filter (address)-> 
    address.lng isnt req.params.lng and address.lat isnt req.params.lat
  addresses = newAddresses
  #console.log 'now addresses is', addresses
  saveAddresses()
  res.send()
  next()

server.get /.*/, restify.serveStatic
  directory: './static'
  default: 'index.html'

port = process.env.PORT || 4000
server.listen port, ()->
  console.log '%s listening at %s', server.name, server.url
