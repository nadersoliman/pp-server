angular.module("ui.bootstrap", [ "ui.bootstrap.tpls", "ui.bootstrap.transition", "ui.bootstrap.collapse", "ui.bootstrap.accordion", "ui.bootstrap.alert", "ui.bootstrap.bindHtml", "ui.bootstrap.buttons", "ui.bootstrap.carousel", "ui.bootstrap.position", "ui.bootstrap.datepicker", "ui.bootstrap.dropdownToggle", "ui.bootstrap.modal", "ui.bootstrap.pagination", "ui.bootstrap.tooltip", "ui.bootstrap.popover", "ui.bootstrap.progressbar", "ui.bootstrap.rating", "ui.bootstrap.tabs", "ui.bootstrap.timepicker", "ui.bootstrap.typeahead" ]);

angular.module("ui.bootstrap.tpls", [ "template/accordion/accordion-group.html", "template/accordion/accordion.html", "template/alert/alert.html", "template/carousel/carousel.html", "template/carousel/slide.html", "template/datepicker/datepicker.html", "template/datepicker/popup.html", "template/modal/backdrop.html", "template/modal/window.html", "template/pagination/pager.html", "template/pagination/pagination.html", "template/tooltip/tooltip-html-unsafe-popup.html", "template/tooltip/tooltip-popup.html", "template/popover/popover.html", "template/progressbar/bar.html", "template/progressbar/progress.html", "template/progressbar/progressbar.html", "template/rating/rating.html", "template/tabs/tab.html", "template/tabs/tabset.html", "template/timepicker/timepicker.html", "template/typeahead/typeahead-match.html", "template/typeahead/typeahead-popup.html" ]);

angular.module("ui.bootstrap.transition", []).factory("$transition", [ "$q", "$timeout", "$rootScope", function($q, $timeout, $rootScope) {
    var $transition = function(element, trigger, options) {
        options = options || {};
        var deferred = $q.defer();
        var endEventName = $transition[options.animation ? "animationEndEventName" : "transitionEndEventName"];
        var transitionEndHandler = function(event) {
            $rootScope.$apply(function() {
                element.unbind(endEventName, transitionEndHandler);
                deferred.resolve(element);
            });
        };
        if (endEventName) {
            element.bind(endEventName, transitionEndHandler);
        }
        $timeout(function() {
            if (angular.isString(trigger)) {
                element.addClass(trigger);
            } else if (angular.isFunction(trigger)) {
                trigger(element);
            } else if (angular.isObject(trigger)) {
                element.css(trigger);
            }
            if (!endEventName) {
                deferred.resolve(element);
            }
        });
        deferred.promise.cancel = function() {
            if (endEventName) {
                element.unbind(endEventName, transitionEndHandler);
            }
            deferred.reject("Transition cancelled");
        };
        return deferred.promise;
    };
    var transElement = document.createElement("trans");
    var transitionEndEventNames = {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "oTransitionEnd",
        transition: "transitionend"
    };
    var animationEndEventNames = {
        WebkitTransition: "webkitAnimationEnd",
        MozTransition: "animationend",
        OTransition: "oAnimationEnd",
        transition: "animationend"
    };
    function findEndEventName(endEventNames) {
        for (var name in endEventNames) {
            if (transElement.style[name] !== undefined) {
                return endEventNames[name];
            }
        }
    }
    $transition.transitionEndEventName = findEndEventName(transitionEndEventNames);
    $transition.animationEndEventName = findEndEventName(animationEndEventNames);
    return $transition;
} ]);

angular.module("ui.bootstrap.collapse", [ "ui.bootstrap.transition" ]).directive("collapse", [ "$transition", function($transition, $timeout) {
    return {
        link: function(scope, element, attrs) {
            var initialAnimSkip = true;
            var currentTransition;
            function doTransition(change) {
                var newTransition = $transition(element, change);
                if (currentTransition) {
                    currentTransition.cancel();
                }
                currentTransition = newTransition;
                newTransition.then(newTransitionDone, newTransitionDone);
                return newTransition;
                function newTransitionDone() {
                    if (currentTransition === newTransition) {
                        currentTransition = undefined;
                    }
                }
            }
            function expand() {
                if (initialAnimSkip) {
                    initialAnimSkip = false;
                    expandDone();
                } else {
                    element.removeClass("collapse").addClass("collapsing");
                    doTransition({
                        height: element[0].scrollHeight + "px"
                    }).then(expandDone);
                }
            }
            function expandDone() {
                element.removeClass("collapsing");
                element.addClass("collapse in");
                element.css({
                    height: "auto"
                });
            }
            function collapse() {
                if (initialAnimSkip) {
                    initialAnimSkip = false;
                    collapseDone();
                    element.css({
                        height: 0
                    });
                } else {
                    element.css({
                        height: element[0].scrollHeight + "px"
                    });
                    var x = element[0].offsetWidth;
                    element.removeClass("collapse in").addClass("collapsing");
                    doTransition({
                        height: 0
                    }).then(collapseDone);
                }
            }
            function collapseDone() {
                element.removeClass("collapsing");
                element.addClass("collapse");
            }
            scope.$watch(attrs.collapse, function(shouldCollapse) {
                if (shouldCollapse) {
                    collapse();
                } else {
                    expand();
                }
            });
        }
    };
} ]);

angular.module("ui.bootstrap.accordion", [ "ui.bootstrap.collapse" ]).constant("accordionConfig", {
    closeOthers: true
}).controller("AccordionController", [ "$scope", "$attrs", "accordionConfig", function($scope, $attrs, accordionConfig) {
    this.groups = [];
    this.closeOthers = function(openGroup) {
        var closeOthers = angular.isDefined($attrs.closeOthers) ? $scope.$eval($attrs.closeOthers) : accordionConfig.closeOthers;
        if (closeOthers) {
            angular.forEach(this.groups, function(group) {
                if (group !== openGroup) {
                    group.isOpen = false;
                }
            });
        }
    };
    this.addGroup = function(groupScope) {
        var that = this;
        this.groups.push(groupScope);
        groupScope.$on("$destroy", function(event) {
            that.removeGroup(groupScope);
        });
    };
    this.removeGroup = function(group) {
        var index = this.groups.indexOf(group);
        if (index !== -1) {
            this.groups.splice(this.groups.indexOf(group), 1);
        }
    };
} ]).directive("accordion", function() {
    return {
        restrict: "EA",
        controller: "AccordionController",
        transclude: true,
        replace: false,
        templateUrl: "template/accordion/accordion.html"
    };
}).directive("accordionGroup", [ "$parse", function($parse) {
    return {
        require: "^accordion",
        restrict: "EA",
        transclude: true,
        replace: true,
        templateUrl: "template/accordion/accordion-group.html",
        scope: {
            heading: "@"
        },
        controller: function() {
            this.setHeading = function(element) {
                this.heading = element;
            };
        },
        link: function(scope, element, attrs, accordionCtrl) {
            var getIsOpen, setIsOpen;
            accordionCtrl.addGroup(scope);
            scope.isOpen = false;
            if (attrs.isOpen) {
                getIsOpen = $parse(attrs.isOpen);
                setIsOpen = getIsOpen.assign;
                scope.$parent.$watch(getIsOpen, function(value) {
                    scope.isOpen = !!value;
                });
            }
            scope.$watch("isOpen", function(value) {
                if (value) {
                    accordionCtrl.closeOthers(scope);
                }
                if (setIsOpen) {
                    setIsOpen(scope.$parent, value);
                }
            });
        }
    };
} ]).directive("accordionHeading", function() {
    return {
        restrict: "EA",
        transclude: true,
        template: "",
        replace: true,
        require: "^accordionGroup",
        compile: function(element, attr, transclude) {
            return function link(scope, element, attr, accordionGroupCtrl) {
                accordionGroupCtrl.setHeading(transclude(scope, function() {}));
            };
        }
    };
}).directive("accordionTransclude", function() {
    return {
        require: "^accordionGroup",
        link: function(scope, element, attr, controller) {
            scope.$watch(function() {
                return controller[attr.accordionTransclude];
            }, function(heading) {
                if (heading) {
                    element.html("");
                    element.append(heading);
                }
            });
        }
    };
});

angular.module("ui.bootstrap.alert", []).controller("AlertController", [ "$scope", "$attrs", function($scope, $attrs) {
    $scope.closeable = "close" in $attrs;
} ]).directive("alert", function() {
    return {
        restrict: "EA",
        controller: "AlertController",
        templateUrl: "template/alert/alert.html",
        transclude: true,
        replace: true,
        scope: {
            type: "=",
            close: "&"
        }
    };
});

angular.module("ui.bootstrap.bindHtml", []).directive("bindHtmlUnsafe", function() {
    return function(scope, element, attr) {
        element.addClass("ng-binding").data("$binding", attr.bindHtmlUnsafe);
        scope.$watch(attr.bindHtmlUnsafe, function bindHtmlUnsafeWatchAction(value) {
            element.html(value || "");
        });
    };
});

angular.module("ui.bootstrap.buttons", []).constant("buttonConfig", {
    activeClass: "active",
    toggleEvent: "click"
}).controller("ButtonsController", [ "buttonConfig", function(buttonConfig) {
    this.activeClass = buttonConfig.activeClass || "active";
    this.toggleEvent = buttonConfig.toggleEvent || "click";
} ]).directive("btnRadio", function() {
    return {
        require: [ "btnRadio", "ngModel" ],
        controller: "ButtonsController",
        link: function(scope, element, attrs, ctrls) {
            var buttonsCtrl = ctrls[0], ngModelCtrl = ctrls[1];
            ngModelCtrl.$render = function() {
                element.toggleClass(buttonsCtrl.activeClass, angular.equals(ngModelCtrl.$modelValue, scope.$eval(attrs.btnRadio)));
            };
            element.bind(buttonsCtrl.toggleEvent, function() {
                if (!element.hasClass(buttonsCtrl.activeClass)) {
                    scope.$apply(function() {
                        ngModelCtrl.$setViewValue(scope.$eval(attrs.btnRadio));
                        ngModelCtrl.$render();
                    });
                }
            });
        }
    };
}).directive("btnCheckbox", function() {
    return {
        require: [ "btnCheckbox", "ngModel" ],
        controller: "ButtonsController",
        link: function(scope, element, attrs, ctrls) {
            var buttonsCtrl = ctrls[0], ngModelCtrl = ctrls[1];
            function getTrueValue() {
                return getCheckboxValue(attrs.btnCheckboxTrue, true);
            }
            function getFalseValue() {
                return getCheckboxValue(attrs.btnCheckboxFalse, false);
            }
            function getCheckboxValue(attributeValue, defaultValue) {
                var val = scope.$eval(attributeValue);
                return angular.isDefined(val) ? val : defaultValue;
            }
            ngModelCtrl.$render = function() {
                element.toggleClass(buttonsCtrl.activeClass, angular.equals(ngModelCtrl.$modelValue, getTrueValue()));
            };
            element.bind(buttonsCtrl.toggleEvent, function() {
                scope.$apply(function() {
                    ngModelCtrl.$setViewValue(element.hasClass(buttonsCtrl.activeClass) ? getFalseValue() : getTrueValue());
                    ngModelCtrl.$render();
                });
            });
        }
    };
});

angular.module("ui.bootstrap.carousel", [ "ui.bootstrap.transition" ]).controller("CarouselController", [ "$scope", "$timeout", "$transition", "$q", function($scope, $timeout, $transition, $q) {
    var self = this, slides = self.slides = [], currentIndex = -1, currentTimeout, isPlaying;
    self.currentSlide = null;
    var destroyed = false;
    self.select = function(nextSlide, direction) {
        var nextIndex = slides.indexOf(nextSlide);
        if (direction === undefined) {
            direction = nextIndex > currentIndex ? "next" : "prev";
        }
        if (nextSlide && nextSlide !== self.currentSlide) {
            if ($scope.$currentTransition) {
                $scope.$currentTransition.cancel();
                $timeout(goNext);
            } else {
                goNext();
            }
        }
        function goNext() {
            if (destroyed) {
                return;
            }
            if (self.currentSlide && angular.isString(direction) && !$scope.noTransition && nextSlide.$element) {
                nextSlide.$element.addClass(direction);
                var reflow = nextSlide.$element[0].offsetWidth;
                angular.forEach(slides, function(slide) {
                    angular.extend(slide, {
                        direction: "",
                        entering: false,
                        leaving: false,
                        active: false
                    });
                });
                angular.extend(nextSlide, {
                    direction: direction,
                    active: true,
                    entering: true
                });
                angular.extend(self.currentSlide || {}, {
                    direction: direction,
                    leaving: true
                });
                $scope.$currentTransition = $transition(nextSlide.$element, {});
                (function(next, current) {
                    $scope.$currentTransition.then(function() {
                        transitionDone(next, current);
                    }, function() {
                        transitionDone(next, current);
                    });
                })(nextSlide, self.currentSlide);
            } else {
                transitionDone(nextSlide, self.currentSlide);
            }
            self.currentSlide = nextSlide;
            currentIndex = nextIndex;
            restartTimer();
        }
        function transitionDone(next, current) {
            angular.extend(next, {
                direction: "",
                active: true,
                leaving: false,
                entering: false
            });
            angular.extend(current || {}, {
                direction: "",
                active: false,
                leaving: false,
                entering: false
            });
            $scope.$currentTransition = null;
        }
    };
    $scope.$on("$destroy", function() {
        destroyed = true;
    });
    self.indexOfSlide = function(slide) {
        return slides.indexOf(slide);
    };
    $scope.next = function() {
        var newIndex = (currentIndex + 1) % slides.length;
        if (!$scope.$currentTransition) {
            return self.select(slides[newIndex], "next");
        }
    };
    $scope.prev = function() {
        var newIndex = currentIndex - 1 < 0 ? slides.length - 1 : currentIndex - 1;
        if (!$scope.$currentTransition) {
            return self.select(slides[newIndex], "prev");
        }
    };
    $scope.select = function(slide) {
        self.select(slide);
    };
    $scope.isActive = function(slide) {
        return self.currentSlide === slide;
    };
    $scope.slides = function() {
        return slides;
    };
    $scope.$watch("interval", restartTimer);
    $scope.$on("$destroy", resetTimer);
    function restartTimer() {
        resetTimer();
        var interval = +$scope.interval;
        if (!isNaN(interval) && interval >= 0) {
            currentTimeout = $timeout(timerFn, interval);
        }
    }
    function resetTimer() {
        if (currentTimeout) {
            $timeout.cancel(currentTimeout);
            currentTimeout = null;
        }
    }
    function timerFn() {
        if (isPlaying) {
            $scope.next();
            restartTimer();
        } else {
            $scope.pause();
        }
    }
    $scope.play = function() {
        if (!isPlaying) {
            isPlaying = true;
            restartTimer();
        }
    };
    $scope.pause = function() {
        if (!$scope.noPause) {
            isPlaying = false;
            resetTimer();
        }
    };
    self.addSlide = function(slide, element) {
        slide.$element = element;
        slides.push(slide);
        if (slides.length === 1 || slide.active) {
            self.select(slides[slides.length - 1]);
            if (slides.length == 1) {
                $scope.play();
            }
        } else {
            slide.active = false;
        }
    };
    self.removeSlide = function(slide) {
        var index = slides.indexOf(slide);
        slides.splice(index, 1);
        if (slides.length > 0 && slide.active) {
            if (index >= slides.length) {
                self.select(slides[index - 1]);
            } else {
                self.select(slides[index]);
            }
        } else if (currentIndex > index) {
            currentIndex--;
        }
    };
} ]).directive("carousel", [ function() {
    return {
        restrict: "EA",
        transclude: true,
        replace: true,
        controller: "CarouselController",
        require: "carousel",
        templateUrl: "template/carousel/carousel.html",
        scope: {
            interval: "=",
            noTransition: "=",
            noPause: "="
        }
    };
} ]).directive("slide", [ "$parse", function($parse) {
    return {
        require: "^carousel",
        restrict: "EA",
        transclude: true,
        replace: true,
        templateUrl: "template/carousel/slide.html",
        scope: {},
        link: function(scope, element, attrs, carouselCtrl) {
            if (attrs.active) {
                var getActive = $parse(attrs.active);
                var setActive = getActive.assign;
                var lastValue = scope.active = getActive(scope.$parent);
                scope.$watch(function parentActiveWatch() {
                    var parentActive = getActive(scope.$parent);
                    if (parentActive !== scope.active) {
                        if (parentActive !== lastValue) {
                            lastValue = scope.active = parentActive;
                        } else {
                            setActive(scope.$parent, parentActive = lastValue = scope.active);
                        }
                    }
                    return parentActive;
                });
            }
            carouselCtrl.addSlide(scope, element);
            scope.$on("$destroy", function() {
                carouselCtrl.removeSlide(scope);
            });
            scope.$watch("active", function(active) {
                if (active) {
                    carouselCtrl.select(scope);
                }
            });
        }
    };
} ]);

angular.module("ui.bootstrap.position", []).factory("$position", [ "$document", "$window", function($document, $window) {
    function getStyle(el, cssprop) {
        if (el.currentStyle) {
            return el.currentStyle[cssprop];
        } else if ($window.getComputedStyle) {
            return $window.getComputedStyle(el)[cssprop];
        }
        return el.style[cssprop];
    }
    function isStaticPositioned(element) {
        return (getStyle(element, "position") || "static") === "static";
    }
    var parentOffsetEl = function(element) {
        var docDomEl = $document[0];
        var offsetParent = element.offsetParent || docDomEl;
        while (offsetParent && offsetParent !== docDomEl && isStaticPositioned(offsetParent)) {
            offsetParent = offsetParent.offsetParent;
        }
        return offsetParent || docDomEl;
    };
    return {
        position: function(element) {
            var elBCR = this.offset(element);
            var offsetParentBCR = {
                top: 0,
                left: 0
            };
            var offsetParentEl = parentOffsetEl(element[0]);
            if (offsetParentEl != $document[0]) {
                offsetParentBCR = this.offset(angular.element(offsetParentEl));
                offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop;
                offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft;
            }
            var boundingClientRect = element[0].getBoundingClientRect();
            return {
                width: boundingClientRect.width || element.prop("offsetWidth"),
                height: boundingClientRect.height || element.prop("offsetHeight"),
                top: elBCR.top - offsetParentBCR.top,
                left: elBCR.left - offsetParentBCR.left
            };
        },
        offset: function(element) {
            var boundingClientRect = element[0].getBoundingClientRect();
            return {
                width: boundingClientRect.width || element.prop("offsetWidth"),
                height: boundingClientRect.height || element.prop("offsetHeight"),
                top: boundingClientRect.top + ($window.pageYOffset || $document[0].body.scrollTop || $document[0].documentElement.scrollTop),
                left: boundingClientRect.left + ($window.pageXOffset || $document[0].body.scrollLeft || $document[0].documentElement.scrollLeft)
            };
        }
    };
} ]);

angular.module("ui.bootstrap.datepicker", [ "ui.bootstrap.position" ]).constant("datepickerConfig", {
    dayFormat: "dd",
    monthFormat: "MMMM",
    yearFormat: "yyyy",
    dayHeaderFormat: "EEE",
    dayTitleFormat: "MMMM yyyy",
    monthTitleFormat: "yyyy",
    showWeeks: true,
    startingDay: 0,
    yearRange: 20,
    minDate: null,
    maxDate: null
}).controller("DatepickerController", [ "$scope", "$attrs", "dateFilter", "datepickerConfig", function($scope, $attrs, dateFilter, dtConfig) {
    var format = {
        day: getValue($attrs.dayFormat, dtConfig.dayFormat),
        month: getValue($attrs.monthFormat, dtConfig.monthFormat),
        year: getValue($attrs.yearFormat, dtConfig.yearFormat),
        dayHeader: getValue($attrs.dayHeaderFormat, dtConfig.dayHeaderFormat),
        dayTitle: getValue($attrs.dayTitleFormat, dtConfig.dayTitleFormat),
        monthTitle: getValue($attrs.monthTitleFormat, dtConfig.monthTitleFormat)
    }, startingDay = getValue($attrs.startingDay, dtConfig.startingDay), yearRange = getValue($attrs.yearRange, dtConfig.yearRange);
    this.minDate = dtConfig.minDate ? new Date(dtConfig.minDate) : null;
    this.maxDate = dtConfig.maxDate ? new Date(dtConfig.maxDate) : null;
    function getValue(value, defaultValue) {
        return angular.isDefined(value) ? $scope.$parent.$eval(value) : defaultValue;
    }
    function getDaysInMonth(year, month) {
        return new Date(year, month, 0).getDate();
    }
    function getDates(startDate, n) {
        var dates = new Array(n);
        var current = startDate, i = 0;
        while (i < n) {
            dates[i++] = new Date(current);
            current.setDate(current.getDate() + 1);
        }
        return dates;
    }
    function makeDate(date, format, isSelected, isSecondary) {
        return {
            date: date,
            label: dateFilter(date, format),
            selected: !!isSelected,
            secondary: !!isSecondary
        };
    }
    this.modes = [ {
        name: "day",
        getVisibleDates: function(date, selected) {
            var year = date.getFullYear(), month = date.getMonth(), firstDayOfMonth = new Date(year, month, 1);
            var difference = startingDay - firstDayOfMonth.getDay(), numDisplayedFromPreviousMonth = difference > 0 ? 7 - difference : -difference, firstDate = new Date(firstDayOfMonth), numDates = 0;
            if (numDisplayedFromPreviousMonth > 0) {
                firstDate.setDate(-numDisplayedFromPreviousMonth + 1);
                numDates += numDisplayedFromPreviousMonth;
            }
            numDates += getDaysInMonth(year, month + 1);
            numDates += (7 - numDates % 7) % 7;
            var days = getDates(firstDate, numDates), labels = new Array(7);
            for (var i = 0; i < numDates; i++) {
                var dt = new Date(days[i]);
                days[i] = makeDate(dt, format.day, selected && selected.getDate() === dt.getDate() && selected.getMonth() === dt.getMonth() && selected.getFullYear() === dt.getFullYear(), dt.getMonth() !== month);
            }
            for (var j = 0; j < 7; j++) {
                labels[j] = dateFilter(days[j].date, format.dayHeader);
            }
            return {
                objects: days,
                title: dateFilter(date, format.dayTitle),
                labels: labels
            };
        },
        compare: function(date1, date2) {
            return new Date(date1.getFullYear(), date1.getMonth(), date1.getDate()) - new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        },
        split: 7,
        step: {
            months: 1
        }
    }, {
        name: "month",
        getVisibleDates: function(date, selected) {
            var months = new Array(12), year = date.getFullYear();
            for (var i = 0; i < 12; i++) {
                var dt = new Date(year, i, 1);
                months[i] = makeDate(dt, format.month, selected && selected.getMonth() === i && selected.getFullYear() === year);
            }
            return {
                objects: months,
                title: dateFilter(date, format.monthTitle)
            };
        },
        compare: function(date1, date2) {
            return new Date(date1.getFullYear(), date1.getMonth()) - new Date(date2.getFullYear(), date2.getMonth());
        },
        split: 3,
        step: {
            years: 1
        }
    }, {
        name: "year",
        getVisibleDates: function(date, selected) {
            var years = new Array(yearRange), year = date.getFullYear(), startYear = parseInt((year - 1) / yearRange, 10) * yearRange + 1;
            for (var i = 0; i < yearRange; i++) {
                var dt = new Date(startYear + i, 0, 1);
                years[i] = makeDate(dt, format.year, selected && selected.getFullYear() === dt.getFullYear());
            }
            return {
                objects: years,
                title: [ years[0].label, years[yearRange - 1].label ].join(" - ")
            };
        },
        compare: function(date1, date2) {
            return date1.getFullYear() - date2.getFullYear();
        },
        split: 5,
        step: {
            years: yearRange
        }
    } ];
    this.isDisabled = function(date, mode) {
        var currentMode = this.modes[mode || 0];
        return this.minDate && currentMode.compare(date, this.minDate) < 0 || this.maxDate && currentMode.compare(date, this.maxDate) > 0 || $scope.dateDisabled && $scope.dateDisabled({
            date: date,
            mode: currentMode.name
        });
    };
} ]).directive("datepicker", [ "dateFilter", "$parse", "datepickerConfig", "$log", function(dateFilter, $parse, datepickerConfig, $log) {
    return {
        restrict: "EA",
        replace: true,
        templateUrl: "template/datepicker/datepicker.html",
        scope: {
            dateDisabled: "&"
        },
        require: [ "datepicker", "?^ngModel" ],
        controller: "DatepickerController",
        link: function(scope, element, attrs, ctrls) {
            var datepickerCtrl = ctrls[0], ngModel = ctrls[1];
            if (!ngModel) {
                return;
            }
            var mode = 0, selected = new Date(), showWeeks = datepickerConfig.showWeeks;
            if (attrs.showWeeks) {
                scope.$parent.$watch($parse(attrs.showWeeks), function(value) {
                    showWeeks = !!value;
                    updateShowWeekNumbers();
                });
            } else {
                updateShowWeekNumbers();
            }
            if (attrs.min) {
                scope.$parent.$watch($parse(attrs.min), function(value) {
                    datepickerCtrl.minDate = value ? new Date(value) : null;
                    refill();
                });
            }
            if (attrs.max) {
                scope.$parent.$watch($parse(attrs.max), function(value) {
                    datepickerCtrl.maxDate = value ? new Date(value) : null;
                    refill();
                });
            }
            function updateShowWeekNumbers() {
                scope.showWeekNumbers = mode === 0 && showWeeks;
            }
            function split(arr, size) {
                var arrays = [];
                while (arr.length > 0) {
                    arrays.push(arr.splice(0, size));
                }
                return arrays;
            }
            function refill(updateSelected) {
                var date = null, valid = true;
                if (ngModel.$modelValue) {
                    date = new Date(ngModel.$modelValue);
                    if (isNaN(date)) {
                        valid = false;
                        $log.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.');
                    } else if (updateSelected) {
                        selected = date;
                    }
                }
                ngModel.$setValidity("date", valid);
                var currentMode = datepickerCtrl.modes[mode], data = currentMode.getVisibleDates(selected, date);
                angular.forEach(data.objects, function(obj) {
                    obj.disabled = datepickerCtrl.isDisabled(obj.date, mode);
                });
                ngModel.$setValidity("date-disabled", !date || !datepickerCtrl.isDisabled(date));
                scope.rows = split(data.objects, currentMode.split);
                scope.labels = data.labels || [];
                scope.title = data.title;
            }
            function setMode(value) {
                mode = value;
                updateShowWeekNumbers();
                refill();
            }
            ngModel.$render = function() {
                refill(true);
            };
            scope.select = function(date) {
                if (mode === 0) {
                    var dt = ngModel.$modelValue ? new Date(ngModel.$modelValue) : new Date(0, 0, 0, 0, 0, 0, 0);
                    dt.setFullYear(date.getFullYear(), date.getMonth(), date.getDate());
                    ngModel.$setViewValue(dt);
                    refill(true);
                } else {
                    selected = date;
                    setMode(mode - 1);
                }
            };
            scope.move = function(direction) {
                var step = datepickerCtrl.modes[mode].step;
                selected.setMonth(selected.getMonth() + direction * (step.months || 0));
                selected.setFullYear(selected.getFullYear() + direction * (step.years || 0));
                refill();
            };
            scope.toggleMode = function() {
                setMode((mode + 1) % datepickerCtrl.modes.length);
            };
            scope.getWeekNumber = function(row) {
                return mode === 0 && scope.showWeekNumbers && row.length === 7 ? getISO8601WeekNumber(row[0].date) : null;
            };
            function getISO8601WeekNumber(date) {
                var checkDate = new Date(date);
                checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7));
                var time = checkDate.getTime();
                checkDate.setMonth(0);
                checkDate.setDate(1);
                return Math.floor(Math.round((time - checkDate) / 864e5) / 7) + 1;
            }
        }
    };
} ]).constant("datepickerPopupConfig", {
    dateFormat: "yyyy-MM-dd",
    currentText: "Today",
    toggleWeeksText: "Weeks",
    clearText: "Clear",
    closeText: "Done",
    closeOnDateSelection: true,
    appendToBody: false,
    showButtonBar: true
}).directive("datepickerPopup", [ "$compile", "$parse", "$document", "$position", "dateFilter", "datepickerPopupConfig", "datepickerConfig", function($compile, $parse, $document, $position, dateFilter, datepickerPopupConfig, datepickerConfig) {
    return {
        restrict: "EA",
        require: "ngModel",
        link: function(originalScope, element, attrs, ngModel) {
            var scope = originalScope.$new(), dateFormat, closeOnDateSelection = angular.isDefined(attrs.closeOnDateSelection) ? originalScope.$eval(attrs.closeOnDateSelection) : datepickerPopupConfig.closeOnDateSelection, appendToBody = angular.isDefined(attrs.datepickerAppendToBody) ? originalScope.$eval(attrs.datepickerAppendToBody) : datepickerPopupConfig.appendToBody;
            attrs.$observe("datepickerPopup", function(value) {
                dateFormat = value || datepickerPopupConfig.dateFormat;
                ngModel.$render();
            });
            scope.showButtonBar = angular.isDefined(attrs.showButtonBar) ? originalScope.$eval(attrs.showButtonBar) : datepickerPopupConfig.showButtonBar;
            originalScope.$on("$destroy", function() {
                $popup.remove();
                scope.$destroy();
            });
            attrs.$observe("currentText", function(text) {
                scope.currentText = angular.isDefined(text) ? text : datepickerPopupConfig.currentText;
            });
            attrs.$observe("toggleWeeksText", function(text) {
                scope.toggleWeeksText = angular.isDefined(text) ? text : datepickerPopupConfig.toggleWeeksText;
            });
            attrs.$observe("clearText", function(text) {
                scope.clearText = angular.isDefined(text) ? text : datepickerPopupConfig.clearText;
            });
            attrs.$observe("closeText", function(text) {
                scope.closeText = angular.isDefined(text) ? text : datepickerPopupConfig.closeText;
            });
            var getIsOpen, setIsOpen;
            if (attrs.isOpen) {
                getIsOpen = $parse(attrs.isOpen);
                setIsOpen = getIsOpen.assign;
                originalScope.$watch(getIsOpen, function updateOpen(value) {
                    scope.isOpen = !!value;
                });
            }
            scope.isOpen = getIsOpen ? getIsOpen(originalScope) : false;
            function setOpen(value) {
                if (setIsOpen) {
                    setIsOpen(originalScope, !!value);
                } else {
                    scope.isOpen = !!value;
                }
            }
            var documentClickBind = function(event) {
                if (scope.isOpen && event.target !== element[0]) {
                    scope.$apply(function() {
                        setOpen(false);
                    });
                }
            };
            var elementFocusBind = function() {
                scope.$apply(function() {
                    setOpen(true);
                });
            };
            var popupEl = angular.element("<div datepicker-popup-wrap><div datepicker></div></div>");
            popupEl.attr({
                "ng-model": "date",
                "ng-change": "dateSelection()"
            });
            var datepickerEl = angular.element(popupEl.children()[0]), datepickerOptions = {};
            if (attrs.datepickerOptions) {
                datepickerOptions = originalScope.$eval(attrs.datepickerOptions);
                datepickerEl.attr(angular.extend({}, datepickerOptions));
            }
            function parseDate(viewValue) {
                if (!viewValue) {
                    ngModel.$setValidity("date", true);
                    return null;
                } else if (angular.isDate(viewValue)) {
                    ngModel.$setValidity("date", true);
                    return viewValue;
                } else if (angular.isString(viewValue)) {
                    var date = new Date(viewValue);
                    if (isNaN(date)) {
                        ngModel.$setValidity("date", false);
                        return undefined;
                    } else {
                        ngModel.$setValidity("date", true);
                        return date;
                    }
                } else {
                    ngModel.$setValidity("date", false);
                    return undefined;
                }
            }
            ngModel.$parsers.unshift(parseDate);
            scope.dateSelection = function(dt) {
                if (angular.isDefined(dt)) {
                    scope.date = dt;
                }
                ngModel.$setViewValue(scope.date);
                ngModel.$render();
                if (closeOnDateSelection) {
                    setOpen(false);
                }
            };
            element.bind("input change keyup", function() {
                scope.$apply(function() {
                    scope.date = ngModel.$modelValue;
                });
            });
            ngModel.$render = function() {
                var date = ngModel.$viewValue ? dateFilter(ngModel.$viewValue, dateFormat) : "";
                element.val(date);
                scope.date = ngModel.$modelValue;
            };
            function addWatchableAttribute(attribute, scopeProperty, datepickerAttribute) {
                if (attribute) {
                    originalScope.$watch($parse(attribute), function(value) {
                        scope[scopeProperty] = value;
                    });
                    datepickerEl.attr(datepickerAttribute || scopeProperty, scopeProperty);
                }
            }
            addWatchableAttribute(attrs.min, "min");
            addWatchableAttribute(attrs.max, "max");
            if (attrs.showWeeks) {
                addWatchableAttribute(attrs.showWeeks, "showWeeks", "show-weeks");
            } else {
                scope.showWeeks = "show-weeks" in datepickerOptions ? datepickerOptions["show-weeks"] : datepickerConfig.showWeeks;
                datepickerEl.attr("show-weeks", "showWeeks");
            }
            if (attrs.dateDisabled) {
                datepickerEl.attr("date-disabled", attrs.dateDisabled);
            }
            function updatePosition() {
                scope.position = appendToBody ? $position.offset(element) : $position.position(element);
                scope.position.top = scope.position.top + element.prop("offsetHeight");
            }
            var documentBindingInitialized = false, elementFocusInitialized = false;
            scope.$watch("isOpen", function(value) {
                if (value) {
                    updatePosition();
                    $document.bind("click", documentClickBind);
                    if (elementFocusInitialized) {
                        element.unbind("focus", elementFocusBind);
                    }
                    element[0].focus();
                    documentBindingInitialized = true;
                } else {
                    if (documentBindingInitialized) {
                        $document.unbind("click", documentClickBind);
                    }
                    element.bind("focus", elementFocusBind);
                    elementFocusInitialized = true;
                }
                if (setIsOpen) {
                    setIsOpen(originalScope, value);
                }
            });
            scope.today = function() {
                scope.dateSelection(new Date());
            };
            scope.clear = function() {
                scope.dateSelection(null);
            };
            var $popup = $compile(popupEl)(scope);
            if (appendToBody) {
                $document.find("body").append($popup);
            } else {
                element.after($popup);
            }
        }
    };
} ]).directive("datepickerPopupWrap", function() {
    return {
        restrict: "EA",
        replace: true,
        transclude: true,
        templateUrl: "template/datepicker/popup.html",
        link: function(scope, element, attrs) {
            element.bind("click", function(event) {
                event.preventDefault();
                event.stopPropagation();
            });
        }
    };
});

angular.module("ui.bootstrap.dropdownToggle", []).directive("dropdownToggle", [ "$document", "$location", function($document, $location) {
    var openElement = null, closeMenu = angular.noop;
    return {
        restrict: "CA",
        link: function(scope, element, attrs) {
            scope.$watch("$location.path", function() {
                closeMenu();
            });
            element.parent().bind("click", function() {
                closeMenu();
            });
            element.bind("click", function(event) {
                var elementWasOpen = element === openElement;
                event.preventDefault();
                event.stopPropagation();
                if (!!openElement) {
                    closeMenu();
                }
                if (!elementWasOpen && !element.hasClass("disabled") && !element.prop("disabled")) {
                    element.parent().addClass("open");
                    openElement = element;
                    closeMenu = function(event) {
                        if (event) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        $document.unbind("click", closeMenu);
                        element.parent().removeClass("open");
                        closeMenu = angular.noop;
                        openElement = null;
                    };
                    $document.bind("click", closeMenu);
                }
            });
        }
    };
} ]);

angular.module("ui.bootstrap.modal", [ "ui.bootstrap.transition" ]).factory("$$stackedMap", function() {
    return {
        createNew: function() {
            var stack = [];
            return {
                add: function(key, value) {
                    stack.push({
                        key: key,
                        value: value
                    });
                },
                get: function(key) {
                    for (var i = 0; i < stack.length; i++) {
                        if (key == stack[i].key) {
                            return stack[i];
                        }
                    }
                },
                keys: function() {
                    var keys = [];
                    for (var i = 0; i < stack.length; i++) {
                        keys.push(stack[i].key);
                    }
                    return keys;
                },
                top: function() {
                    return stack[stack.length - 1];
                },
                remove: function(key) {
                    var idx = -1;
                    for (var i = 0; i < stack.length; i++) {
                        if (key == stack[i].key) {
                            idx = i;
                            break;
                        }
                    }
                    return stack.splice(idx, 1)[0];
                },
                removeTop: function() {
                    return stack.splice(stack.length - 1, 1)[0];
                },
                length: function() {
                    return stack.length;
                }
            };
        }
    };
}).directive("modalBackdrop", [ "$timeout", function($timeout) {
    return {
        restrict: "EA",
        replace: true,
        templateUrl: "template/modal/backdrop.html",
        link: function(scope) {
            scope.animate = false;
            $timeout(function() {
                scope.animate = true;
            });
        }
    };
} ]).directive("modalWindow", [ "$modalStack", "$timeout", function($modalStack, $timeout) {
    return {
        restrict: "EA",
        scope: {
            index: "@",
            animate: "="
        },
        replace: true,
        transclude: true,
        templateUrl: "template/modal/window.html",
        link: function(scope, element, attrs) {
            scope.windowClass = attrs.windowClass || "";
            $timeout(function() {
                scope.animate = true;
                element[0].focus();
            });
            scope.close = function(evt) {
                var modal = $modalStack.getTop();
                if (modal && modal.value.backdrop && modal.value.backdrop != "static" && evt.target === evt.currentTarget) {
                    evt.preventDefault();
                    evt.stopPropagation();
                    $modalStack.dismiss(modal.key, "backdrop click");
                }
            };
        }
    };
} ]).factory("$modalStack", [ "$transition", "$timeout", "$document", "$compile", "$rootScope", "$$stackedMap", function($transition, $timeout, $document, $compile, $rootScope, $$stackedMap) {
    var OPENED_MODAL_CLASS = "modal-open";
    var backdropDomEl, backdropScope;
    var openedWindows = $$stackedMap.createNew();
    var $modalStack = {};
    function backdropIndex() {
        var topBackdropIndex = -1;
        var opened = openedWindows.keys();
        for (var i = 0; i < opened.length; i++) {
            if (openedWindows.get(opened[i]).value.backdrop) {
                topBackdropIndex = i;
            }
        }
        return topBackdropIndex;
    }
    $rootScope.$watch(backdropIndex, function(newBackdropIndex) {
        if (backdropScope) {
            backdropScope.index = newBackdropIndex;
        }
    });
    function removeModalWindow(modalInstance) {
        var body = $document.find("body").eq(0);
        var modalWindow = openedWindows.get(modalInstance).value;
        openedWindows.remove(modalInstance);
        removeAfterAnimate(modalWindow.modalDomEl, modalWindow.modalScope, 300, checkRemoveBackdrop);
        body.toggleClass(OPENED_MODAL_CLASS, openedWindows.length() > 0);
    }
    function checkRemoveBackdrop() {
        if (backdropDomEl && backdropIndex() == -1) {
            var backdropScopeRef = backdropScope;
            removeAfterAnimate(backdropDomEl, backdropScope, 150, function() {
                backdropScopeRef.$destroy();
                backdropScopeRef = null;
            });
            backdropDomEl = undefined;
            backdropScope = undefined;
        }
    }
    function removeAfterAnimate(domEl, scope, emulateTime, done) {
        scope.animate = false;
        var transitionEndEventName = $transition.transitionEndEventName;
        if (transitionEndEventName) {
            var timeout = $timeout(afterAnimating, emulateTime);
            domEl.bind(transitionEndEventName, function() {
                $timeout.cancel(timeout);
                afterAnimating();
                scope.$apply();
            });
        } else {
            $timeout(afterAnimating, 0);
        }
        function afterAnimating() {
            if (afterAnimating.done) {
                return;
            }
            afterAnimating.done = true;
            domEl.remove();
            if (done) {
                done();
            }
        }
    }
    $document.bind("keydown", function(evt) {
        var modal;
        if (evt.which === 27) {
            modal = openedWindows.top();
            if (modal && modal.value.keyboard) {
                $rootScope.$apply(function() {
                    $modalStack.dismiss(modal.key);
                });
            }
        }
    });
    $modalStack.open = function(modalInstance, modal) {
        openedWindows.add(modalInstance, {
            deferred: modal.deferred,
            modalScope: modal.scope,
            backdrop: modal.backdrop,
            keyboard: modal.keyboard
        });
        var body = $document.find("body").eq(0), currBackdropIndex = backdropIndex();
        if (currBackdropIndex >= 0 && !backdropDomEl) {
            backdropScope = $rootScope.$new(true);
            backdropScope.index = currBackdropIndex;
            backdropDomEl = $compile("<div modal-backdrop></div>")(backdropScope);
            body.append(backdropDomEl);
        }
        var angularDomEl = angular.element("<div modal-window></div>");
        angularDomEl.attr("window-class", modal.windowClass);
        angularDomEl.attr("index", openedWindows.length() - 1);
        angularDomEl.attr("animate", "animate");
        angularDomEl.html(modal.content);
        var modalDomEl = $compile(angularDomEl)(modal.scope);
        openedWindows.top().value.modalDomEl = modalDomEl;
        body.append(modalDomEl);
        body.addClass(OPENED_MODAL_CLASS);
    };
    $modalStack.close = function(modalInstance, result) {
        var modalWindow = openedWindows.get(modalInstance).value;
        if (modalWindow) {
            modalWindow.deferred.resolve(result);
            removeModalWindow(modalInstance);
        }
    };
    $modalStack.dismiss = function(modalInstance, reason) {
        var modalWindow = openedWindows.get(modalInstance).value;
        if (modalWindow) {
            modalWindow.deferred.reject(reason);
            removeModalWindow(modalInstance);
        }
    };
    $modalStack.dismissAll = function(reason) {
        var topModal = this.getTop();
        while (topModal) {
            this.dismiss(topModal.key, reason);
            topModal = this.getTop();
        }
    };
    $modalStack.getTop = function() {
        return openedWindows.top();
    };
    return $modalStack;
} ]).provider("$modal", function() {
    var $modalProvider = {
        options: {
            backdrop: true,
            keyboard: true
        },
        $get: [ "$injector", "$rootScope", "$q", "$http", "$templateCache", "$controller", "$modalStack", function($injector, $rootScope, $q, $http, $templateCache, $controller, $modalStack) {
            var $modal = {};
            function getTemplatePromise(options) {
                return options.template ? $q.when(options.template) : $http.get(options.templateUrl, {
                    cache: $templateCache
                }).then(function(result) {
                    return result.data;
                });
            }
            function getResolvePromises(resolves) {
                var promisesArr = [];
                angular.forEach(resolves, function(value, key) {
                    if (angular.isFunction(value) || angular.isArray(value)) {
                        promisesArr.push($q.when($injector.invoke(value)));
                    }
                });
                return promisesArr;
            }
            $modal.open = function(modalOptions) {
                var modalResultDeferred = $q.defer();
                var modalOpenedDeferred = $q.defer();
                var modalInstance = {
                    result: modalResultDeferred.promise,
                    opened: modalOpenedDeferred.promise,
                    close: function(result) {
                        $modalStack.close(modalInstance, result);
                    },
                    dismiss: function(reason) {
                        $modalStack.dismiss(modalInstance, reason);
                    }
                };
                modalOptions = angular.extend({}, $modalProvider.options, modalOptions);
                modalOptions.resolve = modalOptions.resolve || {};
                if (!modalOptions.template && !modalOptions.templateUrl) {
                    throw new Error("One of template or templateUrl options is required.");
                }
                var templateAndResolvePromise = $q.all([ getTemplatePromise(modalOptions) ].concat(getResolvePromises(modalOptions.resolve)));
                templateAndResolvePromise.then(function resolveSuccess(tplAndVars) {
                    var modalScope = (modalOptions.scope || $rootScope).$new();
                    modalScope.$close = modalInstance.close;
                    modalScope.$dismiss = modalInstance.dismiss;
                    var ctrlInstance, ctrlLocals = {};
                    var resolveIter = 1;
                    if (modalOptions.controller) {
                        ctrlLocals.$scope = modalScope;
                        ctrlLocals.$modalInstance = modalInstance;
                        angular.forEach(modalOptions.resolve, function(value, key) {
                            ctrlLocals[key] = tplAndVars[resolveIter++];
                        });
                        ctrlInstance = $controller(modalOptions.controller, ctrlLocals);
                    }
                    $modalStack.open(modalInstance, {
                        scope: modalScope,
                        deferred: modalResultDeferred,
                        content: tplAndVars[0],
                        backdrop: modalOptions.backdrop,
                        keyboard: modalOptions.keyboard,
                        windowClass: modalOptions.windowClass
                    });
                }, function resolveError(reason) {
                    modalResultDeferred.reject(reason);
                });
                templateAndResolvePromise.then(function() {
                    modalOpenedDeferred.resolve(true);
                }, function() {
                    modalOpenedDeferred.reject(false);
                });
                return modalInstance;
            };
            return $modal;
        } ]
    };
    return $modalProvider;
});

angular.module("ui.bootstrap.pagination", []).controller("PaginationController", [ "$scope", "$attrs", "$parse", "$interpolate", function($scope, $attrs, $parse, $interpolate) {
    var self = this, setNumPages = $attrs.numPages ? $parse($attrs.numPages).assign : angular.noop;
    this.init = function(defaultItemsPerPage) {
        if ($attrs.itemsPerPage) {
            $scope.$parent.$watch($parse($attrs.itemsPerPage), function(value) {
                self.itemsPerPage = parseInt(value, 10);
                $scope.totalPages = self.calculateTotalPages();
            });
        } else {
            this.itemsPerPage = defaultItemsPerPage;
        }
    };
    this.noPrevious = function() {
        return this.page === 1;
    };
    this.noNext = function() {
        return this.page === $scope.totalPages;
    };
    this.isActive = function(page) {
        return this.page === page;
    };
    this.calculateTotalPages = function() {
        var totalPages = this.itemsPerPage < 1 ? 1 : Math.ceil($scope.totalItems / this.itemsPerPage);
        return Math.max(totalPages || 0, 1);
    };
    this.getAttributeValue = function(attribute, defaultValue, interpolate) {
        return angular.isDefined(attribute) ? interpolate ? $interpolate(attribute)($scope.$parent) : $scope.$parent.$eval(attribute) : defaultValue;
    };
    this.render = function() {
        this.page = parseInt($scope.page, 10) || 1;
        if (this.page > 0 && this.page <= $scope.totalPages) {
            $scope.pages = this.getPages(this.page, $scope.totalPages);
        }
    };
    $scope.selectPage = function(page) {
        if (!self.isActive(page) && page > 0 && page <= $scope.totalPages) {
            $scope.page = page;
            $scope.onSelectPage({
                page: page
            });
        }
    };
    $scope.$watch("page", function() {
        self.render();
    });
    $scope.$watch("totalItems", function() {
        $scope.totalPages = self.calculateTotalPages();
    });
    $scope.$watch("totalPages", function(value) {
        setNumPages($scope.$parent, value);
        if (self.page > value) {
            $scope.selectPage(value);
        } else {
            self.render();
        }
    });
} ]).constant("paginationConfig", {
    itemsPerPage: 10,
    boundaryLinks: false,
    directionLinks: true,
    firstText: "First",
    previousText: "Previous",
    nextText: "Next",
    lastText: "Last",
    rotate: true
}).directive("pagination", [ "$parse", "paginationConfig", function($parse, config) {
    return {
        restrict: "EA",
        scope: {
            page: "=",
            totalItems: "=",
            onSelectPage: " &"
        },
        controller: "PaginationController",
        templateUrl: "template/pagination/pagination.html",
        replace: true,
        link: function(scope, element, attrs, paginationCtrl) {
            var maxSize, boundaryLinks = paginationCtrl.getAttributeValue(attrs.boundaryLinks, config.boundaryLinks), directionLinks = paginationCtrl.getAttributeValue(attrs.directionLinks, config.directionLinks), firstText = paginationCtrl.getAttributeValue(attrs.firstText, config.firstText, true), previousText = paginationCtrl.getAttributeValue(attrs.previousText, config.previousText, true), nextText = paginationCtrl.getAttributeValue(attrs.nextText, config.nextText, true), lastText = paginationCtrl.getAttributeValue(attrs.lastText, config.lastText, true), rotate = paginationCtrl.getAttributeValue(attrs.rotate, config.rotate);
            paginationCtrl.init(config.itemsPerPage);
            if (attrs.maxSize) {
                scope.$parent.$watch($parse(attrs.maxSize), function(value) {
                    maxSize = parseInt(value, 10);
                    paginationCtrl.render();
                });
            }
            function makePage(number, text, isActive, isDisabled) {
                return {
                    number: number,
                    text: text,
                    active: isActive,
                    disabled: isDisabled
                };
            }
            paginationCtrl.getPages = function(currentPage, totalPages) {
                var pages = [];
                var startPage = 1, endPage = totalPages;
                var isMaxSized = angular.isDefined(maxSize) && maxSize < totalPages;
                if (isMaxSized) {
                    if (rotate) {
                        startPage = Math.max(currentPage - Math.floor(maxSize / 2), 1);
                        endPage = startPage + maxSize - 1;
                        if (endPage > totalPages) {
                            endPage = totalPages;
                            startPage = endPage - maxSize + 1;
                        }
                    } else {
                        startPage = (Math.ceil(currentPage / maxSize) - 1) * maxSize + 1;
                        endPage = Math.min(startPage + maxSize - 1, totalPages);
                    }
                }
                for (var number = startPage; number <= endPage; number++) {
                    var page = makePage(number, number, paginationCtrl.isActive(number), false);
                    pages.push(page);
                }
                if (isMaxSized && !rotate) {
                    if (startPage > 1) {
                        var previousPageSet = makePage(startPage - 1, "...", false, false);
                        pages.unshift(previousPageSet);
                    }
                    if (endPage < totalPages) {
                        var nextPageSet = makePage(endPage + 1, "...", false, false);
                        pages.push(nextPageSet);
                    }
                }
                if (directionLinks) {
                    var previousPage = makePage(currentPage - 1, previousText, false, paginationCtrl.noPrevious());
                    pages.unshift(previousPage);
                    var nextPage = makePage(currentPage + 1, nextText, false, paginationCtrl.noNext());
                    pages.push(nextPage);
                }
                if (boundaryLinks) {
                    var firstPage = makePage(1, firstText, false, paginationCtrl.noPrevious());
                    pages.unshift(firstPage);
                    var lastPage = makePage(totalPages, lastText, false, paginationCtrl.noNext());
                    pages.push(lastPage);
                }
                return pages;
            };
        }
    };
} ]).constant("pagerConfig", {
    itemsPerPage: 10,
    previousText: "« Previous",
    nextText: "Next »",
    align: true
}).directive("pager", [ "pagerConfig", function(config) {
    return {
        restrict: "EA",
        scope: {
            page: "=",
            totalItems: "=",
            onSelectPage: " &"
        },
        controller: "PaginationController",
        templateUrl: "template/pagination/pager.html",
        replace: true,
        link: function(scope, element, attrs, paginationCtrl) {
            var previousText = paginationCtrl.getAttributeValue(attrs.previousText, config.previousText, true), nextText = paginationCtrl.getAttributeValue(attrs.nextText, config.nextText, true), align = paginationCtrl.getAttributeValue(attrs.align, config.align);
            paginationCtrl.init(config.itemsPerPage);
            function makePage(number, text, isDisabled, isPrevious, isNext) {
                return {
                    number: number,
                    text: text,
                    disabled: isDisabled,
                    previous: align && isPrevious,
                    next: align && isNext
                };
            }
            paginationCtrl.getPages = function(currentPage) {
                return [ makePage(currentPage - 1, previousText, paginationCtrl.noPrevious(), true, false), makePage(currentPage + 1, nextText, paginationCtrl.noNext(), false, true) ];
            };
        }
    };
} ]);

angular.module("ui.bootstrap.tooltip", [ "ui.bootstrap.position", "ui.bootstrap.bindHtml" ]).provider("$tooltip", function() {
    var defaultOptions = {
        placement: "top",
        animation: true,
        popupDelay: 0
    };
    var triggerMap = {
        mouseenter: "mouseleave",
        click: "click",
        focus: "blur"
    };
    var globalOptions = {};
    this.options = function(value) {
        angular.extend(globalOptions, value);
    };
    this.setTriggers = function setTriggers(triggers) {
        angular.extend(triggerMap, triggers);
    };
    function snake_case(name) {
        var regexp = /[A-Z]/g;
        var separator = "-";
        return name.replace(regexp, function(letter, pos) {
            return (pos ? separator : "") + letter.toLowerCase();
        });
    }
    this.$get = [ "$window", "$compile", "$timeout", "$parse", "$document", "$position", "$interpolate", function($window, $compile, $timeout, $parse, $document, $position, $interpolate) {
        return function $tooltip(type, prefix, defaultTriggerShow) {
            var options = angular.extend({}, defaultOptions, globalOptions);
            function getTriggers(trigger) {
                var show = trigger || options.trigger || defaultTriggerShow;
                var hide = triggerMap[show] || show;
                return {
                    show: show,
                    hide: hide
                };
            }
            var directiveName = snake_case(type);
            var startSym = $interpolate.startSymbol();
            var endSym = $interpolate.endSymbol();
            var template = "<div " + directiveName + "-popup " + 'title="' + startSym + "tt_title" + endSym + '" ' + 'content="' + startSym + "tt_content" + endSym + '" ' + 'placement="' + startSym + "tt_placement" + endSym + '" ' + 'animation="tt_animation" ' + 'is-open="tt_isOpen"' + ">" + "</div>";
            return {
                restrict: "EA",
                scope: true,
                compile: function(tElem, tAttrs) {
                    var tooltipLinker = $compile(template);
                    return function link(scope, element, attrs) {
                        var tooltip;
                        var transitionTimeout;
                        var popupTimeout;
                        var appendToBody = angular.isDefined(options.appendToBody) ? options.appendToBody : false;
                        var triggers = getTriggers(undefined);
                        var hasRegisteredTriggers = false;
                        var hasEnableExp = angular.isDefined(attrs[prefix + "Enable"]);
                        var positionTooltip = function() {
                            var position, ttWidth, ttHeight, ttPosition;
                            position = appendToBody ? $position.offset(element) : $position.position(element);
                            ttWidth = tooltip.prop("offsetWidth");
                            ttHeight = tooltip.prop("offsetHeight");
                            switch (scope.tt_placement) {
                              case "right":
                                ttPosition = {
                                    top: position.top + position.height / 2 - ttHeight / 2,
                                    left: position.left + position.width
                                };
                                break;

                              case "bottom":
                                ttPosition = {
                                    top: position.top + position.height,
                                    left: position.left + position.width / 2 - ttWidth / 2
                                };
                                break;

                              case "left":
                                ttPosition = {
                                    top: position.top + position.height / 2 - ttHeight / 2,
                                    left: position.left - ttWidth
                                };
                                break;

                              default:
                                ttPosition = {
                                    top: position.top - ttHeight,
                                    left: position.left + position.width / 2 - ttWidth / 2
                                };
                                break;
                            }
                            ttPosition.top += "px";
                            ttPosition.left += "px";
                            tooltip.css(ttPosition);
                        };
                        scope.tt_isOpen = false;
                        function toggleTooltipBind() {
                            if (!scope.tt_isOpen) {
                                showTooltipBind();
                            } else {
                                hideTooltipBind();
                            }
                        }
                        function showTooltipBind() {
                            if (hasEnableExp && !scope.$eval(attrs[prefix + "Enable"])) {
                                return;
                            }
                            if (scope.tt_popupDelay) {
                                popupTimeout = $timeout(show, scope.tt_popupDelay, false);
                                popupTimeout.then(function(reposition) {
                                    reposition();
                                });
                            } else {
                                show()();
                            }
                        }
                        function hideTooltipBind() {
                            scope.$apply(function() {
                                hide();
                            });
                        }
                        function show() {
                            if (!scope.tt_content) {
                                return angular.noop;
                            }
                            createTooltip();
                            if (transitionTimeout) {
                                $timeout.cancel(transitionTimeout);
                            }
                            tooltip.css({
                                top: 0,
                                left: 0,
                                display: "block"
                            });
                            if (appendToBody) {
                                $document.find("body").append(tooltip);
                            } else {
                                element.after(tooltip);
                            }
                            positionTooltip();
                            scope.tt_isOpen = true;
                            scope.$digest();
                            return positionTooltip;
                        }
                        function hide() {
                            scope.tt_isOpen = false;
                            $timeout.cancel(popupTimeout);
                            if (scope.tt_animation) {
                                transitionTimeout = $timeout(removeTooltip, 500);
                            } else {
                                removeTooltip();
                            }
                        }
                        function createTooltip() {
                            if (tooltip) {
                                removeTooltip();
                            }
                            tooltip = tooltipLinker(scope, function() {});
                            scope.$digest();
                        }
                        function removeTooltip() {
                            if (tooltip) {
                                tooltip.remove();
                                tooltip = null;
                            }
                        }
                        attrs.$observe(type, function(val) {
                            scope.tt_content = val;
                            if (!val && scope.tt_isOpen) {
                                hide();
                            }
                        });
                        attrs.$observe(prefix + "Title", function(val) {
                            scope.tt_title = val;
                        });
                        attrs.$observe(prefix + "Placement", function(val) {
                            scope.tt_placement = angular.isDefined(val) ? val : options.placement;
                        });
                        attrs.$observe(prefix + "PopupDelay", function(val) {
                            var delay = parseInt(val, 10);
                            scope.tt_popupDelay = !isNaN(delay) ? delay : options.popupDelay;
                        });
                        var unregisterTriggers = function() {
                            if (hasRegisteredTriggers) {
                                element.unbind(triggers.show, showTooltipBind);
                                element.unbind(triggers.hide, hideTooltipBind);
                            }
                        };
                        attrs.$observe(prefix + "Trigger", function(val) {
                            unregisterTriggers();
                            triggers = getTriggers(val);
                            if (triggers.show === triggers.hide) {
                                element.bind(triggers.show, toggleTooltipBind);
                            } else {
                                element.bind(triggers.show, showTooltipBind);
                                element.bind(triggers.hide, hideTooltipBind);
                            }
                            hasRegisteredTriggers = true;
                        });
                        var animation = scope.$eval(attrs[prefix + "Animation"]);
                        scope.tt_animation = angular.isDefined(animation) ? !!animation : options.animation;
                        attrs.$observe(prefix + "AppendToBody", function(val) {
                            appendToBody = angular.isDefined(val) ? $parse(val)(scope) : appendToBody;
                        });
                        if (appendToBody) {
                            scope.$on("$locationChangeSuccess", function closeTooltipOnLocationChangeSuccess() {
                                if (scope.tt_isOpen) {
                                    hide();
                                }
                            });
                        }
                        scope.$on("$destroy", function onDestroyTooltip() {
                            $timeout.cancel(transitionTimeout);
                            $timeout.cancel(popupTimeout);
                            unregisterTriggers();
                            removeTooltip();
                        });
                    };
                }
            };
        };
    } ];
}).directive("tooltipPopup", function() {
    return {
        restrict: "EA",
        replace: true,
        scope: {
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/tooltip/tooltip-popup.html"
    };
}).directive("tooltip", [ "$tooltip", function($tooltip) {
    return $tooltip("tooltip", "tooltip", "mouseenter");
} ]).directive("tooltipHtmlUnsafePopup", function() {
    return {
        restrict: "EA",
        replace: true,
        scope: {
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/tooltip/tooltip-html-unsafe-popup.html"
    };
}).directive("tooltipHtmlUnsafe", [ "$tooltip", function($tooltip) {
    return $tooltip("tooltipHtmlUnsafe", "tooltip", "mouseenter");
} ]);

angular.module("ui.bootstrap.popover", [ "ui.bootstrap.tooltip" ]).directive("popoverPopup", function() {
    return {
        restrict: "EA",
        replace: true,
        scope: {
            title: "@",
            content: "@",
            placement: "@",
            animation: "&",
            isOpen: "&"
        },
        templateUrl: "template/popover/popover.html"
    };
}).directive("popover", [ "$tooltip", function($tooltip) {
    return $tooltip("popover", "popover", "click");
} ]);

angular.module("ui.bootstrap.progressbar", [ "ui.bootstrap.transition" ]).constant("progressConfig", {
    animate: true,
    max: 100
}).controller("ProgressController", [ "$scope", "$attrs", "progressConfig", "$transition", function($scope, $attrs, progressConfig, $transition) {
    var self = this, bars = [], max = angular.isDefined($attrs.max) ? $scope.$parent.$eval($attrs.max) : progressConfig.max, animate = angular.isDefined($attrs.animate) ? $scope.$parent.$eval($attrs.animate) : progressConfig.animate;
    this.addBar = function(bar, element) {
        var oldValue = 0, index = bar.$parent.$index;
        if (angular.isDefined(index) && bars[index]) {
            oldValue = bars[index].value;
        }
        bars.push(bar);
        this.update(element, bar.value, oldValue);
        bar.$watch("value", function(value, oldValue) {
            if (value !== oldValue) {
                self.update(element, value, oldValue);
            }
        });
        bar.$on("$destroy", function() {
            self.removeBar(bar);
        });
    };
    this.update = function(element, newValue, oldValue) {
        var percent = this.getPercentage(newValue);
        if (animate) {
            element.css("width", this.getPercentage(oldValue) + "%");
            $transition(element, {
                width: percent + "%"
            });
        } else {
            element.css({
                transition: "none",
                width: percent + "%"
            });
        }
    };
    this.removeBar = function(bar) {
        bars.splice(bars.indexOf(bar), 1);
    };
    this.getPercentage = function(value) {
        return Math.round(100 * value / max);
    };
} ]).directive("progress", function() {
    return {
        restrict: "EA",
        replace: true,
        transclude: true,
        controller: "ProgressController",
        require: "progress",
        scope: {},
        template: '<div class="progress" ng-transclude></div>'
    };
}).directive("bar", function() {
    return {
        restrict: "EA",
        replace: true,
        transclude: true,
        require: "^progress",
        scope: {
            value: "=",
            type: "@"
        },
        templateUrl: "template/progressbar/bar.html",
        link: function(scope, element, attrs, progressCtrl) {
            progressCtrl.addBar(scope, element);
        }
    };
}).directive("progressbar", function() {
    return {
        restrict: "EA",
        replace: true,
        transclude: true,
        controller: "ProgressController",
        scope: {
            value: "=",
            type: "@"
        },
        templateUrl: "template/progressbar/progressbar.html",
        link: function(scope, element, attrs, progressCtrl) {
            progressCtrl.addBar(scope, angular.element(element.children()[0]));
        }
    };
});

angular.module("ui.bootstrap.rating", []).constant("ratingConfig", {
    max: 5,
    stateOn: null,
    stateOff: null
}).controller("RatingController", [ "$scope", "$attrs", "$parse", "ratingConfig", function($scope, $attrs, $parse, ratingConfig) {
    this.maxRange = angular.isDefined($attrs.max) ? $scope.$parent.$eval($attrs.max) : ratingConfig.max;
    this.stateOn = angular.isDefined($attrs.stateOn) ? $scope.$parent.$eval($attrs.stateOn) : ratingConfig.stateOn;
    this.stateOff = angular.isDefined($attrs.stateOff) ? $scope.$parent.$eval($attrs.stateOff) : ratingConfig.stateOff;
    this.createRateObjects = function(states) {
        var defaultOptions = {
            stateOn: this.stateOn,
            stateOff: this.stateOff
        };
        for (var i = 0, n = states.length; i < n; i++) {
            states[i] = angular.extend({
                index: i
            }, defaultOptions, states[i]);
        }
        return states;
    };
    $scope.range = angular.isDefined($attrs.ratingStates) ? this.createRateObjects(angular.copy($scope.$parent.$eval($attrs.ratingStates))) : this.createRateObjects(new Array(this.maxRange));
    $scope.rate = function(value) {
        if ($scope.value !== value && !$scope.readonly) {
            $scope.value = value;
        }
    };
    $scope.enter = function(value) {
        if (!$scope.readonly) {
            $scope.val = value;
        }
        $scope.onHover({
            value: value
        });
    };
    $scope.reset = function() {
        $scope.val = angular.copy($scope.value);
        $scope.onLeave();
    };
    $scope.$watch("value", function(value) {
        $scope.val = value;
    });
    $scope.readonly = false;
    if ($attrs.readonly) {
        $scope.$parent.$watch($parse($attrs.readonly), function(value) {
            $scope.readonly = !!value;
        });
    }
} ]).directive("rating", function() {
    return {
        restrict: "EA",
        scope: {
            value: "=",
            onHover: "&",
            onLeave: "&"
        },
        controller: "RatingController",
        templateUrl: "template/rating/rating.html",
        replace: true
    };
});

angular.module("ui.bootstrap.tabs", []).controller("TabsetController", [ "$scope", function TabsetCtrl($scope) {
    var ctrl = this, tabs = ctrl.tabs = $scope.tabs = [];
    ctrl.select = function(tab) {
        angular.forEach(tabs, function(tab) {
            tab.active = false;
        });
        tab.active = true;
    };
    ctrl.addTab = function addTab(tab) {
        tabs.push(tab);
        if (tabs.length === 1 || tab.active) {
            ctrl.select(tab);
        }
    };
    ctrl.removeTab = function removeTab(tab) {
        var index = tabs.indexOf(tab);
        if (tab.active && tabs.length > 1) {
            var newActiveIndex = index == tabs.length - 1 ? index - 1 : index + 1;
            ctrl.select(tabs[newActiveIndex]);
        }
        tabs.splice(index, 1);
    };
} ]).directive("tabset", function() {
    return {
        restrict: "EA",
        transclude: true,
        replace: true,
        scope: {},
        controller: "TabsetController",
        templateUrl: "template/tabs/tabset.html",
        link: function(scope, element, attrs) {
            scope.vertical = angular.isDefined(attrs.vertical) ? scope.$parent.$eval(attrs.vertical) : false;
            scope.justified = angular.isDefined(attrs.justified) ? scope.$parent.$eval(attrs.justified) : false;
            scope.type = angular.isDefined(attrs.type) ? scope.$parent.$eval(attrs.type) : "tabs";
        }
    };
}).directive("tab", [ "$parse", function($parse) {
    return {
        require: "^tabset",
        restrict: "EA",
        replace: true,
        templateUrl: "template/tabs/tab.html",
        transclude: true,
        scope: {
            heading: "@",
            onSelect: "&select",
            onDeselect: "&deselect"
        },
        controller: function() {},
        compile: function(elm, attrs, transclude) {
            return function postLink(scope, elm, attrs, tabsetCtrl) {
                var getActive, setActive;
                if (attrs.active) {
                    getActive = $parse(attrs.active);
                    setActive = getActive.assign;
                    scope.$parent.$watch(getActive, function updateActive(value, oldVal) {
                        if (value !== oldVal) {
                            scope.active = !!value;
                        }
                    });
                    scope.active = getActive(scope.$parent);
                } else {
                    setActive = getActive = angular.noop;
                }
                scope.$watch("active", function(active) {
                    setActive(scope.$parent, active);
                    if (active) {
                        tabsetCtrl.select(scope);
                        scope.onSelect();
                    } else {
                        scope.onDeselect();
                    }
                });
                scope.disabled = false;
                if (attrs.disabled) {
                    scope.$parent.$watch($parse(attrs.disabled), function(value) {
                        scope.disabled = !!value;
                    });
                }
                scope.select = function() {
                    if (!scope.disabled) {
                        scope.active = true;
                    }
                };
                tabsetCtrl.addTab(scope);
                scope.$on("$destroy", function() {
                    tabsetCtrl.removeTab(scope);
                });
                scope.$transcludeFn = transclude;
            };
        }
    };
} ]).directive("tabHeadingTransclude", [ function() {
    return {
        restrict: "A",
        require: "^tab",
        link: function(scope, elm, attrs, tabCtrl) {
            scope.$watch("headingElement", function updateHeadingElement(heading) {
                if (heading) {
                    elm.html("");
                    elm.append(heading);
                }
            });
        }
    };
} ]).directive("tabContentTransclude", function() {
    return {
        restrict: "A",
        require: "^tabset",
        link: function(scope, elm, attrs) {
            var tab = scope.$eval(attrs.tabContentTransclude);
            tab.$transcludeFn(tab.$parent, function(contents) {
                angular.forEach(contents, function(node) {
                    if (isTabHeading(node)) {
                        tab.headingElement = node;
                    } else {
                        elm.append(node);
                    }
                });
            });
        }
    };
    function isTabHeading(node) {
        return node.tagName && (node.hasAttribute("tab-heading") || node.hasAttribute("data-tab-heading") || node.tagName.toLowerCase() === "tab-heading" || node.tagName.toLowerCase() === "data-tab-heading");
    }
});

angular.module("ui.bootstrap.timepicker", []).constant("timepickerConfig", {
    hourStep: 1,
    minuteStep: 1,
    showMeridian: true,
    meridians: null,
    readonlyInput: false,
    mousewheel: true
}).directive("timepicker", [ "$parse", "$log", "timepickerConfig", "$locale", function($parse, $log, timepickerConfig, $locale) {
    return {
        restrict: "EA",
        require: "?^ngModel",
        replace: true,
        scope: {},
        templateUrl: "template/timepicker/timepicker.html",
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) {
                return;
            }
            var selected = new Date(), meridians = angular.isDefined(attrs.meridians) ? scope.$parent.$eval(attrs.meridians) : timepickerConfig.meridians || $locale.DATETIME_FORMATS.AMPMS;
            var hourStep = timepickerConfig.hourStep;
            if (attrs.hourStep) {
                scope.$parent.$watch($parse(attrs.hourStep), function(value) {
                    hourStep = parseInt(value, 10);
                });
            }
            var minuteStep = timepickerConfig.minuteStep;
            if (attrs.minuteStep) {
                scope.$parent.$watch($parse(attrs.minuteStep), function(value) {
                    minuteStep = parseInt(value, 10);
                });
            }
            scope.showMeridian = timepickerConfig.showMeridian;
            if (attrs.showMeridian) {
                scope.$parent.$watch($parse(attrs.showMeridian), function(value) {
                    scope.showMeridian = !!value;
                    if (ngModel.$error.time) {
                        var hours = getHoursFromTemplate(), minutes = getMinutesFromTemplate();
                        if (angular.isDefined(hours) && angular.isDefined(minutes)) {
                            selected.setHours(hours);
                            refresh();
                        }
                    } else {
                        updateTemplate();
                    }
                });
            }
            function getHoursFromTemplate() {
                var hours = parseInt(scope.hours, 10);
                var valid = scope.showMeridian ? hours > 0 && hours < 13 : hours >= 0 && hours < 24;
                if (!valid) {
                    return undefined;
                }
                if (scope.showMeridian) {
                    if (hours === 12) {
                        hours = 0;
                    }
                    if (scope.meridian === meridians[1]) {
                        hours = hours + 12;
                    }
                }
                return hours;
            }
            function getMinutesFromTemplate() {
                var minutes = parseInt(scope.minutes, 10);
                return minutes >= 0 && minutes < 60 ? minutes : undefined;
            }
            function pad(value) {
                return angular.isDefined(value) && value.toString().length < 2 ? "0" + value : value;
            }
            var inputs = element.find("input"), hoursInputEl = inputs.eq(0), minutesInputEl = inputs.eq(1);
            var mousewheel = angular.isDefined(attrs.mousewheel) ? scope.$eval(attrs.mousewheel) : timepickerConfig.mousewheel;
            if (mousewheel) {
                var isScrollingUp = function(e) {
                    if (e.originalEvent) {
                        e = e.originalEvent;
                    }
                    var delta = e.wheelDelta ? e.wheelDelta : -e.deltaY;
                    return e.detail || delta > 0;
                };
                hoursInputEl.bind("mousewheel wheel", function(e) {
                    scope.$apply(isScrollingUp(e) ? scope.incrementHours() : scope.decrementHours());
                    e.preventDefault();
                });
                minutesInputEl.bind("mousewheel wheel", function(e) {
                    scope.$apply(isScrollingUp(e) ? scope.incrementMinutes() : scope.decrementMinutes());
                    e.preventDefault();
                });
            }
            scope.readonlyInput = angular.isDefined(attrs.readonlyInput) ? scope.$eval(attrs.readonlyInput) : timepickerConfig.readonlyInput;
            if (!scope.readonlyInput) {
                var invalidate = function(invalidHours, invalidMinutes) {
                    ngModel.$setViewValue(null);
                    ngModel.$setValidity("time", false);
                    if (angular.isDefined(invalidHours)) {
                        scope.invalidHours = invalidHours;
                    }
                    if (angular.isDefined(invalidMinutes)) {
                        scope.invalidMinutes = invalidMinutes;
                    }
                };
                scope.updateHours = function() {
                    var hours = getHoursFromTemplate();
                    if (angular.isDefined(hours)) {
                        selected.setHours(hours);
                        refresh("h");
                    } else {
                        invalidate(true);
                    }
                };
                hoursInputEl.bind("blur", function(e) {
                    if (!scope.validHours && scope.hours < 10) {
                        scope.$apply(function() {
                            scope.hours = pad(scope.hours);
                        });
                    }
                });
                scope.updateMinutes = function() {
                    var minutes = getMinutesFromTemplate();
                    if (angular.isDefined(minutes)) {
                        selected.setMinutes(minutes);
                        refresh("m");
                    } else {
                        invalidate(undefined, true);
                    }
                };
                minutesInputEl.bind("blur", function(e) {
                    if (!scope.invalidMinutes && scope.minutes < 10) {
                        scope.$apply(function() {
                            scope.minutes = pad(scope.minutes);
                        });
                    }
                });
            } else {
                scope.updateHours = angular.noop;
                scope.updateMinutes = angular.noop;
            }
            ngModel.$render = function() {
                var date = ngModel.$modelValue ? new Date(ngModel.$modelValue) : null;
                if (isNaN(date)) {
                    ngModel.$setValidity("time", false);
                    $log.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.');
                } else {
                    if (date) {
                        selected = date;
                    }
                    makeValid();
                    updateTemplate();
                }
            };
            function refresh(keyboardChange) {
                makeValid();
                ngModel.$setViewValue(new Date(selected));
                updateTemplate(keyboardChange);
            }
            function makeValid() {
                ngModel.$setValidity("time", true);
                scope.invalidHours = false;
                scope.invalidMinutes = false;
            }
            function updateTemplate(keyboardChange) {
                var hours = selected.getHours(), minutes = selected.getMinutes();
                if (scope.showMeridian) {
                    hours = hours === 0 || hours === 12 ? 12 : hours % 12;
                }
                scope.hours = keyboardChange === "h" ? hours : pad(hours);
                scope.minutes = keyboardChange === "m" ? minutes : pad(minutes);
                scope.meridian = selected.getHours() < 12 ? meridians[0] : meridians[1];
            }
            function addMinutes(minutes) {
                var dt = new Date(selected.getTime() + minutes * 6e4);
                selected.setHours(dt.getHours(), dt.getMinutes());
                refresh();
            }
            scope.incrementHours = function() {
                addMinutes(hourStep * 60);
            };
            scope.decrementHours = function() {
                addMinutes(-hourStep * 60);
            };
            scope.incrementMinutes = function() {
                addMinutes(minuteStep);
            };
            scope.decrementMinutes = function() {
                addMinutes(-minuteStep);
            };
            scope.toggleMeridian = function() {
                addMinutes(12 * 60 * (selected.getHours() < 12 ? 1 : -1));
            };
        }
    };
} ]);

angular.module("ui.bootstrap.typeahead", [ "ui.bootstrap.position", "ui.bootstrap.bindHtml" ]).factory("typeaheadParser", [ "$parse", function($parse) {
    var TYPEAHEAD_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;
    return {
        parse: function(input) {
            var match = input.match(TYPEAHEAD_REGEXP), modelMapper, viewMapper, source;
            if (!match) {
                throw new Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_'" + " but got '" + input + "'.");
            }
            return {
                itemName: match[3],
                source: $parse(match[4]),
                viewMapper: $parse(match[2] || match[1]),
                modelMapper: $parse(match[1])
            };
        }
    };
} ]).directive("typeahead", [ "$compile", "$parse", "$q", "$timeout", "$document", "$position", "typeaheadParser", function($compile, $parse, $q, $timeout, $document, $position, typeaheadParser) {
    var HOT_KEYS = [ 9, 13, 27, 38, 40 ];
    return {
        require: "ngModel",
        link: function(originalScope, element, attrs, modelCtrl) {
            var minSearch = originalScope.$eval(attrs.typeaheadMinLength) || 1;
            var waitTime = originalScope.$eval(attrs.typeaheadWaitMs) || 0;
            var isEditable = originalScope.$eval(attrs.typeaheadEditable) !== false;
            var isLoadingSetter = $parse(attrs.typeaheadLoading).assign || angular.noop;
            var onSelectCallback = $parse(attrs.typeaheadOnSelect);
            var inputFormatter = attrs.typeaheadInputFormatter ? $parse(attrs.typeaheadInputFormatter) : undefined;
            var appendToBody = attrs.typeaheadAppendToBody ? $parse(attrs.typeaheadAppendToBody) : false;
            var $setModelValue = $parse(attrs.ngModel).assign;
            var parserResult = typeaheadParser.parse(attrs.typeahead);
            var hasFocus;
            var popUpEl = angular.element("<div typeahead-popup></div>");
            popUpEl.attr({
                matches: "matches",
                active: "activeIdx",
                select: "select(activeIdx)",
                query: "query",
                position: "position"
            });
            if (angular.isDefined(attrs.typeaheadTemplateUrl)) {
                popUpEl.attr("template-url", attrs.typeaheadTemplateUrl);
            }
            var scope = originalScope.$new();
            originalScope.$on("$destroy", function() {
                scope.$destroy();
            });
            var resetMatches = function() {
                scope.matches = [];
                scope.activeIdx = -1;
            };
            var getMatchesAsync = function(inputValue) {
                var locals = {
                    $viewValue: inputValue
                };
                isLoadingSetter(originalScope, true);
                $q.when(parserResult.source(originalScope, locals)).then(function(matches) {
                    if (inputValue === modelCtrl.$viewValue && hasFocus) {
                        if (matches.length > 0) {
                            scope.activeIdx = 0;
                            scope.matches.length = 0;
                            for (var i = 0; i < matches.length; i++) {
                                locals[parserResult.itemName] = matches[i];
                                scope.matches.push({
                                    label: parserResult.viewMapper(scope, locals),
                                    model: matches[i]
                                });
                            }
                            scope.query = inputValue;
                            scope.position = appendToBody ? $position.offset(element) : $position.position(element);
                            scope.position.top = scope.position.top + element.prop("offsetHeight");
                        } else {
                            resetMatches();
                        }
                        isLoadingSetter(originalScope, false);
                    }
                }, function() {
                    resetMatches();
                    isLoadingSetter(originalScope, false);
                });
            };
            resetMatches();
            scope.query = undefined;
            var timeoutPromise;
            modelCtrl.$parsers.unshift(function(inputValue) {
                hasFocus = true;
                if (inputValue && inputValue.length >= minSearch) {
                    if (waitTime > 0) {
                        if (timeoutPromise) {
                            $timeout.cancel(timeoutPromise);
                        }
                        timeoutPromise = $timeout(function() {
                            getMatchesAsync(inputValue);
                        }, waitTime);
                    } else {
                        getMatchesAsync(inputValue);
                    }
                } else {
                    isLoadingSetter(originalScope, false);
                    resetMatches();
                }
                if (isEditable) {
                    return inputValue;
                } else {
                    if (!inputValue) {
                        modelCtrl.$setValidity("editable", true);
                        return inputValue;
                    } else {
                        modelCtrl.$setValidity("editable", false);
                        return undefined;
                    }
                }
            });
            modelCtrl.$formatters.push(function(modelValue) {
                var candidateViewValue, emptyViewValue;
                var locals = {};
                if (inputFormatter) {
                    locals["$model"] = modelValue;
                    return inputFormatter(originalScope, locals);
                } else {
                    locals[parserResult.itemName] = modelValue;
                    candidateViewValue = parserResult.viewMapper(originalScope, locals);
                    locals[parserResult.itemName] = undefined;
                    emptyViewValue = parserResult.viewMapper(originalScope, locals);
                    return candidateViewValue !== emptyViewValue ? candidateViewValue : modelValue;
                }
            });
            scope.select = function(activeIdx) {
                var locals = {};
                var model, item;
                locals[parserResult.itemName] = item = scope.matches[activeIdx].model;
                model = parserResult.modelMapper(originalScope, locals);
                $setModelValue(originalScope, model);
                modelCtrl.$setValidity("editable", true);
                onSelectCallback(originalScope, {
                    $item: item,
                    $model: model,
                    $label: parserResult.viewMapper(originalScope, locals)
                });
                resetMatches();
                element[0].focus();
            };
            element.bind("keydown", function(evt) {
                if (scope.matches.length === 0 || HOT_KEYS.indexOf(evt.which) === -1) {
                    return;
                }
                evt.preventDefault();
                if (evt.which === 40) {
                    scope.activeIdx = (scope.activeIdx + 1) % scope.matches.length;
                    scope.$digest();
                } else if (evt.which === 38) {
                    scope.activeIdx = (scope.activeIdx ? scope.activeIdx : scope.matches.length) - 1;
                    scope.$digest();
                } else if (evt.which === 13 || evt.which === 9) {
                    scope.$apply(function() {
                        scope.select(scope.activeIdx);
                    });
                } else if (evt.which === 27) {
                    evt.stopPropagation();
                    resetMatches();
                    scope.$digest();
                }
            });
            element.bind("blur", function(evt) {
                hasFocus = false;
            });
            var dismissClickHandler = function(evt) {
                if (element[0] !== evt.target) {
                    resetMatches();
                    scope.$digest();
                }
            };
            $document.bind("click", dismissClickHandler);
            originalScope.$on("$destroy", function() {
                $document.unbind("click", dismissClickHandler);
            });
            var $popup = $compile(popUpEl)(scope);
            if (appendToBody) {
                $document.find("body").append($popup);
            } else {
                element.after($popup);
            }
        }
    };
} ]).directive("typeaheadPopup", function() {
    return {
        restrict: "EA",
        scope: {
            matches: "=",
            query: "=",
            active: "=",
            position: "=",
            select: "&"
        },
        replace: true,
        templateUrl: "template/typeahead/typeahead-popup.html",
        link: function(scope, element, attrs) {
            scope.templateUrl = attrs.templateUrl;
            scope.isOpen = function() {
                return scope.matches.length > 0;
            };
            scope.isActive = function(matchIdx) {
                return scope.active == matchIdx;
            };
            scope.selectActive = function(matchIdx) {
                scope.active = matchIdx;
            };
            scope.selectMatch = function(activeIdx) {
                scope.select({
                    activeIdx: activeIdx
                });
            };
        }
    };
}).directive("typeaheadMatch", [ "$http", "$templateCache", "$compile", "$parse", function($http, $templateCache, $compile, $parse) {
    return {
        restrict: "EA",
        scope: {
            index: "=",
            match: "=",
            query: "="
        },
        link: function(scope, element, attrs) {
            var tplUrl = $parse(attrs.templateUrl)(scope.$parent) || "template/typeahead/typeahead-match.html";
            $http.get(tplUrl, {
                cache: $templateCache
            }).success(function(tplContent) {
                element.replaceWith($compile(tplContent.trim())(scope));
            });
        }
    };
} ]).filter("typeaheadHighlight", function() {
    function escapeRegexp(queryToEscape) {
        return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
    }
    return function(matchItem, query) {
        return query ? matchItem.replace(new RegExp(escapeRegexp(query), "gi"), "<strong>$&</strong>") : matchItem;
    };
});

angular.module("template/accordion/accordion-group.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/accordion/accordion-group.html", '<div class="panel panel-default">\n' + '  <div class="panel-heading">\n' + '    <h4 class="panel-title">\n' + '      <a class="accordion-toggle" ng-click="isOpen = !isOpen" accordion-transclude="heading">{{heading}}</a>\n' + "    </h4>\n" + "  </div>\n" + '  <div class="panel-collapse" collapse="!isOpen">\n' + '	  <div class="panel-body" ng-transclude></div>\n' + "  </div>\n" + "</div>");
} ]);

angular.module("template/accordion/accordion.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/accordion/accordion.html", '<div class="panel-group" ng-transclude></div>');
} ]);

angular.module("template/alert/alert.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/alert/alert.html", "<div class='alert' ng-class='\"alert-\" + (type || \"warning\")'>\n" + "    <button ng-show='closeable' type='button' class='close' ng-click='close()'>&times;</button>\n" + "    <div ng-transclude></div>\n" + "</div>\n" + "");
} ]);

angular.module("template/carousel/carousel.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/carousel/carousel.html", '<div ng-mouseenter="pause()" ng-mouseleave="play()" class="carousel">\n' + '    <ol class="carousel-indicators" ng-show="slides().length > 1">\n' + '        <li ng-repeat="slide in slides()" ng-class="{active: isActive(slide)}" ng-click="select(slide)"></li>\n' + "    </ol>\n" + '    <div class="carousel-inner" ng-transclude></div>\n' + '    <a class="left carousel-control" ng-click="prev()" ng-show="slides().length > 1"><span class="icon-prev"></span></a>\n' + '    <a class="right carousel-control" ng-click="next()" ng-show="slides().length > 1"><span class="icon-next"></span></a>\n' + "</div>\n" + "");
} ]);

angular.module("template/carousel/slide.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/carousel/slide.html", '<div ng-class="{\n' + "    'active': leaving || (active && !entering),\n" + "    'prev': (next || active) && direction=='prev',\n" + "    'next': (next || active) && direction=='next',\n" + "    'right': direction=='prev',\n" + "    'left': direction=='next'\n" + '  }" class="item text-center" ng-transclude></div>\n' + "");
} ]);

angular.module("template/datepicker/datepicker.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/datepicker/datepicker.html", "<table>\n" + "  <thead>\n" + "    <tr>\n" + '      <th><button type="button" class="btn btn-default btn-sm pull-left" ng-click="move(-1)"><i class="glyphicon glyphicon-chevron-left"></i></button></th>\n' + '      <th colspan="{{rows[0].length - 2 + showWeekNumbers}}"><button type="button" class="btn btn-default btn-sm btn-block" ng-click="toggleMode()"><strong>{{title}}</strong></button></th>\n' + '      <th><button type="button" class="btn btn-default btn-sm pull-right" ng-click="move(1)"><i class="glyphicon glyphicon-chevron-right"></i></button></th>\n' + "    </tr>\n" + '    <tr ng-show="labels.length > 0" class="h6">\n' + '      <th ng-show="showWeekNumbers" class="text-center">#</th>\n' + '      <th ng-repeat="label in labels" class="text-center">{{label}}</th>\n' + "    </tr>\n" + "  </thead>\n" + "  <tbody>\n" + '    <tr ng-repeat="row in rows">\n' + '      <td ng-show="showWeekNumbers" class="text-center"><em>{{ getWeekNumber(row) }}</em></td>\n' + '      <td ng-repeat="dt in row" class="text-center">\n' + '        <button type="button" style="width:100%;" class="btn btn-default btn-sm" ng-class="{\'btn-info\': dt.selected}" ng-click="select(dt.date)" ng-disabled="dt.disabled"><span ng-class="{\'text-muted\': dt.secondary}">{{dt.label}}</span></button>\n' + "      </td>\n" + "    </tr>\n" + "  </tbody>\n" + "</table>\n" + "");
} ]);

angular.module("template/datepicker/popup.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/datepicker/popup.html", "<ul class=\"dropdown-menu\" ng-style=\"{display: (isOpen && 'block') || 'none', top: position.top+'px', left: position.left+'px'}\">\n" + "	<li ng-transclude></li>\n" + '	<li ng-show="showButtonBar" style="padding:10px 9px 2px">\n' + '		<span class="btn-group">\n' + '			<button type="button" class="btn btn-sm btn-info" ng-click="today()">{{currentText}}</button>\n' + '			<button type="button" class="btn btn-sm btn-default" ng-click="showWeeks = ! showWeeks" ng-class="{active: showWeeks}">{{toggleWeeksText}}</button>\n' + '			<button type="button" class="btn btn-sm btn-danger" ng-click="clear()">{{clearText}}</button>\n' + "		</span>\n" + '		<button type="button" class="btn btn-sm btn-success pull-right" ng-click="isOpen = false">{{closeText}}</button>\n' + "	</li>\n" + "</ul>\n" + "");
} ]);

angular.module("template/modal/backdrop.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/modal/backdrop.html", '<div class="modal-backdrop fade" ng-class="{in: animate}" ng-style="{\'z-index\': 1040 + index*10}"></div>');
} ]);

angular.module("template/modal/window.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/modal/window.html", '<div tabindex="-1" class="modal fade {{ windowClass }}" ng-class="{in: animate}" ng-style="{\'z-index\': 1050 + index*10, display: \'block\'}" ng-click="close($event)">\n' + '    <div class="modal-dialog"><div class="modal-content" ng-transclude></div></div>\n' + "</div>");
} ]);

angular.module("template/pagination/pager.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/pagination/pager.html", '<ul class="pager">\n' + '  <li ng-repeat="page in pages" ng-class="{disabled: page.disabled, previous: page.previous, next: page.next}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n' + "</ul>");
} ]);

angular.module("template/pagination/pagination.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/pagination/pagination.html", '<ul class="pagination">\n' + '  <li ng-repeat="page in pages" ng-class="{active: page.active, disabled: page.disabled}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n' + "</ul>");
} ]);

angular.module("template/tooltip/tooltip-html-unsafe-popup.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/tooltip/tooltip-html-unsafe-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n' + '  <div class="tooltip-arrow"></div>\n' + '  <div class="tooltip-inner" bind-html-unsafe="content"></div>\n' + "</div>\n" + "");
} ]);

angular.module("template/tooltip/tooltip-popup.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/tooltip/tooltip-popup.html", '<div class="tooltip {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n' + '  <div class="tooltip-arrow"></div>\n' + '  <div class="tooltip-inner" ng-bind="content"></div>\n' + "</div>\n" + "");
} ]);

angular.module("template/popover/popover.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/popover/popover.html", '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n' + '  <div class="arrow"></div>\n' + "\n" + '  <div class="popover-inner">\n' + '      <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>\n' + '      <div class="popover-content" ng-bind="content"></div>\n' + "  </div>\n" + "</div>\n" + "");
} ]);

angular.module("template/progressbar/bar.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/progressbar/bar.html", '<div class="progress-bar" ng-class="type && \'progress-bar-\' + type" ng-transclude></div>');
} ]);

angular.module("template/progressbar/progress.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/progressbar/progress.html", '<div class="progress" ng-transclude></div>');
} ]);

angular.module("template/progressbar/progressbar.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/progressbar/progressbar.html", '<div class="progress"><div class="progress-bar" ng-class="type && \'progress-bar-\' + type" ng-transclude></div></div>');
} ]);

angular.module("template/rating/rating.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/rating/rating.html", '<span ng-mouseleave="reset()">\n' + '    <i ng-repeat="r in range" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon" ng-class="$index < val && (r.stateOn || \'glyphicon-star\') || (r.stateOff || \'glyphicon-star-empty\')"></i>\n' + "</span>");
} ]);

angular.module("template/tabs/tab.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/tabs/tab.html", '<li ng-class="{active: active, disabled: disabled}">\n' + '  <a ng-click="select()" tab-heading-transclude>{{heading}}</a>\n' + "</li>\n" + "");
} ]);

angular.module("template/tabs/tabset.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/tabs/tabset.html", "\n" + '<div class="tabbable">\n' + "  <ul class=\"nav {{type && 'nav-' + type}}\" ng-class=\"{'nav-stacked': vertical, 'nav-justified': justified}\" ng-transclude></ul>\n" + '  <div class="tab-content">\n' + '    <div class="tab-pane" \n' + '         ng-repeat="tab in tabs" \n' + '         ng-class="{active: tab.active}"\n' + '         tab-content-transclude="tab">\n' + "    </div>\n" + "  </div>\n" + "</div>\n" + "");
} ]);

angular.module("template/timepicker/timepicker.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/timepicker/timepicker.html", "<table>\n" + "	<tbody>\n" + '		<tr class="text-center">\n' + '			<td><a ng-click="incrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n' + "			<td>&nbsp;</td>\n" + '			<td><a ng-click="incrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-up"></span></a></td>\n' + '			<td ng-show="showMeridian"></td>\n' + "		</tr>\n" + "		<tr>\n" + '			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidHours}">\n' + '				<input type="text" ng-model="hours" ng-change="updateHours()" class="form-control text-center" ng-mousewheel="incrementHours()" ng-readonly="readonlyInput" maxlength="2">\n' + "			</td>\n" + "			<td>:</td>\n" + '			<td style="width:50px;" class="form-group" ng-class="{\'has-error\': invalidMinutes}">\n' + '				<input type="text" ng-model="minutes" ng-change="updateMinutes()" class="form-control text-center" ng-readonly="readonlyInput" maxlength="2">\n' + "			</td>\n" + '			<td ng-show="showMeridian"><button type="button" class="btn btn-default text-center" ng-click="toggleMeridian()">{{meridian}}</button></td>\n' + "		</tr>\n" + '		<tr class="text-center">\n' + '			<td><a ng-click="decrementHours()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n' + "			<td>&nbsp;</td>\n" + '			<td><a ng-click="decrementMinutes()" class="btn btn-link"><span class="glyphicon glyphicon-chevron-down"></span></a></td>\n' + '			<td ng-show="showMeridian"></td>\n' + "		</tr>\n" + "	</tbody>\n" + "</table>\n" + "");
} ]);

angular.module("template/typeahead/typeahead-match.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/typeahead/typeahead-match.html", '<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>');
} ]);

angular.module("template/typeahead/typeahead-popup.html", []).run([ "$templateCache", function($templateCache) {
    $templateCache.put("template/typeahead/typeahead-popup.html", "<ul class=\"dropdown-menu\" ng-style=\"{display: isOpen()&&'block' || 'none', top: position.top+'px', left: position.left+'px'}\">\n" + '    <li ng-repeat="match in matches" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)">\n' + '        <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n' + "    </li>\n" + "</ul>");
} ]);

(function() {
    angular.element(document).ready(function() {
        if (!(google || (typeof google !== "undefined" && google !== null ? google.maps : void 0) || google.maps.InfoWindow != null)) {
            return;
        }
        google.maps.InfoWindow.prototype._open = google.maps.InfoWindow.prototype.open;
        google.maps.InfoWindow.prototype._close = google.maps.InfoWindow.prototype.close;
        google.maps.InfoWindow.prototype._isOpen = false;
        google.maps.InfoWindow.prototype.open = function(map, anchor) {
            this._isOpen = true;
            this._open(map, anchor);
        };
        google.maps.InfoWindow.prototype.close = function() {
            this._isOpen = false;
            this._close();
        };
        google.maps.InfoWindow.prototype.isOpen = function(val) {
            if (val == null) {
                val = void 0;
            }
            if (val == null) {
                return this._isOpen;
            } else {
                return this._isOpen = val;
            }
        };
        if (!window.InfoBox) {
            return;
        }
        window.InfoBox.prototype._open = window.InfoBox.prototype.open;
        window.InfoBox.prototype._close = window.InfoBox.prototype.close;
        window.InfoBox.prototype._isOpen = false;
        window.InfoBox.prototype.open = function(map, anchor) {
            this._isOpen = true;
            this._open(map, anchor);
        };
        window.InfoBox.prototype.close = function() {
            this._isOpen = false;
            this._close();
        };
        return window.InfoBox.prototype.isOpen = function(val) {
            if (val == null) {
                val = void 0;
            }
            if (val == null) {
                return this._isOpen;
            } else {
                return this._isOpen = val;
            }
        };
    });
}).call(this);

(function() {
    _.intersectionObjects = function(array1, array2, comparison) {
        var res, _this = this;
        if (comparison == null) {
            comparison = void 0;
        }
        res = _.map(array1, function(obj1) {
            return _.find(array2, function(obj2) {
                if (comparison != null) {
                    return comparison(obj1, obj2);
                } else {
                    return _.isEqual(obj1, obj2);
                }
            });
        });
        return _.filter(res, function(o) {
            return o != null;
        });
    };
}).call(this);

(function() {
    (function() {
        var app;
        app = angular.module("google-maps", []);
        return app.factory("debounce", [ "$timeout", function($timeout) {
            return function(fn) {
                var nthCall;
                nthCall = 0;
                return function() {
                    var argz, later, that;
                    that = this;
                    argz = arguments;
                    nthCall++;
                    later = function(version) {
                        return function() {
                            if (version === nthCall) {
                                return fn.apply(that, argz);
                            }
                        };
                    }(nthCall);
                    return $timeout(later, 0, true);
                };
            };
        } ]);
    })();
}).call(this);

(function() {
    this.ngGmapModule = function(names, fn) {
        var space, _name;
        if (fn == null) {
            fn = function() {};
        }
        if (typeof names === "string") {
            names = names.split(".");
        }
        space = this[_name = names.shift()] || (this[_name] = {});
        space.ngGmapModule || (space.ngGmapModule = this.ngGmapModule);
        if (names.length) {
            return space.ngGmapModule(names, fn);
        } else {
            return fn.call(space);
        }
    };
}).call(this);

(function() {
    angular.module("google-maps").factory("array-sync", [ "add-events", function(mapEvents) {
        var LatLngArraySync;
        return LatLngArraySync = function(mapArray, scope, pathEval) {
            var isSetFromScope, mapArrayListener, scopeArray, watchListener;
            isSetFromScope = false;
            scopeArray = scope.$eval(pathEval);
            mapArrayListener = mapEvents(mapArray, {
                set_at: function(index) {
                    var value;
                    if (isSetFromScope) {
                        return;
                    }
                    value = mapArray.getAt(index);
                    if (!value) {
                        return;
                    }
                    if (!value.lng || !value.lat) {
                        return;
                    }
                    scopeArray[index].latitude = value.lat();
                    return scopeArray[index].longitude = value.lng();
                },
                insert_at: function(index) {
                    var value;
                    if (isSetFromScope) {
                        return;
                    }
                    value = mapArray.getAt(index);
                    if (!value) {
                        return;
                    }
                    if (!value.lng || !value.lat) {
                        return;
                    }
                    return scopeArray.splice(index, 0, {
                        latitude: value.lat(),
                        longitude: value.lng()
                    });
                },
                remove_at: function(index) {
                    if (isSetFromScope) {
                        return;
                    }
                    return scopeArray.splice(index, 1);
                }
            });
            watchListener = scope.$watchCollection(pathEval, function(newArray) {
                var i, l, newLength, newValue, oldArray, oldLength, oldValue;
                isSetFromScope = true;
                oldArray = mapArray;
                if (newArray) {
                    i = 0;
                    oldLength = oldArray.getLength();
                    newLength = newArray.length;
                    l = Math.min(oldLength, newLength);
                    newValue = void 0;
                    while (i < l) {
                        oldValue = oldArray.getAt(i);
                        newValue = newArray[i];
                        if (oldValue.lat() !== newValue.latitude || oldValue.lng() !== newValue.longitude) {
                            oldArray.setAt(i, new google.maps.LatLng(newValue.latitude, newValue.longitude));
                        }
                        i++;
                    }
                    while (i < newLength) {
                        newValue = newArray[i];
                        oldArray.push(new google.maps.LatLng(newValue.latitude, newValue.longitude));
                        i++;
                    }
                    while (i < oldLength) {
                        oldArray.pop();
                        i++;
                    }
                }
                return isSetFromScope = false;
            });
            return function() {
                if (mapArrayListener) {
                    mapArrayListener();
                    mapArrayListener = null;
                }
                if (watchListener) {
                    watchListener();
                    return watchListener = null;
                }
            };
        };
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").factory("add-events", [ "$timeout", function($timeout) {
        var addEvent, addEvents;
        addEvent = function(target, eventName, handler) {
            return google.maps.event.addListener(target, eventName, function() {
                handler.apply(this, arguments);
                return $timeout(function() {}, true);
            });
        };
        addEvents = function(target, eventName, handler) {
            var remove;
            if (handler) {
                return addEvent(target, eventName, handler);
            }
            remove = [];
            angular.forEach(eventName, function(_handler, key) {
                return remove.push(addEvent(target, key, _handler));
            });
            return function() {
                angular.forEach(remove, function(fn) {
                    if (_.isFunction(fn)) {
                        fn();
                    }
                    if (fn.e !== null && _.isFunction(fn.e)) {
                        return fn.e();
                    }
                });
                return remove = null;
            };
        };
        return addEvents;
    } ]);
}).call(this);

(function() {
    var __indexOf = [].indexOf || function(item) {
        for (var i = 0, l = this.length; i < l; i++) {
            if (i in this && this[i] === item) return i;
        }
        return -1;
    };
    this.ngGmapModule("oo", function() {
        var baseObjectKeywords;
        baseObjectKeywords = [ "extended", "included" ];
        return this.BaseObject = function() {
            function BaseObject() {}
            BaseObject.extend = function(obj) {
                var key, value, _ref;
                for (key in obj) {
                    value = obj[key];
                    if (__indexOf.call(baseObjectKeywords, key) < 0) {
                        this[key] = value;
                    }
                }
                if ((_ref = obj.extended) != null) {
                    _ref.apply(0);
                }
                return this;
            };
            BaseObject.include = function(obj) {
                var key, value, _ref;
                for (key in obj) {
                    value = obj[key];
                    if (__indexOf.call(baseObjectKeywords, key) < 0) {
                        this.prototype[key] = value;
                    }
                }
                if ((_ref = obj.included) != null) {
                    _ref.apply(0);
                }
                return this;
            };
            return BaseObject;
        }();
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.managers", function() {
        return this.ClustererMarkerManager = function(_super) {
            __extends(ClustererMarkerManager, _super);
            function ClustererMarkerManager(gMap, opt_markers, opt_options) {
                this.clear = __bind(this.clear, this);
                this.draw = __bind(this.draw, this);
                this.removeMany = __bind(this.removeMany, this);
                this.remove = __bind(this.remove, this);
                this.addMany = __bind(this.addMany, this);
                this.add = __bind(this.add, this);
                var self;
                ClustererMarkerManager.__super__.constructor.call(this);
                self = this;
                this.opt_options = opt_options;
                if (opt_options != null && opt_markers === void 0) {
                    this.clusterer = new MarkerClusterer(gMap, void 0, opt_options);
                } else if (opt_options != null && opt_markers != null) {
                    this.clusterer = new MarkerClusterer(gMap, opt_markers, opt_options);
                } else {
                    this.clusterer = new MarkerClusterer(gMap);
                }
                this.clusterer.setIgnoreHidden(true);
                this.$log = directives.api.utils.Logger;
                this.noDrawOnSingleAddRemoves = true;
                this.$log.info(this);
            }
            ClustererMarkerManager.prototype.add = function(gMarker) {
                return this.clusterer.addMarker(gMarker, this.noDrawOnSingleAddRemoves);
            };
            ClustererMarkerManager.prototype.addMany = function(gMarkers) {
                return this.clusterer.addMarkers(gMarkers);
            };
            ClustererMarkerManager.prototype.remove = function(gMarker) {
                return this.clusterer.removeMarker(gMarker, this.noDrawOnSingleAddRemoves);
            };
            ClustererMarkerManager.prototype.removeMany = function(gMarkers) {
                return this.clusterer.addMarkers(gMarkers);
            };
            ClustererMarkerManager.prototype.draw = function() {
                return this.clusterer.repaint();
            };
            ClustererMarkerManager.prototype.clear = function() {
                this.clusterer.clearMarkers();
                return this.clusterer.repaint();
            };
            return ClustererMarkerManager;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.managers", function() {
        return this.MarkerManager = function(_super) {
            __extends(MarkerManager, _super);
            function MarkerManager(gMap, opt_markers, opt_options) {
                this.handleOptDraw = __bind(this.handleOptDraw, this);
                this.clear = __bind(this.clear, this);
                this.draw = __bind(this.draw, this);
                this.removeMany = __bind(this.removeMany, this);
                this.remove = __bind(this.remove, this);
                this.addMany = __bind(this.addMany, this);
                this.add = __bind(this.add, this);
                var self;
                MarkerManager.__super__.constructor.call(this);
                self = this;
                this.gMap = gMap;
                this.gMarkers = [];
                this.$log = directives.api.utils.Logger;
                this.$log.info(this);
            }
            MarkerManager.prototype.add = function(gMarker, optDraw) {
                this.handleOptDraw(gMarker, optDraw, true);
                return this.gMarkers.push(gMarker);
            };
            MarkerManager.prototype.addMany = function(gMarkers) {
                var gMarker, _i, _len, _results;
                _results = [];
                for (_i = 0, _len = gMarkers.length; _i < _len; _i++) {
                    gMarker = gMarkers[_i];
                    _results.push(this.add(gMarker));
                }
                return _results;
            };
            MarkerManager.prototype.remove = function(gMarker, optDraw) {
                var index, tempIndex;
                this.handleOptDraw(gMarker, optDraw, false);
                if (!optDraw) {
                    return;
                }
                index = void 0;
                if (this.gMarkers.indexOf != null) {
                    index = this.gMarkers.indexOf(gMarker);
                } else {
                    tempIndex = 0;
                    _.find(this.gMarkers, function(marker) {
                        tempIndex += 1;
                        if (marker === gMarker) {
                            index = tempIndex;
                        }
                    });
                }
                if (index != null) {
                    return this.gMarkers.splice(index, 1);
                }
            };
            MarkerManager.prototype.removeMany = function(gMarkers) {
                var marker, _i, _len, _ref, _results;
                _ref = this.gMarkers;
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    marker = _ref[_i];
                    _results.push(this.remove(marker));
                }
                return _results;
            };
            MarkerManager.prototype.draw = function() {
                var deletes, gMarker, _fn, _i, _j, _len, _len1, _ref, _results, _this = this;
                deletes = [];
                _ref = this.gMarkers;
                _fn = function(gMarker) {
                    if (!gMarker.isDrawn) {
                        if (gMarker.doAdd) {
                            return gMarker.setMap(_this.gMap);
                        } else {
                            return deletes.push(gMarker);
                        }
                    }
                };
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    gMarker = _ref[_i];
                    _fn(gMarker);
                }
                _results = [];
                for (_j = 0, _len1 = deletes.length; _j < _len1; _j++) {
                    gMarker = deletes[_j];
                    _results.push(this.remove(gMarker, true));
                }
                return _results;
            };
            MarkerManager.prototype.clear = function() {
                var gMarker, _i, _len, _ref;
                _ref = this.gMarkers;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    gMarker = _ref[_i];
                    gMarker.setMap(null);
                }
                delete this.gMarkers;
                return this.gMarkers = [];
            };
            MarkerManager.prototype.handleOptDraw = function(gMarker, optDraw, doAdd) {
                if (optDraw === true) {
                    if (doAdd) {
                        gMarker.setMap(this.gMap);
                    } else {
                        gMarker.setMap(null);
                    }
                    return gMarker.isDrawn = true;
                } else {
                    gMarker.isDrawn = false;
                    return gMarker.doAdd = doAdd;
                }
            };
            return MarkerManager;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    this.ngGmapModule("directives.api.utils", function() {
        return this.AsyncProcessor = {
            handleLargeArray: function(array, callback, pausedCallBack, doneCallBack, chunk, index) {
                var doChunk;
                if (chunk == null) {
                    chunk = 100;
                }
                if (index == null) {
                    index = 0;
                }
                if (array === void 0 || array.length <= 0) {
                    doneCallBack();
                    return;
                }
                doChunk = function() {
                    var cnt, i;
                    cnt = chunk;
                    i = index;
                    while (cnt-- && i < array.length) {
                        callback(array[i]);
                        ++i;
                    }
                    if (i < array.length) {
                        index = i;
                        if (pausedCallBack != null) {
                            pausedCallBack();
                        }
                        return setTimeout(doChunk, 1);
                    } else {
                        return doneCallBack();
                    }
                };
                return doChunk();
            }
        };
    });
}).call(this);

(function() {
    this.ngGmapModule("directives.api.utils", function() {
        return this.ChildEvents = {
            onChildCreation: function(child) {}
        };
    });
}).call(this);

(function() {
    this.ngGmapModule("directives.api.utils", function() {
        return this.GmapUtil = {
            getLabelPositionPoint: function(anchor) {
                var xPos, yPos;
                if (anchor === void 0) {
                    return void 0;
                }
                anchor = /^([\d\.]+)\s([\d\.]+)$/.exec(anchor);
                xPos = anchor[1];
                yPos = anchor[2];
                if (xPos && yPos) {
                    return new google.maps.Point(xPos, yPos);
                }
            },
            createMarkerOptions: function(coords, icon, defaults, map) {
                var opts;
                if (map == null) {
                    map = void 0;
                }
                if (defaults == null) {
                    defaults = {};
                }
                opts = angular.extend({}, defaults, {
                    position: defaults.position != null ? defaults.position : new google.maps.LatLng(coords.latitude, coords.longitude),
                    icon: defaults.icon != null ? defaults.icon : icon,
                    visible: defaults.visible != null ? defaults.visible : coords.latitude != null && coords.longitude != null
                });
                if (map != null) {
                    opts.map = map;
                }
                return opts;
            },
            createWindowOptions: function(gMarker, scope, content, defaults) {
                if (content != null && defaults != null) {
                    return angular.extend({}, defaults, {
                        content: defaults.content != null ? defaults.content : content,
                        position: defaults.position != null ? defaults.position : angular.isObject(gMarker) ? gMarker.getPosition() : new google.maps.LatLng(scope.coords.latitude, scope.coords.longitude)
                    });
                } else {
                    if (!defaults) {} else {
                        return defaults;
                    }
                }
            },
            defaultDelay: 50
        };
    });
}).call(this);

(function() {
    var __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.utils", function() {
        return this.Linked = function(_super) {
            __extends(Linked, _super);
            function Linked(scope, element, attrs, ctrls) {
                this.scope = scope;
                this.element = element;
                this.attrs = attrs;
                this.ctrls = ctrls;
            }
            return Linked;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    this.ngGmapModule("directives.api.utils", function() {
        var logger;
        this.Logger = {
            logger: void 0,
            doLog: false,
            info: function(msg) {
                if (logger.doLog) {
                    if (logger.logger != null) {
                        return logger.logger.info(msg);
                    } else {
                        return console.info(msg);
                    }
                }
            },
            error: function(msg) {
                if (logger.doLog) {
                    if (logger.logger != null) {
                        return logger.logger.error(msg);
                    } else {
                        return console.error(msg);
                    }
                }
            }
        };
        return logger = this.Logger;
    });
}).call(this);

(function() {
    this.ngGmapModule("directives.api.utils", function() {
        return this.ModelsWatcher = {
            didModelsChange: function(newValue, oldValue) {
                var didModelsChange, hasIntersectionDiff;
                if (!_.isArray(newValue)) {
                    directives.api.utils.Logger.error("models property must be an array newValue of: " + newValue.toString() + " is not!!");
                    return false;
                }
                if (newValue === oldValue) {
                    return false;
                }
                hasIntersectionDiff = _.intersectionObjects(newValue, oldValue).length !== oldValue.length;
                didModelsChange = true;
                if (!hasIntersectionDiff) {
                    didModelsChange = newValue.length !== oldValue.length;
                }
                return didModelsChange;
            }
        };
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.child", function() {
        return this.MarkerLabelChildModel = function(_super) {
            __extends(MarkerLabelChildModel, _super);
            MarkerLabelChildModel.include(directives.api.utils.GmapUtil);
            function MarkerLabelChildModel(gMarker, opt_options) {
                this.destroy = __bind(this.destroy, this);
                this.draw = __bind(this.draw, this);
                this.setPosition = __bind(this.setPosition, this);
                this.setZIndex = __bind(this.setZIndex, this);
                this.setVisible = __bind(this.setVisible, this);
                this.setAnchor = __bind(this.setAnchor, this);
                this.setMandatoryStyles = __bind(this.setMandatoryStyles, this);
                this.setStyles = __bind(this.setStyles, this);
                this.setContent = __bind(this.setContent, this);
                this.setTitle = __bind(this.setTitle, this);
                this.getSharedCross = __bind(this.getSharedCross, this);
                var self, _ref, _ref1;
                MarkerLabelChildModel.__super__.constructor.call(this);
                self = this;
                this.marker = gMarker;
                this.marker.set("labelContent", opt_options.labelContent);
                this.marker.set("labelAnchor", this.getLabelPositionPoint(opt_options.labelAnchor));
                this.marker.set("labelClass", opt_options.labelClass || "labels");
                this.marker.set("labelStyle", opt_options.labelStyle || {
                    opacity: 100
                });
                this.marker.set("labelInBackground", opt_options.labelInBackground || false);
                if (!opt_options.labelVisible) {
                    this.marker.set("labelVisible", true);
                }
                if (!opt_options.raiseOnDrag) {
                    this.marker.set("raiseOnDrag", true);
                }
                if (!opt_options.clickable) {
                    this.marker.set("clickable", true);
                }
                if (!opt_options.draggable) {
                    this.marker.set("draggable", false);
                }
                if (!opt_options.optimized) {
                    this.marker.set("optimized", false);
                }
                opt_options.crossImage = (_ref = opt_options.crossImage) != null ? _ref : document.location.protocol + "//maps.gstatic.com/intl/en_us/mapfiles/drag_cross_67_16.png";
                opt_options.handCursor = (_ref1 = opt_options.handCursor) != null ? _ref1 : document.location.protocol + "//maps.gstatic.com/intl/en_us/mapfiles/closedhand_8_8.cur";
                this.markerLabel = new MarkerLabel_(this.marker, opt_options.crossImage, opt_options.handCursor);
                this.marker.set("setMap", function(theMap) {
                    google.maps.Marker.prototype.setMap.apply(this, arguments);
                    return self.markerLabel.setMap(theMap);
                });
                this.marker.setMap(this.marker.getMap());
            }
            MarkerLabelChildModel.prototype.getSharedCross = function(crossUrl) {
                return this.markerLabel.getSharedCross(crossUrl);
            };
            MarkerLabelChildModel.prototype.setTitle = function() {
                return this.markerLabel.setTitle();
            };
            MarkerLabelChildModel.prototype.setContent = function() {
                return this.markerLabel.setContent();
            };
            MarkerLabelChildModel.prototype.setStyles = function() {
                return this.markerLabel.setStyles();
            };
            MarkerLabelChildModel.prototype.setMandatoryStyles = function() {
                return this.markerLabel.setMandatoryStyles();
            };
            MarkerLabelChildModel.prototype.setAnchor = function() {
                return this.markerLabel.setAnchor();
            };
            MarkerLabelChildModel.prototype.setVisible = function() {
                return this.markerLabel.setVisible();
            };
            MarkerLabelChildModel.prototype.setZIndex = function() {
                return this.markerLabel.setZIndex();
            };
            MarkerLabelChildModel.prototype.setPosition = function() {
                return this.markerLabel.setPosition();
            };
            MarkerLabelChildModel.prototype.draw = function() {
                return this.markerLabel.draw();
            };
            MarkerLabelChildModel.prototype.destroy = function() {
                if (this.markerLabel.labelDiv_.parentNode != null && this.markerLabel.eventDiv_.parentNode != null) {
                    return this.markerLabel.onRemove();
                }
            };
            return MarkerLabelChildModel;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.child", function() {
        return this.MarkerChildModel = function(_super) {
            __extends(MarkerChildModel, _super);
            MarkerChildModel.include(directives.api.utils.GmapUtil);
            function MarkerChildModel(index, model, parentScope, gMap, $timeout, defaults, doClick, gMarkerManager, $injector) {
                var self, _this = this;
                this.index = index;
                this.model = model;
                this.parentScope = parentScope;
                this.gMap = gMap;
                this.defaults = defaults;
                this.doClick = doClick;
                this.gMarkerManager = gMarkerManager;
                this.watchDestroy = __bind(this.watchDestroy, this);
                this.setLabelOptions = __bind(this.setLabelOptions, this);
                this.isLabelDefined = __bind(this.isLabelDefined, this);
                this.setOptions = __bind(this.setOptions, this);
                this.setIcon = __bind(this.setIcon, this);
                this.setCoords = __bind(this.setCoords, this);
                this.destroy = __bind(this.destroy, this);
                this.maybeSetScopeValue = __bind(this.maybeSetScopeValue, this);
                this.createMarker = __bind(this.createMarker, this);
                this.setMyScope = __bind(this.setMyScope, this);
                self = this;
                this.iconKey = this.parentScope.icon;
                this.coordsKey = this.parentScope.coords;
                this.clickKey = this.parentScope.click();
                this.labelContentKey = this.parentScope.labelContent;
                this.optionsKey = this.parentScope.options;
                this.labelOptionsKey = this.parentScope.labelOptions;
                this.myScope = this.parentScope.$new(false);
                this.$injector = $injector;
                this.myScope.model = this.model;
                this.setMyScope(this.model, void 0, true);
                this.createMarker(this.model);
                this.myScope.$watch("model", function(newValue, oldValue) {
                    if (newValue !== oldValue) {
                        return _this.setMyScope(newValue, oldValue);
                    }
                }, true);
                this.$log = directives.api.utils.Logger;
                this.$log.info(self);
                this.watchDestroy(this.myScope);
            }
            MarkerChildModel.prototype.setMyScope = function(model, oldModel, isInit) {
                var _this = this;
                if (oldModel == null) {
                    oldModel = void 0;
                }
                if (isInit == null) {
                    isInit = false;
                }
                this.maybeSetScopeValue("icon", model, oldModel, this.iconKey, this.evalModelHandle, isInit, this.setIcon);
                this.maybeSetScopeValue("coords", model, oldModel, this.coordsKey, this.evalModelHandle, isInit, this.setCoords);
                this.maybeSetScopeValue("labelContent", model, oldModel, this.labelContentKey, this.evalModelHandle, isInit);
                if (_.isFunction(this.clickKey) && this.$injector) {
                    this.myScope.click = function() {
                        return _this.$injector.invoke(_this.clickKey, void 0, {
                            $markerModel: model
                        });
                    };
                } else {
                    this.maybeSetScopeValue("click", model, oldModel, this.clickKey, this.evalModelHandle, isInit);
                }
                return this.createMarker(model, oldModel, isInit);
            };
            MarkerChildModel.prototype.createMarker = function(model, oldModel, isInit) {
                var _this = this;
                if (oldModel == null) {
                    oldModel = void 0;
                }
                if (isInit == null) {
                    isInit = false;
                }
                return this.maybeSetScopeValue("options", model, oldModel, this.optionsKey, function(lModel, lModelKey) {
                    var value;
                    if (lModel === void 0) {
                        return void 0;
                    }
                    value = lModelKey === "self" ? lModel : lModel[lModelKey];
                    if (value === void 0) {
                        return value = lModelKey === void 0 ? _this.defaults : _this.myScope.options;
                    } else {
                        return value;
                    }
                }, isInit, this.setOptions);
            };
            MarkerChildModel.prototype.evalModelHandle = function(model, modelKey) {
                if (model === void 0) {
                    return void 0;
                }
                if (modelKey === "self") {
                    return model;
                } else {
                    return model[modelKey];
                }
            };
            MarkerChildModel.prototype.maybeSetScopeValue = function(scopePropName, model, oldModel, modelKey, evaluate, isInit, gSetter) {
                var newValue, oldVal;
                if (gSetter == null) {
                    gSetter = void 0;
                }
                if (oldModel === void 0) {
                    this.myScope[scopePropName] = evaluate(model, modelKey);
                    if (!isInit) {
                        if (gSetter != null) {
                            gSetter(this.myScope);
                        }
                    }
                    return;
                }
                oldVal = evaluate(oldModel, modelKey);
                newValue = evaluate(model, modelKey);
                if (newValue !== oldVal && this.myScope[scopePropName] !== newValue) {
                    this.myScope[scopePropName] = newValue;
                    if (!isInit) {
                        if (gSetter != null) {
                            gSetter(this.myScope);
                        }
                        return this.gMarkerManager.draw();
                    }
                }
            };
            MarkerChildModel.prototype.destroy = function() {
                return this.myScope.$destroy();
            };
            MarkerChildModel.prototype.setCoords = function(scope) {
                if (scope.$id !== this.myScope.$id || this.gMarker === void 0) {
                    return;
                }
                if (scope.coords != null) {
                    if (this.scope.coords.latitude == null || this.scope.coords.longitude == null) {
                        this.$log.error("MarkerChildMarker cannot render marker as scope.coords as no position on marker: " + JSON.stringify(this.model));
                        return;
                    }
                    this.gMarker.setPosition(new google.maps.LatLng(scope.coords.latitude, scope.coords.longitude));
                    this.gMarker.setVisible(scope.coords.latitude != null && scope.coords.longitude != null);
                    this.gMarkerManager.remove(this.gMarker);
                    return this.gMarkerManager.add(this.gMarker);
                } else {
                    return this.gMarkerManager.remove(this.gMarker);
                }
            };
            MarkerChildModel.prototype.setIcon = function(scope) {
                if (scope.$id !== this.myScope.$id || this.gMarker === void 0) {
                    return;
                }
                this.gMarkerManager.remove(this.gMarker);
                this.gMarker.setIcon(scope.icon);
                this.gMarkerManager.add(this.gMarker);
                this.gMarker.setPosition(new google.maps.LatLng(scope.coords.latitude, scope.coords.longitude));
                return this.gMarker.setVisible(scope.coords.latitude && scope.coords.longitude != null);
            };
            MarkerChildModel.prototype.setOptions = function(scope) {
                var _ref, _this = this;
                if (scope.$id !== this.myScope.$id) {
                    return;
                }
                if (this.gMarker != null) {
                    this.gMarkerManager.remove(this.gMarker);
                    delete this.gMarker;
                }
                if (!((_ref = scope.coords) != null ? _ref : typeof scope.icon === "function" ? scope.icon(scope.options != null) : void 0)) {
                    return;
                }
                this.opts = this.createMarkerOptions(scope.coords, scope.icon, scope.options);
                delete this.gMarker;
                if (this.isLabelDefined(scope)) {
                    this.gMarker = new MarkerWithLabel(this.setLabelOptions(this.opts, scope));
                } else {
                    this.gMarker = new google.maps.Marker(this.opts);
                }
                this.gMarkerManager.add(this.gMarker);
                return google.maps.event.addListener(this.gMarker, "click", function() {
                    if (_this.doClick && _this.myScope.click != null) {
                        return _this.myScope.click();
                    }
                });
            };
            MarkerChildModel.prototype.isLabelDefined = function(scope) {
                return scope.labelContent != null;
            };
            MarkerChildModel.prototype.setLabelOptions = function(opts, scope) {
                opts.labelAnchor = this.getLabelPositionPoint(scope.labelAnchor);
                opts.labelClass = scope.labelClass;
                opts.labelContent = scope.labelContent;
                return opts;
            };
            MarkerChildModel.prototype.watchDestroy = function(scope) {
                var _this = this;
                return scope.$on("$destroy", function() {
                    var self;
                    if (_this.gMarker != null) {
                        _this.gMarkerManager.remove(_this.gMarker);
                        delete _this.gMarker;
                    }
                    return self = void 0;
                });
            };
            return MarkerChildModel;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.child", function() {
        return this.WindowChildModel = function(_super) {
            __extends(WindowChildModel, _super);
            WindowChildModel.include(directives.api.utils.GmapUtil);
            function WindowChildModel(scope, opts, isIconVisibleOnClick, mapCtrl, markerCtrl, $http, $templateCache, $compile, element, needToManualDestroy) {
                this.element = element;
                if (needToManualDestroy == null) {
                    needToManualDestroy = false;
                }
                this.destroy = __bind(this.destroy, this);
                this.hideWindow = __bind(this.hideWindow, this);
                this.getLatestPosition = __bind(this.getLatestPosition, this);
                this.showWindow = __bind(this.showWindow, this);
                this.handleClick = __bind(this.handleClick, this);
                this.watchCoords = __bind(this.watchCoords, this);
                this.watchShow = __bind(this.watchShow, this);
                this.createGWin = __bind(this.createGWin, this);
                this.scope = scope;
                this.googleMapsHandles = [];
                this.opts = opts;
                this.mapCtrl = mapCtrl;
                this.markerCtrl = markerCtrl;
                this.isIconVisibleOnClick = isIconVisibleOnClick;
                this.initialMarkerVisibility = this.markerCtrl != null ? this.markerCtrl.getVisible() : false;
                this.$log = directives.api.utils.Logger;
                this.$http = $http;
                this.$templateCache = $templateCache;
                this.$compile = $compile;
                this.createGWin();
                if (this.markerCtrl != null) {
                    this.markerCtrl.setClickable(true);
                }
                this.handleClick();
                this.watchShow();
                this.watchCoords();
                this.needToManualDestroy = needToManualDestroy;
                this.$log.info(this);
            }
            WindowChildModel.prototype.createGWin = function() {
                var defaults, html, _this = this;
                if (this.gWin == null && this.markerCtrl != null) {
                    defaults = this.opts != null ? this.opts : {};
                    html = this.element != null && _.isFunction(this.element.html) ? this.element.html() : this.element;
                    this.opts = this.markerCtrl != null ? this.createWindowOptions(this.markerCtrl, this.scope, html, defaults) : {};
                }
                if (this.opts != null && this.gWin === void 0) {
                    if (this.opts.boxClass && (window.InfoBox && typeof window.InfoBox === "function")) {
                        this.gWin = new window.InfoBox(this.opts);
                    } else {
                        this.gWin = new google.maps.InfoWindow(this.opts);
                    }
                    return this.googleMapsHandles.push(google.maps.event.addListener(this.gWin, "closeclick", function() {
                        if (_this.markerCtrl != null) {
                            _this.markerCtrl.setVisible(_this.initialMarkerVisibility);
                        }
                        _this.gWin.isOpen(false);
                        if (_this.scope.closeClick != null) {
                            return _this.scope.closeClick();
                        }
                    }));
                }
            };
            WindowChildModel.prototype.watchShow = function() {
                var _this = this;
                return this.scope.$watch("show", function(newValue, oldValue) {
                    if (newValue) {
                        return _this.showWindow();
                    } else {
                        return _this.hideWindow();
                    }
                });
            };
            WindowChildModel.prototype.watchCoords = function() {
                var scope, _this = this;
                scope = this.markerCtrl != null ? this.scope.$parent : this.scope;
                return scope.$watch("coords", function(newValue, oldValue) {
                    if (newValue !== oldValue) {
                        if (newValue == null) {
                            return _this.hideWindow();
                        } else {
                            if (newValue.latitude == null || newValue.longitude == null) {
                                _this.$log.error("WindowChildMarker cannot render marker as scope.coords as no position on marker: " + JSON.stringify(_this.model));
                                return;
                            }
                            return _this.gWin.setPosition(new google.maps.LatLng(newValue.latitude, newValue.longitude));
                        }
                    }
                }, true);
            };
            WindowChildModel.prototype.handleClick = function() {
                var _this = this;
                if (this.markerCtrl != null) {
                    return this.googleMapsHandles.push(google.maps.event.addListener(this.markerCtrl, "click", function() {
                        var pos;
                        if (_this.gWin == null) {
                            _this.createGWin();
                        }
                        pos = _this.markerCtrl.getPosition();
                        if (_this.gWin != null) {
                            _this.gWin.setPosition(pos);
                            _this.showWindow();
                        }
                        _this.initialMarkerVisibility = _this.markerCtrl.getVisible();
                        return _this.markerCtrl.setVisible(_this.isIconVisibleOnClick);
                    }));
                }
            };
            WindowChildModel.prototype.showWindow = function() {
                var show, _this = this;
                show = function() {
                    if (_this.gWin) {
                        if ((_this.scope.show || _this.scope.show == null) && !_this.gWin.isOpen()) {
                            return _this.gWin.open(_this.mapCtrl);
                        }
                    }
                };
                if (this.scope.templateUrl) {
                    if (this.gWin) {
                        return this.$http.get(this.scope.templateUrl, {
                            cache: this.$templateCache
                        }).then(function(content) {
                            var compiled, templateScope;
                            templateScope = _this.scope.$new();
                            if (angular.isDefined(_this.scope.templateParameter)) {
                                templateScope.parameter = _this.scope.templateParameter;
                            }
                            compiled = _this.$compile(content.data)(templateScope);
                            _this.gWin.setContent(compiled[0]);
                            return show();
                        });
                    }
                } else {
                    return show();
                }
            };
            WindowChildModel.prototype.getLatestPosition = function() {
                if (this.gWin != null && this.markerCtrl != null) {
                    return this.gWin.setPosition(this.markerCtrl.getPosition());
                }
            };
            WindowChildModel.prototype.hideWindow = function() {
                if (this.gWin != null && this.gWin.isOpen()) {
                    return this.gWin.close();
                }
            };
            WindowChildModel.prototype.destroy = function() {
                var self;
                this.hideWindow();
                _.each(this.googleMapsHandles, function(h) {
                    return google.maps.event.removeListener(h);
                });
                this.googleMapsHandles.length = 0;
                if (this.scope != null && this.needToManualDestroy) {
                    this.scope.$destroy();
                }
                delete this.gWin;
                return self = void 0;
            };
            return WindowChildModel;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.parent", function() {
        return this.IMarkerParentModel = function(_super) {
            __extends(IMarkerParentModel, _super);
            IMarkerParentModel.prototype.DEFAULTS = {};
            IMarkerParentModel.prototype.isFalse = function(value) {
                return [ "false", "FALSE", 0, "n", "N", "no", "NO" ].indexOf(value) !== -1;
            };
            function IMarkerParentModel(scope, element, attrs, mapCtrl, $timeout) {
                var self, _this = this;
                this.scope = scope;
                this.element = element;
                this.attrs = attrs;
                this.mapCtrl = mapCtrl;
                this.$timeout = $timeout;
                this.linkInit = __bind(this.linkInit, this);
                this.onDestroy = __bind(this.onDestroy, this);
                this.onWatch = __bind(this.onWatch, this);
                this.watch = __bind(this.watch, this);
                this.validateScope = __bind(this.validateScope, this);
                this.onTimeOut = __bind(this.onTimeOut, this);
                self = this;
                this.$log = directives.api.utils.Logger;
                if (!this.validateScope(scope)) {
                    return;
                }
                this.doClick = angular.isDefined(attrs.click);
                if (scope.options != null) {
                    this.DEFAULTS = scope.options;
                }
                this.$timeout(function() {
                    _this.watch("coords", scope);
                    _this.watch("icon", scope);
                    _this.watch("options", scope);
                    _this.onTimeOut(scope);
                    return scope.$on("$destroy", function() {
                        return _this.onDestroy(scope);
                    });
                });
            }
            IMarkerParentModel.prototype.onTimeOut = function(scope) {};
            IMarkerParentModel.prototype.validateScope = function(scope) {
                var ret;
                if (scope == null) {
                    return false;
                }
                ret = scope.coords != null;
                if (!ret) {
                    this.$log.error(this.constructor.name + ": no valid coords attribute found");
                }
                return ret;
            };
            IMarkerParentModel.prototype.watch = function(propNameToWatch, scope) {
                var _this = this;
                return scope.$watch(propNameToWatch, function(newValue, oldValue) {
                    if (newValue !== oldValue) {
                        return _this.onWatch(propNameToWatch, scope, newValue, oldValue);
                    }
                }, true);
            };
            IMarkerParentModel.prototype.onWatch = function(propNameToWatch, scope, newValue, oldValue) {
                throw new Exception("Not Implemented!!");
            };
            IMarkerParentModel.prototype.onDestroy = function(scope) {
                throw new Exception("Not Implemented!!");
            };
            IMarkerParentModel.prototype.linkInit = function(element, mapCtrl, scope, animate) {
                throw new Exception("Not Implemented!!");
            };
            return IMarkerParentModel;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.parent", function() {
        return this.IWindowParentModel = function(_super) {
            __extends(IWindowParentModel, _super);
            IWindowParentModel.include(directives.api.utils.GmapUtil);
            IWindowParentModel.prototype.DEFAULTS = {};
            function IWindowParentModel(scope, element, attrs, ctrls, $timeout, $compile, $http, $templateCache) {
                var self;
                self = this;
                this.$log = directives.api.utils.Logger;
                this.$timeout = $timeout;
                this.$compile = $compile;
                this.$http = $http;
                this.$templateCache = $templateCache;
                if (scope.options != null) {
                    this.DEFAULTS = scope.options;
                }
            }
            return IWindowParentModel;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.parent", function() {
        return this.LayerParentModel = function(_super) {
            __extends(LayerParentModel, _super);
            function LayerParentModel(scope, element, attrs, mapCtrl, $timeout, onLayerCreated, $log) {
                var _this = this;
                this.scope = scope;
                this.element = element;
                this.attrs = attrs;
                this.mapCtrl = mapCtrl;
                this.$timeout = $timeout;
                this.onLayerCreated = onLayerCreated != null ? onLayerCreated : void 0;
                this.$log = $log != null ? $log : directives.api.utils.Logger;
                this.createGoogleLayer = __bind(this.createGoogleLayer, this);
                if (this.attrs.type == null) {
                    this.$log.info("type attribute for the layer directive is mandatory. Layer creation aborted!!");
                    return;
                }
                this.createGoogleLayer();
                this.gMap = void 0;
                this.doShow = true;
                this.$timeout(function() {
                    _this.gMap = mapCtrl.getMap();
                    if (angular.isDefined(_this.attrs.show)) {
                        _this.doShow = _this.scope.show;
                    }
                    if (_this.doShow !== null && _this.doShow && _this.gMap !== null) {
                        _this.layer.setMap(_this.gMap);
                    }
                    _this.scope.$watch("show", function(newValue, oldValue) {
                        if (newValue !== oldValue) {
                            _this.doShow = newValue;
                            if (newValue) {
                                return _this.layer.setMap(_this.gMap);
                            } else {
                                return _this.layer.setMap(null);
                            }
                        }
                    }, true);
                    _this.scope.$watch("options", function(newValue, oldValue) {
                        if (newValue !== oldValue) {
                            _this.layer.setMap(null);
                            _this.layer = null;
                            return _this.createGoogleLayer();
                        }
                    }, true);
                    return _this.scope.$on("$destroy", function() {
                        return _this.layer.setMap(null);
                    });
                });
            }
            LayerParentModel.prototype.createGoogleLayer = function() {
                var _this = this;
                if (this.attrs.options == null) {
                    this.layer = this.attrs.namespace === void 0 ? new google.maps[this.attrs.type]() : new google.maps[this.attrs.namespace][this.attrs.type]();
                } else {
                    this.layer = this.attrs.namespace === void 0 ? new google.maps[this.attrs.type](this.scope.options) : new google.maps[this.attrs.namespace][this.attrs.type](this.scope.options);
                }
                return this.$timeout(function() {
                    var fn;
                    if (_this.layer != null && _this.onLayerCreated != null) {
                        fn = _this.onLayerCreated(_this.scope, _this.layer);
                        if (fn) {
                            return fn(_this.layer);
                        }
                    }
                });
            };
            return LayerParentModel;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.parent", function() {
        return this.MarkerParentModel = function(_super) {
            __extends(MarkerParentModel, _super);
            MarkerParentModel.include(directives.api.utils.GmapUtil);
            function MarkerParentModel(scope, element, attrs, mapCtrl, $timeout) {
                this.onDestroy = __bind(this.onDestroy, this);
                this.onWatch = __bind(this.onWatch, this);
                this.onTimeOut = __bind(this.onTimeOut, this);
                var self;
                MarkerParentModel.__super__.constructor.call(this, scope, element, attrs, mapCtrl, $timeout);
                self = this;
            }
            MarkerParentModel.prototype.onTimeOut = function(scope) {
                var opts, _this = this;
                opts = this.createMarkerOptions(scope.coords, scope.icon, scope.options, this.mapCtrl.getMap());
                this.scope.gMarker = new google.maps.Marker(opts);
                google.maps.event.addListener(this.scope.gMarker, "click", function() {
                    if (_this.doClick && scope.click != null) {
                        return _this.$timeout(function() {
                            return _this.scope.click();
                        });
                    }
                });
                this.setEvents(this.scope.gMarker, scope);
                return this.$log.info(this);
            };
            MarkerParentModel.prototype.onWatch = function(propNameToWatch, scope) {
                switch (propNameToWatch) {
                  case "coords":
                    if (scope.coords != null && this.scope.gMarker != null) {
                        this.scope.gMarker.setMap(this.mapCtrl.getMap());
                        this.scope.gMarker.setPosition(new google.maps.LatLng(scope.coords.latitude, scope.coords.longitude));
                        this.scope.gMarker.setVisible(scope.coords.latitude != null && scope.coords.longitude != null);
                        return this.scope.gMarker.setOptions(scope.options);
                    } else {
                        return this.scope.gMarker.setMap(null);
                    }
                    break;

                  case "icon":
                    if (scope.icon != null && scope.coords != null && this.scope.gMarker != null) {
                        this.scope.gMarker.setOptions(scope.options);
                        this.scope.gMarker.setIcon(scope.icon);
                        this.scope.gMarker.setMap(null);
                        this.scope.gMarker.setMap(this.mapCtrl.getMap());
                        this.scope.gMarker.setPosition(new google.maps.LatLng(scope.coords.latitude, scope.coords.longitude));
                        return this.scope.gMarker.setVisible(scope.coords.latitude && scope.coords.longitude != null);
                    }
                    break;

                  case "options":
                    if (scope.coords != null && scope.icon != null && scope.options) {
                        if (this.scope.gMarker != null) {
                            this.scope.gMarker.setMap(null);
                        }
                        delete this.scope.gMarker;
                        return this.scope.gMarker = new google.maps.Marker(this.createMarkerOptions(scope.coords, scope.icon, scope.options, this.mapCtrl.getMap()));
                    }
                }
            };
            MarkerParentModel.prototype.onDestroy = function(scope) {
                var self;
                if (this.scope.gMarker === void 0) {
                    self = void 0;
                    return;
                }
                this.scope.gMarker.setMap(null);
                delete this.scope.gMarker;
                return self = void 0;
            };
            MarkerParentModel.prototype.setEvents = function(marker, scope) {
                if (angular.isDefined(scope.events) && scope.events != null && angular.isObject(scope.events)) {
                    return _.compact(_.each(scope.events, function(eventHandler, eventName) {
                        if (scope.events.hasOwnProperty(eventName) && angular.isFunction(scope.events[eventName])) {
                            return google.maps.event.addListener(marker, eventName, function() {
                                return eventHandler.apply(scope, [ marker, eventName, arguments ]);
                            });
                        }
                    }));
                }
            };
            return MarkerParentModel;
        }(directives.api.models.parent.IMarkerParentModel);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.parent", function() {
        return this.MarkersParentModel = function(_super) {
            __extends(MarkersParentModel, _super);
            MarkersParentModel.include(directives.api.utils.ModelsWatcher);
            function MarkersParentModel(scope, element, attrs, mapCtrl, $timeout, $injector) {
                this.fit = __bind(this.fit, this);
                this.onDestroy = __bind(this.onDestroy, this);
                this.onWatch = __bind(this.onWatch, this);
                this.reBuildMarkers = __bind(this.reBuildMarkers, this);
                this.createMarkers = __bind(this.createMarkers, this);
                this.validateScope = __bind(this.validateScope, this);
                this.onTimeOut = __bind(this.onTimeOut, this);
                var self;
                MarkersParentModel.__super__.constructor.call(this, scope, element, attrs, mapCtrl, $timeout, $injector);
                self = this;
                this.markersIndex = 0;
                this.gMarkerManager = void 0;
                this.scope = scope;
                this.scope.markerModels = [];
                this.bigGulp = directives.api.utils.AsyncProcessor;
                this.$timeout = $timeout;
                this.$injector = $injector;
                this.$log.info(this);
            }
            MarkersParentModel.prototype.onTimeOut = function(scope) {
                this.watch("models", scope);
                this.watch("doCluster", scope);
                this.watch("clusterOptions", scope);
                this.watch("fit", scope);
                return this.createMarkers(scope);
            };
            MarkersParentModel.prototype.validateScope = function(scope) {
                var modelsNotDefined;
                modelsNotDefined = angular.isUndefined(scope.models) || scope.models === void 0;
                if (modelsNotDefined) {
                    this.$log.error(this.constructor.name + ": no valid models attribute found");
                }
                return MarkersParentModel.__super__.validateScope.call(this, scope) || modelsNotDefined;
            };
            MarkersParentModel.prototype.createMarkers = function(scope) {
                var markers, _this = this;
                if (scope.doCluster != null && scope.doCluster === true) {
                    if (scope.clusterOptions != null) {
                        if (this.gMarkerManager === void 0) {
                            this.gMarkerManager = new directives.api.managers.ClustererMarkerManager(this.mapCtrl.getMap(), void 0, scope.clusterOptions);
                        } else {
                            if (this.gMarkerManager.opt_options !== scope.clusterOptions) {
                                this.gMarkerManager = new directives.api.managers.ClustererMarkerManager(this.mapCtrl.getMap(), void 0, scope.clusterOptions);
                            }
                        }
                    } else {
                        this.gMarkerManager = new directives.api.managers.ClustererMarkerManager(this.mapCtrl.getMap());
                    }
                } else {
                    this.gMarkerManager = new directives.api.managers.MarkerManager(this.mapCtrl.getMap());
                }
                markers = [];
                scope.isMarkerModelsReady = false;
                return this.bigGulp.handleLargeArray(scope.models, function(model) {
                    var child;
                    scope.doRebuild = true;
                    child = new directives.api.models.child.MarkerChildModel(_this.markersIndex, model, scope, _this.mapCtrl, _this.$timeout, _this.DEFAULTS, _this.doClick, _this.gMarkerManager, _this.$injector);
                    _this.$log.info("child", child, "markers", markers);
                    markers.push(child);
                    return _this.markersIndex++;
                }, function() {}, function() {
                    _this.gMarkerManager.draw();
                    scope.markerModels = markers;
                    if (angular.isDefined(_this.attrs.fit) && scope.fit != null && scope.fit) {
                        _this.fit();
                    }
                    scope.isMarkerModelsReady = true;
                    if (scope.onMarkerModelsReady != null) {
                        return scope.onMarkerModelsReady(scope);
                    }
                });
            };
            MarkersParentModel.prototype.reBuildMarkers = function(scope) {
                var _this = this;
                if (!scope.doRebuild && scope.doRebuild !== void 0) {
                    return;
                }
                _.each(scope.markerModels, function(oldM) {
                    return oldM.destroy();
                });
                this.markersIndex = 0;
                if (this.gMarkerManager != null) {
                    this.gMarkerManager.clear();
                }
                return this.createMarkers(scope);
            };
            MarkersParentModel.prototype.onWatch = function(propNameToWatch, scope, newValue, oldValue) {
                if (propNameToWatch === "models") {
                    if (!this.didModelsChange(newValue, oldValue)) {
                        return;
                    }
                }
                if (propNameToWatch === "options" && newValue != null) {
                    this.DEFAULTS = newValue;
                    return;
                }
                return this.reBuildMarkers(scope);
            };
            MarkersParentModel.prototype.onDestroy = function(scope) {
                var model, _i, _len, _ref;
                _ref = scope.markerModels;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    model = _ref[_i];
                    model.destroy();
                }
                if (this.gMarkerManager != null) {
                    return this.gMarkerManager.clear();
                }
            };
            MarkersParentModel.prototype.fit = function() {
                var bounds, everSet, _this = this;
                if (this.mapCtrl && this.scope.markerModels != null && this.scope.markerModels.length > 0) {
                    bounds = new google.maps.LatLngBounds();
                    everSet = false;
                    _.each(this.scope.markerModels, function(childModelMarker) {
                        if (childModelMarker.gMarker != null) {
                            if (!everSet) {
                                everSet = true;
                            }
                            return bounds.extend(childModelMarker.gMarker.getPosition());
                        }
                    });
                    if (everSet) {
                        return this.mapCtrl.getMap().fitBounds(bounds);
                    }
                }
            };
            return MarkersParentModel;
        }(directives.api.models.parent.IMarkerParentModel);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api.models.parent", function() {
        return this.WindowsParentModel = function(_super) {
            __extends(WindowsParentModel, _super);
            WindowsParentModel.include(directives.api.utils.ModelsWatcher);
            function WindowsParentModel(scope, element, attrs, ctrls, $timeout, $compile, $http, $templateCache, $interpolate) {
                this.interpolateContent = __bind(this.interpolateContent, this);
                this.setChildScope = __bind(this.setChildScope, this);
                this.createWindow = __bind(this.createWindow, this);
                this.setContentKeys = __bind(this.setContentKeys, this);
                this.createChildScopesWindows = __bind(this.createChildScopesWindows, this);
                this.onMarkerModelsReady = __bind(this.onMarkerModelsReady, this);
                this.watchOurScope = __bind(this.watchOurScope, this);
                this.destroy = __bind(this.destroy, this);
                this.watchDestroy = __bind(this.watchDestroy, this);
                this.watchModels = __bind(this.watchModels, this);
                this.watch = __bind(this.watch, this);
                var name, self, _i, _len, _ref, _this = this;
                WindowsParentModel.__super__.constructor.call(this, scope, element, attrs, ctrls, $timeout, $compile, $http, $templateCache, $interpolate);
                self = this;
                this.$interpolate = $interpolate;
                this.windows = [];
                this.windwsIndex = 0;
                this.scopePropNames = [ "show", "coords", "templateUrl", "templateParameter", "isIconVisibleOnClick", "closeClick" ];
                _ref = this.scopePropNames;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    name = _ref[_i];
                    this[name + "Key"] = void 0;
                }
                this.linked = new directives.api.utils.Linked(scope, element, attrs, ctrls);
                this.models = void 0;
                this.contentKeys = void 0;
                this.isIconVisibleOnClick = void 0;
                this.firstTime = true;
                this.bigGulp = directives.api.utils.AsyncProcessor;
                this.$log.info(self);
                this.$timeout(function() {
                    _this.watchOurScope(scope);
                    return _this.createChildScopesWindows();
                }, 50);
            }
            WindowsParentModel.prototype.watch = function(scope, name, nameKey) {
                var _this = this;
                return scope.$watch(name, function(newValue, oldValue) {
                    if (newValue !== oldValue) {
                        _this[nameKey] = typeof newValue === "function" ? newValue() : newValue;
                        return _.each(_this.windows, function(model) {
                            return model.scope[name] = _this[nameKey] === "self" ? model : model[_this[nameKey]];
                        });
                    }
                }, true);
            };
            WindowsParentModel.prototype.watchModels = function(scope) {
                var _this = this;
                return scope.$watch("models", function(newValue, oldValue) {
                    if (_this.didModelsChange(newValue, oldValue)) {
                        _this.destroy();
                        return _this.createChildScopesWindows();
                    }
                });
            };
            WindowsParentModel.prototype.watchDestroy = function(scope) {
                var _this = this;
                return scope.$on("$destroy", function() {
                    return _this.destroy();
                });
            };
            WindowsParentModel.prototype.destroy = function() {
                var _this = this;
                _.each(this.windows, function(model) {
                    return model.destroy();
                });
                delete this.windows;
                this.windows = [];
                return this.windowsIndex = 0;
            };
            WindowsParentModel.prototype.watchOurScope = function(scope) {
                var _this = this;
                return _.each(this.scopePropNames, function(name) {
                    var nameKey;
                    nameKey = name + "Key";
                    _this[nameKey] = typeof scope[name] === "function" ? scope[name]() : scope[name];
                    return _this.watch(scope, name, nameKey);
                });
            };
            WindowsParentModel.prototype.onMarkerModelsReady = function(scope) {
                var _this = this;
                this.destroy();
                this.models = scope.models;
                if (this.firstTime) {
                    this.watchDestroy(scope);
                }
                this.setContentKeys(scope.models);
                return this.bigGulp.handleLargeArray(scope.markerModels, function(mm) {
                    return _this.createWindow(mm.model, mm.gMarker, _this.gMap);
                }, function() {}, function() {
                    return _this.firstTime = false;
                });
            };
            WindowsParentModel.prototype.createChildScopesWindows = function() {
                var markersScope, modelsNotDefined, _this = this;
                this.isIconVisibleOnClick = true;
                if (angular.isDefined(this.linked.attrs.isiconvisibleonclick)) {
                    this.isIconVisibleOnClick = this.linked.scope.isIconVisibleOnClick;
                }
                this.gMap = this.linked.ctrls[0].getMap();
                markersScope = this.linked.ctrls.length > 1 && this.linked.ctrls[1] != null ? this.linked.ctrls[1].getMarkersScope() : void 0;
                modelsNotDefined = angular.isUndefined(this.linked.scope.models);
                if (modelsNotDefined && (markersScope === void 0 || markersScope.markerModels === void 0 && markersScope.models === void 0)) {
                    this.$log.info("No models to create windows from! Need direct models or models derrived from markers!");
                    return;
                }
                if (this.gMap != null) {
                    if (this.linked.scope.models != null) {
                        this.models = this.linked.scope.models;
                        if (this.firstTime) {
                            this.watchModels(this.linked.scope);
                            this.watchDestroy(this.linked.scope);
                        }
                        this.setContentKeys(this.linked.scope.models);
                        return this.bigGulp.handleLargeArray(this.linked.scope.models, function(model) {
                            return _this.createWindow(model, void 0, _this.gMap);
                        }, function() {}, function() {
                            return _this.firstTime = false;
                        });
                    } else {
                        markersScope.onMarkerModelsReady = this.onMarkerModelsReady;
                        if (markersScope.isMarkerModelsReady) {
                            return this.onMarkerModelsReady(markersScope);
                        }
                    }
                }
            };
            WindowsParentModel.prototype.setContentKeys = function(models) {
                if (models.length > 0) {
                    return this.contentKeys = Object.keys(models[0]);
                }
            };
            WindowsParentModel.prototype.createWindow = function(model, gMarker, gMap) {
                var childScope, opts, parsedContent, _this = this;
                childScope = this.linked.scope.$new(false);
                this.setChildScope(childScope, model);
                childScope.$watch("model", function(newValue, oldValue) {
                    if (newValue !== oldValue) {
                        return _this.setChildScope(childScope, newValue);
                    }
                }, true);
                parsedContent = this.interpolateContent(this.linked.element.html(), model);
                opts = this.createWindowOptions(gMarker, childScope, parsedContent, this.DEFAULTS);
                return this.windows.push(new directives.api.models.child.WindowChildModel(childScope, opts, this.isIconVisibleOnClick, gMap, gMarker, this.$http, this.$templateCache, this.$compile, void 0, true));
            };
            WindowsParentModel.prototype.setChildScope = function(childScope, model) {
                var name, _fn, _i, _len, _ref, _this = this;
                _ref = this.scopePropNames;
                _fn = function(name) {
                    var nameKey, newValue;
                    nameKey = name + "Key";
                    newValue = _this[nameKey] === "self" ? model : model[_this[nameKey]];
                    if (newValue !== childScope[name]) {
                        return childScope[name] = newValue;
                    }
                };
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    name = _ref[_i];
                    _fn(name);
                }
                return childScope.model = model;
            };
            WindowsParentModel.prototype.interpolateContent = function(content, model) {
                var exp, interpModel, key, _i, _len, _ref;
                if (this.contentKeys === void 0 || this.contentKeys.length === 0) {
                    return;
                }
                exp = this.$interpolate(content);
                interpModel = {};
                _ref = this.contentKeys;
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    key = _ref[_i];
                    interpModel[key] = model[key];
                }
                return exp(interpModel);
            };
            return WindowsParentModel;
        }(directives.api.models.parent.IWindowParentModel);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.ILabel = function(_super) {
            __extends(ILabel, _super);
            function ILabel($timeout) {
                this.link = __bind(this.link, this);
                var self;
                self = this;
                this.restrict = "ECMA";
                this.replace = true;
                this.template = void 0;
                this.require = void 0;
                this.transclude = true;
                this.priority = -100;
                this.scope = {
                    labelContent: "=content",
                    labelAnchor: "@anchor",
                    labelClass: "@class",
                    labelStyle: "=style"
                };
                this.$log = directives.api.utils.Logger;
                this.$timeout = $timeout;
            }
            ILabel.prototype.link = function(scope, element, attrs, ctrl) {
                throw new Exception("Not Implemented!!");
            };
            return ILabel;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.IMarker = function(_super) {
            __extends(IMarker, _super);
            function IMarker($timeout) {
                this.link = __bind(this.link, this);
                var self;
                self = this;
                this.$log = directives.api.utils.Logger;
                this.$timeout = $timeout;
                this.restrict = "ECMA";
                this.require = "^googleMap";
                this.priority = -1;
                this.transclude = true;
                this.replace = true;
                this.scope = {
                    coords: "=coords",
                    icon: "=icon",
                    click: "&click",
                    options: "=options",
                    events: "=events"
                };
            }
            IMarker.prototype.controller = [ "$scope", "$element", function($scope, $element) {
                throw new Exception("Not Implemented!!");
            } ];
            IMarker.prototype.link = function(scope, element, attrs, ctrl) {
                throw new Exception("Not implemented!!");
            };
            return IMarker;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.IWindow = function(_super) {
            __extends(IWindow, _super);
            IWindow.include(directives.api.utils.ChildEvents);
            function IWindow($timeout, $compile, $http, $templateCache) {
                var self;
                this.$timeout = $timeout;
                this.$compile = $compile;
                this.$http = $http;
                this.$templateCache = $templateCache;
                this.link = __bind(this.link, this);
                self = this;
                this.restrict = "ECMA";
                this.template = void 0;
                this.transclude = true;
                this.priority = -100;
                this.require = void 0;
                this.replace = true;
                this.scope = {
                    coords: "=coords",
                    show: "=show",
                    templateUrl: "=templateurl",
                    templateParameter: "=templateparameter",
                    isIconVisibleOnClick: "=isiconvisibleonclick",
                    closeClick: "&closeclick",
                    options: "=options"
                };
                this.$log = directives.api.utils.Logger;
            }
            IWindow.prototype.link = function(scope, element, attrs, ctrls) {
                throw new Exception("Not Implemented!!");
            };
            return IWindow;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.Label = function(_super) {
            __extends(Label, _super);
            function Label($timeout) {
                this.link = __bind(this.link, this);
                var self;
                Label.__super__.constructor.call(this, $timeout);
                self = this;
                this.require = "^marker";
                this.template = '<span class="angular-google-maps-marker-label" ng-transclude></span>';
                this.$log.info(this);
            }
            Label.prototype.link = function(scope, element, attrs, ctrl) {
                var _this = this;
                return this.$timeout(function() {
                    var label, markerCtrl;
                    markerCtrl = ctrl.getMarkerScope().gMarker;
                    if (markerCtrl != null) {
                        label = new directives.api.models.child.MarkerLabelChildModel(markerCtrl, scope);
                    }
                    return scope.$on("$destroy", function() {
                        return label.destroy();
                    });
                }, directives.api.utils.GmapUtil.defaultDelay + 25);
            };
            return Label;
        }(directives.api.ILabel);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.Layer = function(_super) {
            __extends(Layer, _super);
            function Layer($timeout) {
                this.$timeout = $timeout;
                this.link = __bind(this.link, this);
                this.$log = directives.api.utils.Logger;
                this.restrict = "ECMA";
                this.require = "^googleMap";
                this.priority = -1;
                this.transclude = true;
                this.template = '<span class="angular-google-map-layer" ng-transclude></span>';
                this.replace = true;
                this.scope = {
                    show: "=show",
                    type: "=type",
                    namespace: "=namespace",
                    options: "=options",
                    onCreated: "&oncreated"
                };
            }
            Layer.prototype.link = function(scope, element, attrs, mapCtrl) {
                if (attrs.oncreated != null) {
                    return new directives.api.models.parent.LayerParentModel(scope, element, attrs, mapCtrl, this.$timeout, scope.onCreated);
                } else {
                    return new directives.api.models.parent.LayerParentModel(scope, element, attrs, mapCtrl, this.$timeout);
                }
            };
            return Layer;
        }(oo.BaseObject);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.Marker = function(_super) {
            __extends(Marker, _super);
            function Marker($timeout) {
                this.link = __bind(this.link, this);
                var self;
                Marker.__super__.constructor.call(this, $timeout);
                self = this;
                this.template = '<span class="angular-google-map-marker" ng-transclude></span>';
                this.$log.info(this);
            }
            Marker.prototype.controller = [ "$scope", "$element", function($scope, $element) {
                return {
                    getMarkerScope: function() {
                        return $scope;
                    }
                };
            } ];
            Marker.prototype.link = function(scope, element, attrs, ctrl) {
                return new directives.api.models.parent.MarkerParentModel(scope, element, attrs, ctrl, this.$timeout);
            };
            return Marker;
        }(directives.api.IMarker);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.Markers = function(_super) {
            __extends(Markers, _super);
            function Markers($timeout, $injector) {
                this.link = __bind(this.link, this);
                var self;
                Markers.__super__.constructor.call(this, $timeout);
                self = this;
                this.template = '<span class="angular-google-map-markers" ng-transclude></span>';
                this.scope.models = "=models";
                this.scope.doCluster = "=docluster";
                this.scope.clusterOptions = "=clusteroptions";
                this.scope.fit = "=fit";
                this.scope.labelContent = "=labelcontent";
                this.scope.labelAnchor = "@labelanchor";
                this.scope.labelClass = "@labelclass";
                this.$timeout = $timeout;
                this.$injector = $injector;
                this.$log.info(this);
            }
            Markers.prototype.controller = [ "$scope", "$element", function($scope, $element) {
                return {
                    getMarkersScope: function() {
                        return $scope;
                    }
                };
            } ];
            Markers.prototype.link = function(scope, element, attrs, ctrl) {
                return new directives.api.models.parent.MarkersParentModel(scope, element, attrs, ctrl, this.$timeout, this.$injector);
            };
            return Markers;
        }(directives.api.IMarker);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.Window = function(_super) {
            __extends(Window, _super);
            Window.include(directives.api.utils.GmapUtil);
            function Window($timeout, $compile, $http, $templateCache) {
                this.link = __bind(this.link, this);
                var self;
                Window.__super__.constructor.call(this, $timeout, $compile, $http, $templateCache);
                self = this;
                this.require = [ "^googleMap", "^?marker" ];
                this.template = '<span class="angular-google-maps-window" ng-transclude></span>';
                this.$log.info(self);
            }
            Window.prototype.link = function(scope, element, attrs, ctrls) {
                var _this = this;
                return this.$timeout(function() {
                    var defaults, hasScopeCoords, isIconVisibleOnClick, mapCtrl, markerCtrl, markerScope, opts, window;
                    isIconVisibleOnClick = true;
                    if (angular.isDefined(attrs.isiconvisibleonclick)) {
                        isIconVisibleOnClick = scope.isIconVisibleOnClick;
                    }
                    mapCtrl = ctrls[0].getMap();
                    markerCtrl = ctrls.length > 1 && ctrls[1] != null ? ctrls[1].getMarkerScope().gMarker : void 0;
                    defaults = scope.options != null ? scope.options : {};
                    hasScopeCoords = scope != null && scope.coords != null && scope.coords.latitude != null && scope.coords.longitude != null;
                    opts = hasScopeCoords ? _this.createWindowOptions(markerCtrl, scope, element.html(), defaults) : defaults;
                    if (mapCtrl != null) {
                        window = new directives.api.models.child.WindowChildModel(scope, opts, isIconVisibleOnClick, mapCtrl, markerCtrl, _this.$http, _this.$templateCache, _this.$compile, element);
                    }
                    scope.$on("$destroy", function() {
                        return window.destroy();
                    });
                    if (ctrls[1] != null) {
                        markerScope = ctrls[1].getMarkerScope();
                        markerScope.$watch("coords", function(newValue, oldValue) {
                            if (newValue == null) {
                                return window.hideWindow();
                            }
                        });
                        markerScope.$watch("coords.latitude", function(newValue, oldValue) {
                            if (newValue !== oldValue) {
                                return window.getLatestPosition();
                            }
                        });
                    }
                    if (_this.onChildCreation != null && window != null) {
                        return _this.onChildCreation(window);
                    }
                }, directives.api.utils.GmapUtil.defaultDelay + 25);
            };
            return Window;
        }(directives.api.IWindow);
    });
}).call(this);

(function() {
    var __bind = function(fn, me) {
        return function() {
            return fn.apply(me, arguments);
        };
    }, __hasProp = {}.hasOwnProperty, __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };
    this.ngGmapModule("directives.api", function() {
        return this.Windows = function(_super) {
            __extends(Windows, _super);
            function Windows($timeout, $compile, $http, $templateCache, $interpolate) {
                this.link = __bind(this.link, this);
                var self;
                Windows.__super__.constructor.call(this, $timeout, $compile, $http, $templateCache);
                self = this;
                this.$interpolate = $interpolate;
                this.require = [ "^googleMap", "^?markers" ];
                this.template = '<span class="angular-google-maps-windows" ng-transclude></span>';
                this.scope.models = "=models";
                this.$log.info(self);
            }
            Windows.prototype.link = function(scope, element, attrs, ctrls) {
                return new directives.api.models.parent.WindowsParentModel(scope, element, attrs, ctrls, this.$timeout, this.$compile, this.$http, this.$templateCache, this.$interpolate);
            };
            return Windows;
        }(directives.api.IWindow);
    });
}).call(this);

(function() {
    angular.module("google-maps").directive("googleMap", [ "$log", "$timeout", function($log, $timeout) {
        "use strict";
        var DEFAULTS, getCoords, isTrue;
        isTrue = function(val) {
            return angular.isDefined(val) && val !== null && val === true || val === "1" || val === "y" || val === "true";
        };
        directives.api.utils.Logger.logger = $log;
        DEFAULTS = {
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        getCoords = function(value) {
            return new google.maps.LatLng(value.latitude, value.longitude);
        };
        return {
            self: this,
            restrict: "ECMA",
            transclude: true,
            replace: false,
            template: '<div class="angular-google-map"><div class="angular-google-map-container"></div><div ng-transclude style="display: none"></div></div>',
            scope: {
                center: "=center",
                zoom: "=zoom",
                dragging: "=dragging",
                control: "=",
                windows: "=windows",
                options: "=options",
                events: "=events",
                styles: "=styles",
                bounds: "=bounds"
            },
            controller: [ "$scope", function($scope) {
                return {
                    getMap: function() {
                        return $scope.map;
                    }
                };
            } ],
            link: function(scope, element, attrs) {
                var dragging, el, eventName, getEventHandler, opts, settingCenterFromScope, type, _m, _this = this;
                if (!angular.isDefined(scope.center) || (!angular.isDefined(scope.center.latitude) || !angular.isDefined(scope.center.longitude))) {
                    $log.error("angular-google-maps: could not find a valid center property");
                    return;
                }
                if (!angular.isDefined(scope.zoom)) {
                    $log.error("angular-google-maps: map zoom property not set");
                    return;
                }
                el = angular.element(element);
                el.addClass("angular-google-map");
                opts = {
                    options: {}
                };
                if (attrs.options) {
                    opts.options = scope.options;
                }
                if (attrs.styles) {
                    opts.styles = scope.styles;
                }
                if (attrs.type) {
                    type = attrs.type.toUpperCase();
                    if (google.maps.MapTypeId.hasOwnProperty(type)) {
                        opts.mapTypeId = google.maps.MapTypeId[attrs.type.toUpperCase()];
                    } else {
                        $log.error('angular-google-maps: invalid map type "' + attrs.type + '"');
                    }
                }
                _m = new google.maps.Map(el.find("div")[1], angular.extend({}, DEFAULTS, opts, {
                    center: new google.maps.LatLng(scope.center.latitude, scope.center.longitude),
                    draggable: isTrue(attrs.draggable),
                    zoom: scope.zoom,
                    bounds: scope.bounds
                }));
                dragging = false;
                google.maps.event.addListener(_m, "dragstart", function() {
                    dragging = true;
                    return _.defer(function() {
                        return scope.$apply(function(s) {
                            if (s.dragging != null) {
                                return s.dragging = dragging;
                            }
                        });
                    });
                });
                google.maps.event.addListener(_m, "dragend", function() {
                    dragging = false;
                    return _.defer(function() {
                        return scope.$apply(function(s) {
                            if (s.dragging != null) {
                                return s.dragging = dragging;
                            }
                        });
                    });
                });
                google.maps.event.addListener(_m, "drag", function() {
                    var c;
                    c = _m.center;
                    return _.defer(function() {
                        return scope.$apply(function(s) {
                            s.center.latitude = c.lat();
                            return s.center.longitude = c.lng();
                        });
                    });
                });
                google.maps.event.addListener(_m, "zoom_changed", function() {
                    if (scope.zoom !== _m.zoom) {
                        return _.defer(function() {
                            return scope.$apply(function(s) {
                                return s.zoom = _m.zoom;
                            });
                        });
                    }
                });
                settingCenterFromScope = false;
                google.maps.event.addListener(_m, "center_changed", function() {
                    var c;
                    c = _m.center;
                    if (settingCenterFromScope) {
                        return;
                    }
                    return _.defer(function() {
                        return scope.$apply(function(s) {
                            if (!_m.dragging) {
                                if (s.center.latitude !== c.lat()) {
                                    s.center.latitude = c.lat();
                                }
                                if (s.center.longitude !== c.lng()) {
                                    return s.center.longitude = c.lng();
                                }
                            }
                        });
                    });
                });
                google.maps.event.addListener(_m, "idle", function() {
                    var b, ne, sw;
                    b = _m.getBounds();
                    ne = b.getNorthEast();
                    sw = b.getSouthWest();
                    return _.defer(function() {
                        return scope.$apply(function(s) {
                            if (s.bounds !== null && s.bounds !== undefined && s.bounds !== void 0) {
                                s.bounds.northeast = {
                                    latitude: ne.lat(),
                                    longitude: ne.lng()
                                };
                                return s.bounds.southwest = {
                                    latitude: sw.lat(),
                                    longitude: sw.lng()
                                };
                            }
                        });
                    });
                });
                if (angular.isDefined(scope.events) && scope.events !== null && angular.isObject(scope.events)) {
                    getEventHandler = function(eventName) {
                        return function() {
                            return scope.events[eventName].apply(scope, [ _m, eventName, arguments ]);
                        };
                    };
                    for (eventName in scope.events) {
                        if (scope.events.hasOwnProperty(eventName) && angular.isFunction(scope.events[eventName])) {
                            google.maps.event.addListener(_m, eventName, getEventHandler(eventName));
                        }
                    }
                }
                scope.map = _m;
                if (attrs.control != null && scope.control != null) {
                    scope.control.refresh = function(maybeCoords) {
                        var coords;
                        if (_m == null) {
                            return;
                        }
                        google.maps.event.trigger(_m, "resize");
                        if ((maybeCoords != null ? maybeCoords.latitude : void 0) != null && (maybeCoords != null ? maybeCoords.latitude : void 0) != null) {
                            coords = getCoords(maybeCoords);
                            if (isTrue(attrs.pan)) {
                                return _m.panTo(coords);
                            } else {
                                return _m.setCenter(coords);
                            }
                        }
                    };
                    scope.control.getGMap = function() {
                        return _m;
                    };
                }
                scope.$watch("center", function(newValue, oldValue) {
                    var coords;
                    coords = getCoords(newValue);
                    if (newValue === oldValue || coords.lat() === _m.center.lat() && coords.lng() === _m.center.lng()) {
                        return;
                    }
                    settingCenterFromScope = true;
                    if (!dragging) {
                        if (newValue.latitude == null || newValue.longitude == null) {
                            $log.error("Invalid center for newValue: " + JSON.stringify(newValue));
                        }
                        if (isTrue(attrs.pan) && scope.zoom === _m.zoom) {
                            _m.panTo(coords);
                        } else {
                            _m.setCenter(coords);
                        }
                    }
                    return settingCenterFromScope = false;
                }, true);
                scope.$watch("zoom", function(newValue, oldValue) {
                    if (newValue === oldValue || newValue === _m.zoom) {
                        return;
                    }
                    return _.defer(function() {
                        return _m.setZoom(newValue);
                    });
                });
                scope.$watch("bounds", function(newValue, oldValue) {
                    var bounds, ne, sw;
                    if (newValue === oldValue) {
                        return;
                    }
                    if (newValue.northeast.latitude == null || newValue.northeast.longitude == null || newValue.southwest.latitude == null || newValue.southwest.longitude == null) {
                        $log.error("Invalid map bounds for new value: " + JSON.stringify(newValue));
                        return;
                    }
                    ne = new google.maps.LatLng(newValue.northeast.latitude, newValue.northeast.longitude);
                    sw = new google.maps.LatLng(newValue.southwest.latitude, newValue.southwest.longitude);
                    bounds = new google.maps.LatLngBounds(sw, ne);
                    return _m.fitBounds(bounds);
                });
                scope.$watch("options", function(newValue, oldValue) {
                    if (!_.isEqual(newValue, oldValue)) {
                        opts.options = newValue;
                        if (_m != null) {
                            return _m.setOptions(opts);
                        }
                    }
                }, true);
                return scope.$watch("styles", function(newValue, oldValue) {
                    if (!_.isEqual(newValue, oldValue)) {
                        opts.styles = newValue;
                        if (_m != null) {
                            return _m.setOptions(opts);
                        }
                    }
                }, true);
            }
        };
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").directive("marker", [ "$timeout", function($timeout) {
        return new directives.api.Marker($timeout);
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").directive("markers", [ "$timeout", "$injector", function($timeout, $injector) {
        return new directives.api.Markers($timeout, $injector);
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").directive("markerLabel", [ "$log", "$timeout", function($log, $timeout) {
        return new directives.api.Label($timeout);
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").directive("polygon", [ "$log", "$timeout", function($log, $timeout) {
        var DEFAULTS, convertPathPoints, extendMapBounds, isTrue, validatePathPoints;
        validatePathPoints = function(path) {
            var i;
            i = 0;
            while (i < path.length) {
                if (angular.isUndefined(path[i].latitude) || angular.isUndefined(path[i].longitude)) {
                    return false;
                }
                i++;
            }
            return true;
        };
        convertPathPoints = function(path) {
            var i, result;
            result = new google.maps.MVCArray();
            i = 0;
            while (i < path.length) {
                result.push(new google.maps.LatLng(path[i].latitude, path[i].longitude));
                i++;
            }
            return result;
        };
        extendMapBounds = function(map, points) {
            var bounds, i;
            bounds = new google.maps.LatLngBounds();
            i = 0;
            while (i < points.length) {
                bounds.extend(points.getAt(i));
                i++;
            }
            return map.fitBounds(bounds);
        };
        isTrue = function(val) {
            return angular.isDefined(val) && val !== null && val === true || val === "1" || val === "y" || val === "true";
        };
        "use strict";
        DEFAULTS = {};
        return {
            restrict: "ECA",
            require: "^googleMap",
            replace: true,
            scope: {
                path: "=path",
                stroke: "=stroke",
                clickable: "=",
                draggable: "=",
                editable: "=",
                geodesic: "=",
                icons: "=icons",
                visible: "="
            },
            link: function(scope, element, attrs, mapCtrl) {
                if (angular.isUndefined(scope.path) || scope.path === null || scope.path.length < 2 || !validatePathPoints(scope.path)) {
                    $log.error("polyline: no valid path attribute found");
                    return;
                }
                return $timeout(function() {
                    var map, opts, pathInsertAtListener, pathPoints, pathRemoveAtListener, pathSetAtListener, polyPath, polyline;
                    map = mapCtrl.getMap();
                    pathPoints = convertPathPoints(scope.path);
                    opts = angular.extend({}, DEFAULTS, {
                        map: map,
                        path: pathPoints,
                        strokeColor: scope.stroke && scope.stroke.color,
                        strokeOpacity: scope.stroke && scope.stroke.opacity,
                        strokeWeight: scope.stroke && scope.stroke.weight
                    });
                    angular.forEach({
                        clickable: true,
                        draggable: false,
                        editable: false,
                        geodesic: false,
                        visible: true
                    }, function(defaultValue, key) {
                        if (angular.isUndefined(scope[key]) || scope[key] === null) {
                            return opts[key] = defaultValue;
                        } else {
                            return opts[key] = scope[key];
                        }
                    });
                    polyline = new google.maps.Polyline(opts);
                    if (isTrue(attrs.fit)) {
                        extendMapBounds(map, pathPoints);
                    }
                    if (angular.isDefined(scope.editable)) {
                        scope.$watch("editable", function(newValue, oldValue) {
                            return polyline.setEditable(newValue);
                        });
                    }
                    if (angular.isDefined(scope.draggable)) {
                        scope.$watch("draggable", function(newValue, oldValue) {
                            return polyline.setDraggable(newValue);
                        });
                    }
                    if (angular.isDefined(scope.visible)) {
                        scope.$watch("visible", function(newValue, oldValue) {
                            return polyline.setVisible(newValue);
                        });
                    }
                    pathSetAtListener = void 0;
                    pathInsertAtListener = void 0;
                    pathRemoveAtListener = void 0;
                    polyPath = polyline.getPath();
                    pathSetAtListener = google.maps.event.addListener(polyPath, "set_at", function(index) {
                        var value;
                        value = polyPath.getAt(index);
                        if (!value) {
                            return;
                        }
                        if (!value.lng || !value.lat) {
                            return;
                        }
                        scope.path[index].latitude = value.lat();
                        scope.path[index].longitude = value.lng();
                        return scope.$apply();
                    });
                    pathInsertAtListener = google.maps.event.addListener(polyPath, "insert_at", function(index) {
                        var value;
                        value = polyPath.getAt(index);
                        if (!value) {
                            return;
                        }
                        if (!value.lng || !value.lat) {
                            return;
                        }
                        scope.path.splice(index, 0, {
                            latitude: value.lat(),
                            longitude: value.lng()
                        });
                        return scope.$apply();
                    });
                    pathRemoveAtListener = google.maps.event.addListener(polyPath, "remove_at", function(index) {
                        scope.path.splice(index, 1);
                        return scope.$apply();
                    });
                    scope.$watch("path", function(newArray) {
                        var i, l, newLength, newValue, oldArray, oldLength, oldValue;
                        oldArray = polyline.getPath();
                        if (newArray !== oldArray) {
                            if (newArray) {
                                polyline.setMap(map);
                                i = 0;
                                oldLength = oldArray.getLength();
                                newLength = newArray.length;
                                l = Math.min(oldLength, newLength);
                                while (i < l) {
                                    oldValue = oldArray.getAt(i);
                                    newValue = newArray[i];
                                    if (oldValue.lat() !== newValue.latitude || oldValue.lng() !== newValue.longitude) {
                                        oldArray.setAt(i, new google.maps.LatLng(newValue.latitude, newValue.longitude));
                                    }
                                    i++;
                                }
                                while (i < newLength) {
                                    newValue = newArray[i];
                                    oldArray.push(new google.maps.LatLng(newValue.latitude, newValue.longitude));
                                    i++;
                                }
                                while (i < oldLength) {
                                    oldArray.pop();
                                    i++;
                                }
                                if (isTrue(attrs.fit)) {
                                    return extendMapBounds(map, oldArray);
                                }
                            } else {
                                return polyline.setMap(null);
                            }
                        }
                    }, true);
                    return scope.$on("$destroy", function() {
                        polyline.setMap(null);
                        pathSetAtListener();
                        pathSetAtListener = null;
                        pathInsertAtListener();
                        pathInsertAtListener = null;
                        pathRemoveAtListener();
                        return pathRemoveAtListener = null;
                    });
                });
            }
        };
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").directive("polyline", [ "$log", "$timeout", "array-sync", function($log, $timeout, arraySync) {
        var DEFAULTS, convertPathPoints, extendMapBounds, isTrue, validatePathPoints;
        validatePathPoints = function(path) {
            var i;
            i = 0;
            while (i < path.length) {
                if (angular.isUndefined(path[i].latitude) || angular.isUndefined(path[i].longitude)) {
                    return false;
                }
                i++;
            }
            return true;
        };
        convertPathPoints = function(path) {
            var i, result;
            result = new google.maps.MVCArray();
            i = 0;
            while (i < path.length) {
                result.push(new google.maps.LatLng(path[i].latitude, path[i].longitude));
                i++;
            }
            return result;
        };
        extendMapBounds = function(map, points) {
            var bounds, i;
            bounds = new google.maps.LatLngBounds();
            i = 0;
            while (i < points.length) {
                bounds.extend(points.getAt(i));
                i++;
            }
            return map.fitBounds(bounds);
        };
        isTrue = function(val) {
            return angular.isDefined(val) && val !== null && val === true || val === "1" || val === "y" || val === "true";
        };
        "use strict";
        DEFAULTS = {};
        return {
            restrict: "ECA",
            replace: true,
            require: "^googleMap",
            scope: {
                path: "=path",
                stroke: "=stroke",
                clickable: "=",
                draggable: "=",
                editable: "=",
                geodesic: "=",
                icons: "=icons",
                visible: "="
            },
            link: function(scope, element, attrs, mapCtrl) {
                if (angular.isUndefined(scope.path) || scope.path === null || scope.path.length < 2 || !validatePathPoints(scope.path)) {
                    $log.error("polyline: no valid path attribute found");
                    return;
                }
                return $timeout(function() {
                    var arraySyncer, buildOpts, map, polyline;
                    buildOpts = function(pathPoints) {
                        var opts;
                        opts = angular.extend({}, DEFAULTS, {
                            map: map,
                            path: pathPoints,
                            strokeColor: scope.stroke && scope.stroke.color,
                            strokeOpacity: scope.stroke && scope.stroke.opacity,
                            strokeWeight: scope.stroke && scope.stroke.weight
                        });
                        angular.forEach({
                            clickable: true,
                            draggable: false,
                            editable: false,
                            geodesic: false,
                            visible: true
                        }, function(defaultValue, key) {
                            if (angular.isUndefined(scope[key]) || scope[key] === null) {
                                return opts[key] = defaultValue;
                            } else {
                                return opts[key] = scope[key];
                            }
                        });
                        return opts;
                    };
                    map = mapCtrl.getMap();
                    polyline = new google.maps.Polyline(buildOpts(convertPathPoints(scope.path)));
                    if (isTrue(attrs.fit)) {
                        extendMapBounds(map, pathPoints);
                    }
                    if (angular.isDefined(scope.editable)) {
                        scope.$watch("editable", function(newValue, oldValue) {
                            return polyline.setEditable(newValue);
                        });
                    }
                    if (angular.isDefined(scope.draggable)) {
                        scope.$watch("draggable", function(newValue, oldValue) {
                            return polyline.setDraggable(newValue);
                        });
                    }
                    if (angular.isDefined(scope.visible)) {
                        scope.$watch("visible", function(newValue, oldValue) {
                            return polyline.setVisible(newValue);
                        });
                    }
                    if (angular.isDefined(scope.geodesic)) {
                        scope.$watch("geodesic", function(newValue, oldValue) {
                            return polyline.setOptions(buildOpts(polyline.getPath()));
                        });
                    }
                    if (angular.isDefined(scope.stroke) && angular.isDefined(scope.stroke.weight)) {
                        scope.$watch("stroke.weight", function(newValue, oldValue) {
                            return polyline.setOptions(buildOpts(polyline.getPath()));
                        });
                    }
                    if (angular.isDefined(scope.stroke) && angular.isDefined(scope.stroke.color)) {
                        scope.$watch("stroke.color", function(newValue, oldValue) {
                            return polyline.setOptions(buildOpts(polyline.getPath()));
                        });
                    }
                    arraySyncer = arraySync(polyline.getPath(), scope, "path");
                    return scope.$on("$destroy", function() {
                        polyline.setMap(null);
                        if (arraySyncer) {
                            arraySyncer();
                            return arraySyncer = null;
                        }
                    });
                });
            }
        };
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").directive("window", [ "$timeout", "$compile", "$http", "$templateCache", function($timeout, $compile, $http, $templateCache) {
        return new directives.api.Window($timeout, $compile, $http, $templateCache);
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").directive("windows", [ "$timeout", "$compile", "$http", "$templateCache", "$interpolate", function($timeout, $compile, $http, $templateCache, $interpolate) {
        return new directives.api.Windows($timeout, $compile, $http, $templateCache, $interpolate);
    } ]);
}).call(this);

(function() {
    angular.module("google-maps").directive("layer", [ "$timeout", function($timeout) {
        return new directives.api.Layer($timeout);
    } ]);
}).call(this);

function InfoBox(opt_opts) {
    opt_opts = opt_opts || {};
    google.maps.OverlayView.apply(this, arguments);
    this.content_ = opt_opts.content || "";
    this.disableAutoPan_ = opt_opts.disableAutoPan || false;
    this.maxWidth_ = opt_opts.maxWidth || 0;
    this.pixelOffset_ = opt_opts.pixelOffset || new google.maps.Size(0, 0);
    this.position_ = opt_opts.position || new google.maps.LatLng(0, 0);
    this.zIndex_ = opt_opts.zIndex || null;
    this.boxClass_ = opt_opts.boxClass || "infoBox";
    this.boxStyle_ = opt_opts.boxStyle || {};
    this.closeBoxMargin_ = opt_opts.closeBoxMargin || "2px";
    this.closeBoxURL_ = opt_opts.closeBoxURL || "http://www.google.com/intl/en_us/mapfiles/close.gif";
    if (opt_opts.closeBoxURL === "") {
        this.closeBoxURL_ = "";
    }
    this.infoBoxClearance_ = opt_opts.infoBoxClearance || new google.maps.Size(1, 1);
    if (typeof opt_opts.visible === "undefined") {
        if (typeof opt_opts.isHidden === "undefined") {
            opt_opts.visible = true;
        } else {
            opt_opts.visible = !opt_opts.isHidden;
        }
    }
    this.isHidden_ = !opt_opts.visible;
    this.alignBottom_ = opt_opts.alignBottom || false;
    this.pane_ = opt_opts.pane || "floatPane";
    this.enableEventPropagation_ = opt_opts.enableEventPropagation || false;
    this.div_ = null;
    this.closeListener_ = null;
    this.moveListener_ = null;
    this.contextListener_ = null;
    this.eventListeners_ = null;
    this.fixedWidthSet_ = null;
}

InfoBox.prototype = new google.maps.OverlayView();

InfoBox.prototype.createInfoBoxDiv_ = function() {
    var i;
    var events;
    var bw;
    var me = this;
    var cancelHandler = function(e) {
        e.cancelBubble = true;
        if (e.stopPropagation) {
            e.stopPropagation();
        }
    };
    var ignoreHandler = function(e) {
        e.returnValue = false;
        if (e.preventDefault) {
            e.preventDefault();
        }
        if (!me.enableEventPropagation_) {
            cancelHandler(e);
        }
    };
    if (!this.div_) {
        this.div_ = document.createElement("div");
        this.setBoxStyle_();
        if (typeof this.content_.nodeType === "undefined") {
            this.div_.innerHTML = this.getCloseBoxImg_() + this.content_;
        } else {
            this.div_.innerHTML = this.getCloseBoxImg_();
            this.div_.appendChild(this.content_);
        }
        this.getPanes()[this.pane_].appendChild(this.div_);
        this.addClickHandler_();
        if (this.div_.style.width) {
            this.fixedWidthSet_ = true;
        } else {
            if (this.maxWidth_ !== 0 && this.div_.offsetWidth > this.maxWidth_) {
                this.div_.style.width = this.maxWidth_;
                this.div_.style.overflow = "auto";
                this.fixedWidthSet_ = true;
            } else {
                bw = this.getBoxWidths_();
                this.div_.style.width = this.div_.offsetWidth - bw.left - bw.right + "px";
                this.fixedWidthSet_ = false;
            }
        }
        this.panBox_(this.disableAutoPan_);
        if (!this.enableEventPropagation_) {
            this.eventListeners_ = [];
            events = [ "mousedown", "mouseover", "mouseout", "mouseup", "click", "dblclick", "touchstart", "touchend", "touchmove" ];
            for (i = 0; i < events.length; i++) {
                this.eventListeners_.push(google.maps.event.addDomListener(this.div_, events[i], cancelHandler));
            }
            this.eventListeners_.push(google.maps.event.addDomListener(this.div_, "mouseover", function(e) {
                this.style.cursor = "default";
            }));
        }
        this.contextListener_ = google.maps.event.addDomListener(this.div_, "contextmenu", ignoreHandler);
        google.maps.event.trigger(this, "domready");
    }
};

InfoBox.prototype.getCloseBoxImg_ = function() {
    var img = "";
    if (this.closeBoxURL_ !== "") {
        img = "<img";
        img += " src='" + this.closeBoxURL_ + "'";
        img += " align=right";
        img += " style='";
        img += " position: relative;";
        img += " cursor: pointer;";
        img += " margin: " + this.closeBoxMargin_ + ";";
        img += "'>";
    }
    return img;
};

InfoBox.prototype.addClickHandler_ = function() {
    var closeBox;
    if (this.closeBoxURL_ !== "") {
        closeBox = this.div_.firstChild;
        this.closeListener_ = google.maps.event.addDomListener(closeBox, "click", this.getCloseClickHandler_());
    } else {
        this.closeListener_ = null;
    }
};

InfoBox.prototype.getCloseClickHandler_ = function() {
    var me = this;
    return function(e) {
        e.cancelBubble = true;
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        google.maps.event.trigger(me, "closeclick");
        me.close();
    };
};

InfoBox.prototype.panBox_ = function(disablePan) {
    var map;
    var bounds;
    var xOffset = 0, yOffset = 0;
    if (!disablePan) {
        map = this.getMap();
        if (map instanceof google.maps.Map) {
            if (!map.getBounds().contains(this.position_)) {
                map.setCenter(this.position_);
            }
            bounds = map.getBounds();
            var mapDiv = map.getDiv();
            var mapWidth = mapDiv.offsetWidth;
            var mapHeight = mapDiv.offsetHeight;
            var iwOffsetX = this.pixelOffset_.width;
            var iwOffsetY = this.pixelOffset_.height;
            var iwWidth = this.div_.offsetWidth;
            var iwHeight = this.div_.offsetHeight;
            var padX = this.infoBoxClearance_.width;
            var padY = this.infoBoxClearance_.height;
            var pixPosition = this.getProjection().fromLatLngToContainerPixel(this.position_);
            if (pixPosition.x < -iwOffsetX + padX) {
                xOffset = pixPosition.x + iwOffsetX - padX;
            } else if (pixPosition.x + iwWidth + iwOffsetX + padX > mapWidth) {
                xOffset = pixPosition.x + iwWidth + iwOffsetX + padX - mapWidth;
            }
            if (this.alignBottom_) {
                if (pixPosition.y < -iwOffsetY + padY + iwHeight) {
                    yOffset = pixPosition.y + iwOffsetY - padY - iwHeight;
                } else if (pixPosition.y + iwOffsetY + padY > mapHeight) {
                    yOffset = pixPosition.y + iwOffsetY + padY - mapHeight;
                }
            } else {
                if (pixPosition.y < -iwOffsetY + padY) {
                    yOffset = pixPosition.y + iwOffsetY - padY;
                } else if (pixPosition.y + iwHeight + iwOffsetY + padY > mapHeight) {
                    yOffset = pixPosition.y + iwHeight + iwOffsetY + padY - mapHeight;
                }
            }
            if (!(xOffset === 0 && yOffset === 0)) {
                var c = map.getCenter();
                map.panBy(xOffset, yOffset);
            }
        }
    }
};

InfoBox.prototype.setBoxStyle_ = function() {
    var i, boxStyle;
    if (this.div_) {
        this.div_.className = this.boxClass_;
        this.div_.style.cssText = "";
        boxStyle = this.boxStyle_;
        for (i in boxStyle) {
            if (boxStyle.hasOwnProperty(i)) {
                this.div_.style[i] = boxStyle[i];
            }
        }
        if (typeof this.div_.style.opacity !== "undefined" && this.div_.style.opacity !== "") {
            this.div_.style.filter = "alpha(opacity=" + this.div_.style.opacity * 100 + ")";
        }
        this.div_.style.position = "absolute";
        this.div_.style.visibility = "hidden";
        if (this.zIndex_ !== null) {
            this.div_.style.zIndex = this.zIndex_;
        }
    }
};

InfoBox.prototype.getBoxWidths_ = function() {
    var computedStyle;
    var bw = {
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    };
    var box = this.div_;
    if (document.defaultView && document.defaultView.getComputedStyle) {
        computedStyle = box.ownerDocument.defaultView.getComputedStyle(box, "");
        if (computedStyle) {
            bw.top = parseInt(computedStyle.borderTopWidth, 10) || 0;
            bw.bottom = parseInt(computedStyle.borderBottomWidth, 10) || 0;
            bw.left = parseInt(computedStyle.borderLeftWidth, 10) || 0;
            bw.right = parseInt(computedStyle.borderRightWidth, 10) || 0;
        }
    } else if (document.documentElement.currentStyle) {
        if (box.currentStyle) {
            bw.top = parseInt(box.currentStyle.borderTopWidth, 10) || 0;
            bw.bottom = parseInt(box.currentStyle.borderBottomWidth, 10) || 0;
            bw.left = parseInt(box.currentStyle.borderLeftWidth, 10) || 0;
            bw.right = parseInt(box.currentStyle.borderRightWidth, 10) || 0;
        }
    }
    return bw;
};

InfoBox.prototype.onRemove = function() {
    if (this.div_) {
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
    }
};

InfoBox.prototype.draw = function() {
    this.createInfoBoxDiv_();
    var pixPosition = this.getProjection().fromLatLngToDivPixel(this.position_);
    this.div_.style.left = pixPosition.x + this.pixelOffset_.width + "px";
    if (this.alignBottom_) {
        this.div_.style.bottom = -(pixPosition.y + this.pixelOffset_.height) + "px";
    } else {
        this.div_.style.top = pixPosition.y + this.pixelOffset_.height + "px";
    }
    if (this.isHidden_) {
        this.div_.style.visibility = "hidden";
    } else {
        this.div_.style.visibility = "visible";
    }
};

InfoBox.prototype.setOptions = function(opt_opts) {
    if (typeof opt_opts.boxClass !== "undefined") {
        this.boxClass_ = opt_opts.boxClass;
        this.setBoxStyle_();
    }
    if (typeof opt_opts.boxStyle !== "undefined") {
        this.boxStyle_ = opt_opts.boxStyle;
        this.setBoxStyle_();
    }
    if (typeof opt_opts.content !== "undefined") {
        this.setContent(opt_opts.content);
    }
    if (typeof opt_opts.disableAutoPan !== "undefined") {
        this.disableAutoPan_ = opt_opts.disableAutoPan;
    }
    if (typeof opt_opts.maxWidth !== "undefined") {
        this.maxWidth_ = opt_opts.maxWidth;
    }
    if (typeof opt_opts.pixelOffset !== "undefined") {
        this.pixelOffset_ = opt_opts.pixelOffset;
    }
    if (typeof opt_opts.alignBottom !== "undefined") {
        this.alignBottom_ = opt_opts.alignBottom;
    }
    if (typeof opt_opts.position !== "undefined") {
        this.setPosition(opt_opts.position);
    }
    if (typeof opt_opts.zIndex !== "undefined") {
        this.setZIndex(opt_opts.zIndex);
    }
    if (typeof opt_opts.closeBoxMargin !== "undefined") {
        this.closeBoxMargin_ = opt_opts.closeBoxMargin;
    }
    if (typeof opt_opts.closeBoxURL !== "undefined") {
        this.closeBoxURL_ = opt_opts.closeBoxURL;
    }
    if (typeof opt_opts.infoBoxClearance !== "undefined") {
        this.infoBoxClearance_ = opt_opts.infoBoxClearance;
    }
    if (typeof opt_opts.isHidden !== "undefined") {
        this.isHidden_ = opt_opts.isHidden;
    }
    if (typeof opt_opts.visible !== "undefined") {
        this.isHidden_ = !opt_opts.visible;
    }
    if (typeof opt_opts.enableEventPropagation !== "undefined") {
        this.enableEventPropagation_ = opt_opts.enableEventPropagation;
    }
    if (this.div_) {
        this.draw();
    }
};

InfoBox.prototype.setContent = function(content) {
    this.content_ = content;
    if (this.div_) {
        if (this.closeListener_) {
            google.maps.event.removeListener(this.closeListener_);
            this.closeListener_ = null;
        }
        if (!this.fixedWidthSet_) {
            this.div_.style.width = "";
        }
        if (typeof content.nodeType === "undefined") {
            this.div_.innerHTML = this.getCloseBoxImg_() + content;
        } else {
            this.div_.innerHTML = this.getCloseBoxImg_();
            this.div_.appendChild(content);
        }
        if (!this.fixedWidthSet_) {
            this.div_.style.width = this.div_.offsetWidth + "px";
            if (typeof content.nodeType === "undefined") {
                this.div_.innerHTML = this.getCloseBoxImg_() + content;
            } else {
                this.div_.innerHTML = this.getCloseBoxImg_();
                this.div_.appendChild(content);
            }
        }
        this.addClickHandler_();
    }
    google.maps.event.trigger(this, "content_changed");
};

InfoBox.prototype.setPosition = function(latlng) {
    this.position_ = latlng;
    if (this.div_) {
        this.draw();
    }
    google.maps.event.trigger(this, "position_changed");
};

InfoBox.prototype.setZIndex = function(index) {
    this.zIndex_ = index;
    if (this.div_) {
        this.div_.style.zIndex = index;
    }
    google.maps.event.trigger(this, "zindex_changed");
};

InfoBox.prototype.setVisible = function(isVisible) {
    this.isHidden_ = !isVisible;
    if (this.div_) {
        this.div_.style.visibility = this.isHidden_ ? "hidden" : "visible";
    }
};

InfoBox.prototype.getContent = function() {
    return this.content_;
};

InfoBox.prototype.getPosition = function() {
    return this.position_;
};

InfoBox.prototype.getZIndex = function() {
    return this.zIndex_;
};

InfoBox.prototype.getVisible = function() {
    var isVisible;
    if (typeof this.getMap() === "undefined" || this.getMap() === null) {
        isVisible = false;
    } else {
        isVisible = !this.isHidden_;
    }
    return isVisible;
};

InfoBox.prototype.show = function() {
    this.isHidden_ = false;
    if (this.div_) {
        this.div_.style.visibility = "visible";
    }
};

InfoBox.prototype.hide = function() {
    this.isHidden_ = true;
    if (this.div_) {
        this.div_.style.visibility = "hidden";
    }
};

InfoBox.prototype.open = function(map, anchor) {
    var me = this;
    if (anchor) {
        this.position_ = anchor.getPosition();
        this.moveListener_ = google.maps.event.addListener(anchor, "position_changed", function() {
            me.setPosition(this.getPosition());
        });
    }
    this.setMap(map);
    if (this.div_) {
        this.panBox_();
    }
};

InfoBox.prototype.close = function() {
    var i;
    if (this.closeListener_) {
        google.maps.event.removeListener(this.closeListener_);
        this.closeListener_ = null;
    }
    if (this.eventListeners_) {
        for (i = 0; i < this.eventListeners_.length; i++) {
            google.maps.event.removeListener(this.eventListeners_[i]);
        }
        this.eventListeners_ = null;
    }
    if (this.moveListener_) {
        google.maps.event.removeListener(this.moveListener_);
        this.moveListener_ = null;
    }
    if (this.contextListener_) {
        google.maps.event.removeListener(this.contextListener_);
        this.contextListener_ = null;
    }
    this.setMap(null);
};

function ClusterIcon(cluster, styles) {
    cluster.getMarkerClusterer().extend(ClusterIcon, google.maps.OverlayView);
    this.cluster_ = cluster;
    this.className_ = cluster.getMarkerClusterer().getClusterClass();
    this.styles_ = styles;
    this.center_ = null;
    this.div_ = null;
    this.sums_ = null;
    this.visible_ = false;
    this.setMap(cluster.getMap());
}

ClusterIcon.prototype.onAdd = function() {
    var cClusterIcon = this;
    var cMouseDownInCluster;
    var cDraggingMapByCluster;
    this.div_ = document.createElement("div");
    this.div_.className = this.className_;
    if (this.visible_) {
        this.show();
    }
    this.getPanes().overlayMouseTarget.appendChild(this.div_);
    this.boundsChangedListener_ = google.maps.event.addListener(this.getMap(), "bounds_changed", function() {
        cDraggingMapByCluster = cMouseDownInCluster;
    });
    google.maps.event.addDomListener(this.div_, "mousedown", function() {
        cMouseDownInCluster = true;
        cDraggingMapByCluster = false;
    });
    google.maps.event.addDomListener(this.div_, "click", function(e) {
        cMouseDownInCluster = false;
        if (!cDraggingMapByCluster) {
            var theBounds;
            var mz;
            var mc = cClusterIcon.cluster_.getMarkerClusterer();
            google.maps.event.trigger(mc, "click", cClusterIcon.cluster_);
            google.maps.event.trigger(mc, "clusterclick", cClusterIcon.cluster_);
            if (mc.getZoomOnClick()) {
                mz = mc.getMaxZoom();
                theBounds = cClusterIcon.cluster_.getBounds();
                mc.getMap().fitBounds(theBounds);
                setTimeout(function() {
                    mc.getMap().fitBounds(theBounds);
                    if (mz !== null && mc.getMap().getZoom() > mz) {
                        mc.getMap().setZoom(mz + 1);
                    }
                }, 100);
            }
            e.cancelBubble = true;
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }
    });
    google.maps.event.addDomListener(this.div_, "mouseover", function() {
        var mc = cClusterIcon.cluster_.getMarkerClusterer();
        google.maps.event.trigger(mc, "mouseover", cClusterIcon.cluster_);
    });
    google.maps.event.addDomListener(this.div_, "mouseout", function() {
        var mc = cClusterIcon.cluster_.getMarkerClusterer();
        google.maps.event.trigger(mc, "mouseout", cClusterIcon.cluster_);
    });
};

ClusterIcon.prototype.onRemove = function() {
    if (this.div_ && this.div_.parentNode) {
        this.hide();
        google.maps.event.removeListener(this.boundsChangedListener_);
        google.maps.event.clearInstanceListeners(this.div_);
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
    }
};

ClusterIcon.prototype.draw = function() {
    if (this.visible_) {
        var pos = this.getPosFromLatLng_(this.center_);
        this.div_.style.top = pos.y + "px";
        this.div_.style.left = pos.x + "px";
    }
};

ClusterIcon.prototype.hide = function() {
    if (this.div_) {
        this.div_.style.display = "none";
    }
    this.visible_ = false;
};

ClusterIcon.prototype.show = function() {
    if (this.div_) {
        var img = "";
        var bp = this.backgroundPosition_.split(" ");
        var spriteH = parseInt(bp[0].trim(), 10);
        var spriteV = parseInt(bp[1].trim(), 10);
        var pos = this.getPosFromLatLng_(this.center_);
        this.div_.style.cssText = this.createCss(pos);
        img = "<img src='" + this.url_ + "' style='position: absolute; top: " + spriteV + "px; left: " + spriteH + "px; ";
        if (!this.cluster_.getMarkerClusterer().enableRetinaIcons_) {
            img += "clip: rect(" + -1 * spriteV + "px, " + (-1 * spriteH + this.width_) + "px, " + (-1 * spriteV + this.height_) + "px, " + -1 * spriteH + "px);";
        }
        img += "'>";
        this.div_.innerHTML = img + "<div style='" + "position: absolute;" + "top: " + this.anchorText_[0] + "px;" + "left: " + this.anchorText_[1] + "px;" + "color: " + this.textColor_ + ";" + "font-size: " + this.textSize_ + "px;" + "font-family: " + this.fontFamily_ + ";" + "font-weight: " + this.fontWeight_ + ";" + "font-style: " + this.fontStyle_ + ";" + "text-decoration: " + this.textDecoration_ + ";" + "text-align: center;" + "width: " + this.width_ + "px;" + "line-height:" + this.height_ + "px;" + "'>" + this.sums_.text + "</div>";
        if (typeof this.sums_.title === "undefined" || this.sums_.title === "") {
            this.div_.title = this.cluster_.getMarkerClusterer().getTitle();
        } else {
            this.div_.title = this.sums_.title;
        }
        this.div_.style.display = "";
    }
    this.visible_ = true;
};

ClusterIcon.prototype.useStyle = function(sums) {
    this.sums_ = sums;
    var index = Math.max(0, sums.index - 1);
    index = Math.min(this.styles_.length - 1, index);
    var style = this.styles_[index];
    this.url_ = style.url;
    this.height_ = style.height;
    this.width_ = style.width;
    this.anchorText_ = style.anchorText || [ 0, 0 ];
    this.anchorIcon_ = style.anchorIcon || [ parseInt(this.height_ / 2, 10), parseInt(this.width_ / 2, 10) ];
    this.textColor_ = style.textColor || "black";
    this.textSize_ = style.textSize || 11;
    this.textDecoration_ = style.textDecoration || "none";
    this.fontWeight_ = style.fontWeight || "bold";
    this.fontStyle_ = style.fontStyle || "normal";
    this.fontFamily_ = style.fontFamily || "Arial,sans-serif";
    this.backgroundPosition_ = style.backgroundPosition || "0 0";
};

ClusterIcon.prototype.setCenter = function(center) {
    this.center_ = center;
};

ClusterIcon.prototype.createCss = function(pos) {
    var style = [];
    style.push("cursor: pointer;");
    style.push("position: absolute; top: " + pos.y + "px; left: " + pos.x + "px;");
    style.push("width: " + this.width_ + "px; height: " + this.height_ + "px;");
    return style.join("");
};

ClusterIcon.prototype.getPosFromLatLng_ = function(latlng) {
    var pos = this.getProjection().fromLatLngToDivPixel(latlng);
    pos.x -= this.anchorIcon_[1];
    pos.y -= this.anchorIcon_[0];
    pos.x = parseInt(pos.x, 10);
    pos.y = parseInt(pos.y, 10);
    return pos;
};

function Cluster(mc) {
    this.markerClusterer_ = mc;
    this.map_ = mc.getMap();
    this.gridSize_ = mc.getGridSize();
    this.minClusterSize_ = mc.getMinimumClusterSize();
    this.averageCenter_ = mc.getAverageCenter();
    this.markers_ = [];
    this.center_ = null;
    this.bounds_ = null;
    this.clusterIcon_ = new ClusterIcon(this, mc.getStyles());
}

Cluster.prototype.getSize = function() {
    return this.markers_.length;
};

Cluster.prototype.getMarkers = function() {
    return this.markers_;
};

Cluster.prototype.getCenter = function() {
    return this.center_;
};

Cluster.prototype.getMap = function() {
    return this.map_;
};

Cluster.prototype.getMarkerClusterer = function() {
    return this.markerClusterer_;
};

Cluster.prototype.getBounds = function() {
    var i;
    var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
    var markers = this.getMarkers();
    for (i = 0; i < markers.length; i++) {
        bounds.extend(markers[i].getPosition());
    }
    return bounds;
};

Cluster.prototype.remove = function() {
    this.clusterIcon_.setMap(null);
    this.markers_ = [];
    delete this.markers_;
};

Cluster.prototype.addMarker = function(marker) {
    var i;
    var mCount;
    var mz;
    if (this.isMarkerAlreadyAdded_(marker)) {
        return false;
    }
    if (!this.center_) {
        this.center_ = marker.getPosition();
        this.calculateBounds_();
    } else {
        if (this.averageCenter_) {
            var l = this.markers_.length + 1;
            var lat = (this.center_.lat() * (l - 1) + marker.getPosition().lat()) / l;
            var lng = (this.center_.lng() * (l - 1) + marker.getPosition().lng()) / l;
            this.center_ = new google.maps.LatLng(lat, lng);
            this.calculateBounds_();
        }
    }
    marker.isAdded = true;
    this.markers_.push(marker);
    mCount = this.markers_.length;
    mz = this.markerClusterer_.getMaxZoom();
    if (mz !== null && this.map_.getZoom() > mz) {
        if (marker.getMap() !== this.map_) {
            marker.setMap(this.map_);
        }
    } else if (mCount < this.minClusterSize_) {
        if (marker.getMap() !== this.map_) {
            marker.setMap(this.map_);
        }
    } else if (mCount === this.minClusterSize_) {
        for (i = 0; i < mCount; i++) {
            this.markers_[i].setMap(null);
        }
    } else {
        marker.setMap(null);
    }
    this.updateIcon_();
    return true;
};

Cluster.prototype.isMarkerInClusterBounds = function(marker) {
    return this.bounds_.contains(marker.getPosition());
};

Cluster.prototype.calculateBounds_ = function() {
    var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
    this.bounds_ = this.markerClusterer_.getExtendedBounds(bounds);
};

Cluster.prototype.updateIcon_ = function() {
    var mCount = this.markers_.length;
    var mz = this.markerClusterer_.getMaxZoom();
    if (mz !== null && this.map_.getZoom() > mz) {
        this.clusterIcon_.hide();
        return;
    }
    if (mCount < this.minClusterSize_) {
        this.clusterIcon_.hide();
        return;
    }
    var numStyles = this.markerClusterer_.getStyles().length;
    var sums = this.markerClusterer_.getCalculator()(this.markers_, numStyles);
    this.clusterIcon_.setCenter(this.center_);
    this.clusterIcon_.useStyle(sums);
    this.clusterIcon_.show();
};

Cluster.prototype.isMarkerAlreadyAdded_ = function(marker) {
    var i;
    if (this.markers_.indexOf) {
        return this.markers_.indexOf(marker) !== -1;
    } else {
        for (i = 0; i < this.markers_.length; i++) {
            if (marker === this.markers_[i]) {
                return true;
            }
        }
    }
    return false;
};

function MarkerClusterer(map, opt_markers, opt_options) {
    this.extend(MarkerClusterer, google.maps.OverlayView);
    opt_markers = opt_markers || [];
    opt_options = opt_options || {};
    this.markers_ = [];
    this.clusters_ = [];
    this.listeners_ = [];
    this.activeMap_ = null;
    this.ready_ = false;
    this.gridSize_ = opt_options.gridSize || 60;
    this.minClusterSize_ = opt_options.minimumClusterSize || 2;
    this.maxZoom_ = opt_options.maxZoom || null;
    this.styles_ = opt_options.styles || [];
    this.title_ = opt_options.title || "";
    this.zoomOnClick_ = true;
    if (opt_options.zoomOnClick !== undefined) {
        this.zoomOnClick_ = opt_options.zoomOnClick;
    }
    this.averageCenter_ = false;
    if (opt_options.averageCenter !== undefined) {
        this.averageCenter_ = opt_options.averageCenter;
    }
    this.ignoreHidden_ = false;
    if (opt_options.ignoreHidden !== undefined) {
        this.ignoreHidden_ = opt_options.ignoreHidden;
    }
    this.enableRetinaIcons_ = false;
    if (opt_options.enableRetinaIcons !== undefined) {
        this.enableRetinaIcons_ = opt_options.enableRetinaIcons;
    }
    this.imagePath_ = opt_options.imagePath || MarkerClusterer.IMAGE_PATH;
    this.imageExtension_ = opt_options.imageExtension || MarkerClusterer.IMAGE_EXTENSION;
    this.imageSizes_ = opt_options.imageSizes || MarkerClusterer.IMAGE_SIZES;
    this.calculator_ = opt_options.calculator || MarkerClusterer.CALCULATOR;
    this.batchSize_ = opt_options.batchSize || MarkerClusterer.BATCH_SIZE;
    this.batchSizeIE_ = opt_options.batchSizeIE || MarkerClusterer.BATCH_SIZE_IE;
    this.clusterClass_ = opt_options.clusterClass || "cluster";
    if (navigator.userAgent.toLowerCase().indexOf("msie") !== -1) {
        this.batchSize_ = this.batchSizeIE_;
    }
    this.setupStyles_();
    this.addMarkers(opt_markers, true);
    this.setMap(map);
}

MarkerClusterer.prototype.onAdd = function() {
    var cMarkerClusterer = this;
    this.activeMap_ = this.getMap();
    this.ready_ = true;
    this.repaint();
    this.listeners_ = [ google.maps.event.addListener(this.getMap(), "zoom_changed", function() {
        cMarkerClusterer.resetViewport_(false);
        if (this.getZoom() === (this.get("minZoom") || 0) || this.getZoom() === this.get("maxZoom")) {
            google.maps.event.trigger(this, "idle");
        }
    }), google.maps.event.addListener(this.getMap(), "idle", function() {
        cMarkerClusterer.redraw_();
    }) ];
};

MarkerClusterer.prototype.onRemove = function() {
    var i;
    for (i = 0; i < this.markers_.length; i++) {
        if (this.markers_[i].getMap() !== this.activeMap_) {
            this.markers_[i].setMap(this.activeMap_);
        }
    }
    for (i = 0; i < this.clusters_.length; i++) {
        this.clusters_[i].remove();
    }
    this.clusters_ = [];
    for (i = 0; i < this.listeners_.length; i++) {
        google.maps.event.removeListener(this.listeners_[i]);
    }
    this.listeners_ = [];
    this.activeMap_ = null;
    this.ready_ = false;
};

MarkerClusterer.prototype.draw = function() {};

MarkerClusterer.prototype.setupStyles_ = function() {
    var i, size;
    if (this.styles_.length > 0) {
        return;
    }
    for (i = 0; i < this.imageSizes_.length; i++) {
        size = this.imageSizes_[i];
        this.styles_.push({
            url: this.imagePath_ + (i + 1) + "." + this.imageExtension_,
            height: size,
            width: size
        });
    }
};

MarkerClusterer.prototype.fitMapToMarkers = function() {
    var i;
    var markers = this.getMarkers();
    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i < markers.length; i++) {
        bounds.extend(markers[i].getPosition());
    }
    this.getMap().fitBounds(bounds);
};

MarkerClusterer.prototype.getGridSize = function() {
    return this.gridSize_;
};

MarkerClusterer.prototype.setGridSize = function(gridSize) {
    this.gridSize_ = gridSize;
};

MarkerClusterer.prototype.getMinimumClusterSize = function() {
    return this.minClusterSize_;
};

MarkerClusterer.prototype.setMinimumClusterSize = function(minimumClusterSize) {
    this.minClusterSize_ = minimumClusterSize;
};

MarkerClusterer.prototype.getMaxZoom = function() {
    return this.maxZoom_;
};

MarkerClusterer.prototype.setMaxZoom = function(maxZoom) {
    this.maxZoom_ = maxZoom;
};

MarkerClusterer.prototype.getStyles = function() {
    return this.styles_;
};

MarkerClusterer.prototype.setStyles = function(styles) {
    this.styles_ = styles;
};

MarkerClusterer.prototype.getTitle = function() {
    return this.title_;
};

MarkerClusterer.prototype.setTitle = function(title) {
    this.title_ = title;
};

MarkerClusterer.prototype.getZoomOnClick = function() {
    return this.zoomOnClick_;
};

MarkerClusterer.prototype.setZoomOnClick = function(zoomOnClick) {
    this.zoomOnClick_ = zoomOnClick;
};

MarkerClusterer.prototype.getAverageCenter = function() {
    return this.averageCenter_;
};

MarkerClusterer.prototype.setAverageCenter = function(averageCenter) {
    this.averageCenter_ = averageCenter;
};

MarkerClusterer.prototype.getIgnoreHidden = function() {
    return this.ignoreHidden_;
};

MarkerClusterer.prototype.setIgnoreHidden = function(ignoreHidden) {
    this.ignoreHidden_ = ignoreHidden;
};

MarkerClusterer.prototype.getEnableRetinaIcons = function() {
    return this.enableRetinaIcons_;
};

MarkerClusterer.prototype.setEnableRetinaIcons = function(enableRetinaIcons) {
    this.enableRetinaIcons_ = enableRetinaIcons;
};

MarkerClusterer.prototype.getImageExtension = function() {
    return this.imageExtension_;
};

MarkerClusterer.prototype.setImageExtension = function(imageExtension) {
    this.imageExtension_ = imageExtension;
};

MarkerClusterer.prototype.getImagePath = function() {
    return this.imagePath_;
};

MarkerClusterer.prototype.setImagePath = function(imagePath) {
    this.imagePath_ = imagePath;
};

MarkerClusterer.prototype.getImageSizes = function() {
    return this.imageSizes_;
};

MarkerClusterer.prototype.setImageSizes = function(imageSizes) {
    this.imageSizes_ = imageSizes;
};

MarkerClusterer.prototype.getCalculator = function() {
    return this.calculator_;
};

MarkerClusterer.prototype.setCalculator = function(calculator) {
    this.calculator_ = calculator;
};

MarkerClusterer.prototype.getBatchSizeIE = function() {
    return this.batchSizeIE_;
};

MarkerClusterer.prototype.setBatchSizeIE = function(batchSizeIE) {
    this.batchSizeIE_ = batchSizeIE;
};

MarkerClusterer.prototype.getClusterClass = function() {
    return this.clusterClass_;
};

MarkerClusterer.prototype.setClusterClass = function(clusterClass) {
    this.clusterClass_ = clusterClass;
};

MarkerClusterer.prototype.getMarkers = function() {
    return this.markers_;
};

MarkerClusterer.prototype.getTotalMarkers = function() {
    return this.markers_.length;
};

MarkerClusterer.prototype.getClusters = function() {
    return this.clusters_;
};

MarkerClusterer.prototype.getTotalClusters = function() {
    return this.clusters_.length;
};

MarkerClusterer.prototype.addMarker = function(marker, opt_nodraw) {
    this.pushMarkerTo_(marker);
    if (!opt_nodraw) {
        this.redraw_();
    }
};

MarkerClusterer.prototype.addMarkers = function(markers, opt_nodraw) {
    var key;
    for (key in markers) {
        if (markers.hasOwnProperty(key)) {
            this.pushMarkerTo_(markers[key]);
        }
    }
    if (!opt_nodraw) {
        this.redraw_();
    }
};

MarkerClusterer.prototype.pushMarkerTo_ = function(marker) {
    if (marker.getDraggable()) {
        var cMarkerClusterer = this;
        google.maps.event.addListener(marker, "dragend", function() {
            if (cMarkerClusterer.ready_) {
                this.isAdded = false;
                cMarkerClusterer.repaint();
            }
        });
    }
    marker.isAdded = false;
    this.markers_.push(marker);
};

MarkerClusterer.prototype.removeMarker = function(marker, opt_nodraw) {
    var removed = this.removeMarker_(marker);
    if (!opt_nodraw && removed) {
        this.repaint();
    }
    return removed;
};

MarkerClusterer.prototype.removeMarkers = function(markers, opt_nodraw) {
    var i, r;
    var removed = false;
    for (i = 0; i < markers.length; i++) {
        r = this.removeMarker_(markers[i]);
        removed = removed || r;
    }
    if (!opt_nodraw && removed) {
        this.repaint();
    }
    return removed;
};

MarkerClusterer.prototype.removeMarker_ = function(marker) {
    var i;
    var index = -1;
    if (this.markers_.indexOf) {
        index = this.markers_.indexOf(marker);
    } else {
        for (i = 0; i < this.markers_.length; i++) {
            if (marker === this.markers_[i]) {
                index = i;
                break;
            }
        }
    }
    if (index === -1) {
        return false;
    }
    marker.setMap(null);
    this.markers_.splice(index, 1);
    return true;
};

MarkerClusterer.prototype.clearMarkers = function() {
    this.resetViewport_(true);
    this.markers_ = [];
};

MarkerClusterer.prototype.repaint = function() {
    var oldClusters = this.clusters_.slice();
    this.clusters_ = [];
    this.resetViewport_(false);
    this.redraw_();
    setTimeout(function() {
        var i;
        for (i = 0; i < oldClusters.length; i++) {
            oldClusters[i].remove();
        }
    }, 0);
};

MarkerClusterer.prototype.getExtendedBounds = function(bounds) {
    var projection = this.getProjection();
    var tr = new google.maps.LatLng(bounds.getNorthEast().lat(), bounds.getNorthEast().lng());
    var bl = new google.maps.LatLng(bounds.getSouthWest().lat(), bounds.getSouthWest().lng());
    var trPix = projection.fromLatLngToDivPixel(tr);
    trPix.x += this.gridSize_;
    trPix.y -= this.gridSize_;
    var blPix = projection.fromLatLngToDivPixel(bl);
    blPix.x -= this.gridSize_;
    blPix.y += this.gridSize_;
    var ne = projection.fromDivPixelToLatLng(trPix);
    var sw = projection.fromDivPixelToLatLng(blPix);
    bounds.extend(ne);
    bounds.extend(sw);
    return bounds;
};

MarkerClusterer.prototype.redraw_ = function() {
    this.createClusters_(0);
};

MarkerClusterer.prototype.resetViewport_ = function(opt_hide) {
    var i, marker;
    for (i = 0; i < this.clusters_.length; i++) {
        this.clusters_[i].remove();
    }
    this.clusters_ = [];
    for (i = 0; i < this.markers_.length; i++) {
        marker = this.markers_[i];
        marker.isAdded = false;
        if (opt_hide) {
            marker.setMap(null);
        }
    }
};

MarkerClusterer.prototype.distanceBetweenPoints_ = function(p1, p2) {
    var R = 6371;
    var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
    var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
};

MarkerClusterer.prototype.isMarkerInBounds_ = function(marker, bounds) {
    return bounds.contains(marker.getPosition());
};

MarkerClusterer.prototype.addToClosestCluster_ = function(marker) {
    var i, d, cluster, center;
    var distance = 4e4;
    var clusterToAddTo = null;
    for (i = 0; i < this.clusters_.length; i++) {
        cluster = this.clusters_[i];
        center = cluster.getCenter();
        if (center) {
            d = this.distanceBetweenPoints_(center, marker.getPosition());
            if (d < distance) {
                distance = d;
                clusterToAddTo = cluster;
            }
        }
    }
    if (clusterToAddTo && clusterToAddTo.isMarkerInClusterBounds(marker)) {
        clusterToAddTo.addMarker(marker);
    } else {
        cluster = new Cluster(this);
        cluster.addMarker(marker);
        this.clusters_.push(cluster);
    }
};

MarkerClusterer.prototype.createClusters_ = function(iFirst) {
    var i, marker;
    var mapBounds;
    var cMarkerClusterer = this;
    if (!this.ready_) {
        return;
    }
    if (iFirst === 0) {
        google.maps.event.trigger(this, "clusteringbegin", this);
        if (typeof this.timerRefStatic !== "undefined") {
            clearTimeout(this.timerRefStatic);
            delete this.timerRefStatic;
        }
    }
    if (this.getMap().getZoom() > 3) {
        mapBounds = new google.maps.LatLngBounds(this.getMap().getBounds().getSouthWest(), this.getMap().getBounds().getNorthEast());
    } else {
        mapBounds = new google.maps.LatLngBounds(new google.maps.LatLng(85.02070771743472, -178.48388434375), new google.maps.LatLng(-85.08136444384544, 178.00048865625));
    }
    var bounds = this.getExtendedBounds(mapBounds);
    var iLast = Math.min(iFirst + this.batchSize_, this.markers_.length);
    for (i = iFirst; i < iLast; i++) {
        marker = this.markers_[i];
        if (!marker.isAdded && this.isMarkerInBounds_(marker, bounds)) {
            if (!this.ignoreHidden_ || this.ignoreHidden_ && marker.getVisible()) {
                this.addToClosestCluster_(marker);
            }
        }
    }
    if (iLast < this.markers_.length) {
        this.timerRefStatic = setTimeout(function() {
            cMarkerClusterer.createClusters_(iLast);
        }, 0);
    } else {
        delete this.timerRefStatic;
        google.maps.event.trigger(this, "clusteringend", this);
    }
};

MarkerClusterer.prototype.extend = function(obj1, obj2) {
    return function(object) {
        var property;
        for (property in object.prototype) {
            this.prototype[property] = object.prototype[property];
        }
        return this;
    }.apply(obj1, [ obj2 ]);
};

MarkerClusterer.CALCULATOR = function(markers, numStyles) {
    var index = 0;
    var title = "";
    var count = markers.length.toString();
    var dv = count;
    while (dv !== 0) {
        dv = parseInt(dv / 10, 10);
        index++;
    }
    index = Math.min(index, numStyles);
    return {
        text: count,
        index: index,
        title: title
    };
};

MarkerClusterer.BATCH_SIZE = 2e3;

MarkerClusterer.BATCH_SIZE_IE = 500;

MarkerClusterer.IMAGE_PATH = "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/images/m";

MarkerClusterer.IMAGE_EXTENSION = "png";

MarkerClusterer.IMAGE_SIZES = [ 53, 56, 66, 78, 90 ];

if (typeof String.prototype.trim !== "function") {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, "");
    };
}

function inherits(childCtor, parentCtor) {
    function tempCtor() {}
    tempCtor.prototype = parentCtor.prototype;
    childCtor.superClass_ = parentCtor.prototype;
    childCtor.prototype = new tempCtor();
    childCtor.prototype.constructor = childCtor;
}

function MarkerLabel_(marker, crossURL, handCursorURL) {
    this.marker_ = marker;
    this.handCursorURL_ = marker.handCursorURL;
    this.labelDiv_ = document.createElement("div");
    this.labelDiv_.style.cssText = "position: absolute; overflow: hidden;";
    this.eventDiv_ = document.createElement("div");
    this.eventDiv_.style.cssText = this.labelDiv_.style.cssText;
    this.eventDiv_.setAttribute("onselectstart", "return false;");
    this.eventDiv_.setAttribute("ondragstart", "return false;");
    this.crossDiv_ = MarkerLabel_.getSharedCross(crossURL);
}

inherits(MarkerLabel_, google.maps.OverlayView);

MarkerLabel_.getSharedCross = function(crossURL) {
    var div;
    if (typeof MarkerLabel_.getSharedCross.crossDiv === "undefined") {
        div = document.createElement("img");
        div.style.cssText = "position: absolute; z-index: 1000002; display: none;";
        div.style.marginLeft = "-8px";
        div.style.marginTop = "-9px";
        div.src = crossURL;
        MarkerLabel_.getSharedCross.crossDiv = div;
    }
    return MarkerLabel_.getSharedCross.crossDiv;
};

MarkerLabel_.prototype.onAdd = function() {
    var me = this;
    var cMouseIsDown = false;
    var cDraggingLabel = false;
    var cSavedZIndex;
    var cLatOffset, cLngOffset;
    var cIgnoreClick;
    var cRaiseEnabled;
    var cStartPosition;
    var cStartCenter;
    var cRaiseOffset = 20;
    var cDraggingCursor = "url(" + this.handCursorURL_ + ")";
    var cAbortEvent = function(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        e.cancelBubble = true;
        if (e.stopPropagation) {
            e.stopPropagation();
        }
    };
    var cStopBounce = function() {
        me.marker_.setAnimation(null);
    };
    this.getPanes().overlayImage.appendChild(this.labelDiv_);
    this.getPanes().overlayMouseTarget.appendChild(this.eventDiv_);
    if (typeof MarkerLabel_.getSharedCross.processed === "undefined") {
        this.getPanes().overlayImage.appendChild(this.crossDiv_);
        MarkerLabel_.getSharedCross.processed = true;
    }
    this.listeners_ = [ google.maps.event.addDomListener(this.eventDiv_, "mouseover", function(e) {
        if (me.marker_.getDraggable() || me.marker_.getClickable()) {
            this.style.cursor = "pointer";
            google.maps.event.trigger(me.marker_, "mouseover", e);
        }
    }), google.maps.event.addDomListener(this.eventDiv_, "mouseout", function(e) {
        if ((me.marker_.getDraggable() || me.marker_.getClickable()) && !cDraggingLabel) {
            this.style.cursor = me.marker_.getCursor();
            google.maps.event.trigger(me.marker_, "mouseout", e);
        }
    }), google.maps.event.addDomListener(this.eventDiv_, "mousedown", function(e) {
        cDraggingLabel = false;
        if (me.marker_.getDraggable()) {
            cMouseIsDown = true;
            this.style.cursor = cDraggingCursor;
        }
        if (me.marker_.getDraggable() || me.marker_.getClickable()) {
            google.maps.event.trigger(me.marker_, "mousedown", e);
            cAbortEvent(e);
        }
    }), google.maps.event.addDomListener(document, "mouseup", function(mEvent) {
        var position;
        if (cMouseIsDown) {
            cMouseIsDown = false;
            me.eventDiv_.style.cursor = "pointer";
            google.maps.event.trigger(me.marker_, "mouseup", mEvent);
        }
        if (cDraggingLabel) {
            if (cRaiseEnabled) {
                position = me.getProjection().fromLatLngToDivPixel(me.marker_.getPosition());
                position.y += cRaiseOffset;
                me.marker_.setPosition(me.getProjection().fromDivPixelToLatLng(position));
                try {
                    me.marker_.setAnimation(google.maps.Animation.BOUNCE);
                    setTimeout(cStopBounce, 1406);
                } catch (e) {}
            }
            me.crossDiv_.style.display = "none";
            me.marker_.setZIndex(cSavedZIndex);
            cIgnoreClick = true;
            cDraggingLabel = false;
            mEvent.latLng = me.marker_.getPosition();
            google.maps.event.trigger(me.marker_, "dragend", mEvent);
        }
    }), google.maps.event.addListener(me.marker_.getMap(), "mousemove", function(mEvent) {
        var position;
        if (cMouseIsDown) {
            if (cDraggingLabel) {
                mEvent.latLng = new google.maps.LatLng(mEvent.latLng.lat() - cLatOffset, mEvent.latLng.lng() - cLngOffset);
                position = me.getProjection().fromLatLngToDivPixel(mEvent.latLng);
                if (cRaiseEnabled) {
                    me.crossDiv_.style.left = position.x + "px";
                    me.crossDiv_.style.top = position.y + "px";
                    me.crossDiv_.style.display = "";
                    position.y -= cRaiseOffset;
                }
                me.marker_.setPosition(me.getProjection().fromDivPixelToLatLng(position));
                if (cRaiseEnabled) {
                    me.eventDiv_.style.top = position.y + cRaiseOffset + "px";
                }
                google.maps.event.trigger(me.marker_, "drag", mEvent);
            } else {
                cLatOffset = mEvent.latLng.lat() - me.marker_.getPosition().lat();
                cLngOffset = mEvent.latLng.lng() - me.marker_.getPosition().lng();
                cSavedZIndex = me.marker_.getZIndex();
                cStartPosition = me.marker_.getPosition();
                cStartCenter = me.marker_.getMap().getCenter();
                cRaiseEnabled = me.marker_.get("raiseOnDrag");
                cDraggingLabel = true;
                me.marker_.setZIndex(1e6);
                mEvent.latLng = me.marker_.getPosition();
                google.maps.event.trigger(me.marker_, "dragstart", mEvent);
            }
        }
    }), google.maps.event.addDomListener(document, "keydown", function(e) {
        if (cDraggingLabel) {
            if (e.keyCode === 27) {
                cRaiseEnabled = false;
                me.marker_.setPosition(cStartPosition);
                me.marker_.getMap().setCenter(cStartCenter);
                google.maps.event.trigger(document, "mouseup", e);
            }
        }
    }), google.maps.event.addDomListener(this.eventDiv_, "click", function(e) {
        if (me.marker_.getDraggable() || me.marker_.getClickable()) {
            if (cIgnoreClick) {
                cIgnoreClick = false;
            } else {
                google.maps.event.trigger(me.marker_, "click", e);
                cAbortEvent(e);
            }
        }
    }), google.maps.event.addDomListener(this.eventDiv_, "dblclick", function(e) {
        if (me.marker_.getDraggable() || me.marker_.getClickable()) {
            google.maps.event.trigger(me.marker_, "dblclick", e);
            cAbortEvent(e);
        }
    }), google.maps.event.addListener(this.marker_, "dragstart", function(mEvent) {
        if (!cDraggingLabel) {
            cRaiseEnabled = this.get("raiseOnDrag");
        }
    }), google.maps.event.addListener(this.marker_, "drag", function(mEvent) {
        if (!cDraggingLabel) {
            if (cRaiseEnabled) {
                me.setPosition(cRaiseOffset);
                me.labelDiv_.style.zIndex = 1e6 + (this.get("labelInBackground") ? -1 : +1);
            }
        }
    }), google.maps.event.addListener(this.marker_, "dragend", function(mEvent) {
        if (!cDraggingLabel) {
            if (cRaiseEnabled) {
                me.setPosition(0);
            }
        }
    }), google.maps.event.addListener(this.marker_, "position_changed", function() {
        me.setPosition();
    }), google.maps.event.addListener(this.marker_, "zindex_changed", function() {
        me.setZIndex();
    }), google.maps.event.addListener(this.marker_, "visible_changed", function() {
        me.setVisible();
    }), google.maps.event.addListener(this.marker_, "labelvisible_changed", function() {
        me.setVisible();
    }), google.maps.event.addListener(this.marker_, "title_changed", function() {
        me.setTitle();
    }), google.maps.event.addListener(this.marker_, "labelcontent_changed", function() {
        me.setContent();
    }), google.maps.event.addListener(this.marker_, "labelanchor_changed", function() {
        me.setAnchor();
    }), google.maps.event.addListener(this.marker_, "labelclass_changed", function() {
        me.setStyles();
    }), google.maps.event.addListener(this.marker_, "labelstyle_changed", function() {
        me.setStyles();
    }) ];
};

MarkerLabel_.prototype.onRemove = function() {
    var i;
    if (this.labelDiv_.parentNode !== null) this.labelDiv_.parentNode.removeChild(this.labelDiv_);
    if (this.eventDiv_.parentNode !== null) this.eventDiv_.parentNode.removeChild(this.eventDiv_);
    for (i = 0; i < this.listeners_.length; i++) {
        google.maps.event.removeListener(this.listeners_[i]);
    }
};

MarkerLabel_.prototype.draw = function() {
    this.setContent();
    this.setTitle();
    this.setStyles();
};

MarkerLabel_.prototype.setContent = function() {
    var content = this.marker_.get("labelContent");
    if (typeof content.nodeType === "undefined") {
        this.labelDiv_.innerHTML = content;
        this.eventDiv_.innerHTML = this.labelDiv_.innerHTML;
    } else {
        this.labelDiv_.innerHTML = "";
        this.labelDiv_.appendChild(content);
        content = content.cloneNode(true);
        this.eventDiv_.appendChild(content);
    }
};

MarkerLabel_.prototype.setTitle = function() {
    this.eventDiv_.title = this.marker_.getTitle() || "";
};

MarkerLabel_.prototype.setStyles = function() {
    var i, labelStyle;
    this.labelDiv_.className = this.marker_.get("labelClass");
    this.eventDiv_.className = this.labelDiv_.className;
    this.labelDiv_.style.cssText = "";
    this.eventDiv_.style.cssText = "";
    labelStyle = this.marker_.get("labelStyle");
    for (i in labelStyle) {
        if (labelStyle.hasOwnProperty(i)) {
            this.labelDiv_.style[i] = labelStyle[i];
            this.eventDiv_.style[i] = labelStyle[i];
        }
    }
    this.setMandatoryStyles();
};

MarkerLabel_.prototype.setMandatoryStyles = function() {
    this.labelDiv_.style.position = "absolute";
    this.labelDiv_.style.overflow = "hidden";
    if (typeof this.labelDiv_.style.opacity !== "undefined" && this.labelDiv_.style.opacity !== "") {
        this.labelDiv_.style.MsFilter = '"progid:DXImageTransform.Microsoft.Alpha(opacity=' + this.labelDiv_.style.opacity * 100 + ')"';
        this.labelDiv_.style.filter = "alpha(opacity=" + this.labelDiv_.style.opacity * 100 + ")";
    }
    this.eventDiv_.style.position = this.labelDiv_.style.position;
    this.eventDiv_.style.overflow = this.labelDiv_.style.overflow;
    this.eventDiv_.style.opacity = .01;
    this.eventDiv_.style.MsFilter = '"progid:DXImageTransform.Microsoft.Alpha(opacity=1)"';
    this.eventDiv_.style.filter = "alpha(opacity=1)";
    this.setAnchor();
    this.setPosition();
    this.setVisible();
};

MarkerLabel_.prototype.setAnchor = function() {
    var anchor = this.marker_.get("labelAnchor");
    this.labelDiv_.style.marginLeft = -anchor.x + "px";
    this.labelDiv_.style.marginTop = -anchor.y + "px";
    this.eventDiv_.style.marginLeft = -anchor.x + "px";
    this.eventDiv_.style.marginTop = -anchor.y + "px";
};

MarkerLabel_.prototype.setPosition = function(yOffset) {
    var position = this.getProjection().fromLatLngToDivPixel(this.marker_.getPosition());
    if (typeof yOffset === "undefined") {
        yOffset = 0;
    }
    this.labelDiv_.style.left = Math.round(position.x) + "px";
    this.labelDiv_.style.top = Math.round(position.y - yOffset) + "px";
    this.eventDiv_.style.left = this.labelDiv_.style.left;
    this.eventDiv_.style.top = this.labelDiv_.style.top;
    this.setZIndex();
};

MarkerLabel_.prototype.setZIndex = function() {
    var zAdjust = this.marker_.get("labelInBackground") ? -1 : +1;
    if (typeof this.marker_.getZIndex() === "undefined") {
        this.labelDiv_.style.zIndex = parseInt(this.labelDiv_.style.top, 10) + zAdjust;
        this.eventDiv_.style.zIndex = this.labelDiv_.style.zIndex;
    } else {
        this.labelDiv_.style.zIndex = this.marker_.getZIndex() + zAdjust;
        this.eventDiv_.style.zIndex = this.labelDiv_.style.zIndex;
    }
};

MarkerLabel_.prototype.setVisible = function() {
    if (this.marker_.get("labelVisible")) {
        this.labelDiv_.style.display = this.marker_.getVisible() ? "block" : "none";
    } else {
        this.labelDiv_.style.display = "none";
    }
    this.eventDiv_.style.display = this.labelDiv_.style.display;
};

function MarkerWithLabel(opt_options) {
    opt_options = opt_options || {};
    opt_options.labelContent = opt_options.labelContent || "";
    opt_options.labelAnchor = opt_options.labelAnchor || new google.maps.Point(0, 0);
    opt_options.labelClass = opt_options.labelClass || "markerLabels";
    opt_options.labelStyle = opt_options.labelStyle || {};
    opt_options.labelInBackground = opt_options.labelInBackground || false;
    if (typeof opt_options.labelVisible === "undefined") {
        opt_options.labelVisible = true;
    }
    if (typeof opt_options.raiseOnDrag === "undefined") {
        opt_options.raiseOnDrag = true;
    }
    if (typeof opt_options.clickable === "undefined") {
        opt_options.clickable = true;
    }
    if (typeof opt_options.draggable === "undefined") {
        opt_options.draggable = false;
    }
    if (typeof opt_options.optimized === "undefined") {
        opt_options.optimized = false;
    }
    opt_options.crossImage = opt_options.crossImage || "http" + (document.location.protocol === "https:" ? "s" : "") + "://maps.gstatic.com/intl/en_us/mapfiles/drag_cross_67_16.png";
    opt_options.handCursor = opt_options.handCursor || "http" + (document.location.protocol === "https:" ? "s" : "") + "://maps.gstatic.com/intl/en_us/mapfiles/closedhand_8_8.cur";
    opt_options.optimized = false;
    this.label = new MarkerLabel_(this, opt_options.crossImage, opt_options.handCursor);
    google.maps.Marker.apply(this, arguments);
}

inherits(MarkerWithLabel, google.maps.Marker);

MarkerWithLabel.prototype.setMap = function(theMap) {
    google.maps.Marker.prototype.setMap.apply(this, arguments);
    this.label.setMap(theMap);
};